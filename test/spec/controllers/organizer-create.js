'use strict';

describe('Controller: OrganizerCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var OrganizerCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrganizerCreateCtrl = $controller('OrganizerCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
