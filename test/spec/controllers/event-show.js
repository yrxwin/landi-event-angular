'use strict';

describe('Controller: EventShowCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var EventShowCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventShowCtrl = $controller('EventShowCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
