'use strict';

describe('Controller: OrganizerEditCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var OrganizerEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrganizerEditCtrl = $controller('OrganizerEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
