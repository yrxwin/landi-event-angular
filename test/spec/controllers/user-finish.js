'use strict';

describe('Controller: UserFinishCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var UserFinishCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserFinishCtrl = $controller('UserFinishCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
