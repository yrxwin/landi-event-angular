'use strict';

describe('Controller: StudentSearchCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var StudentSearchCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentSearchCtrl = $controller('StudentSearchCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
