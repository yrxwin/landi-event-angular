'use strict';

describe('Controller: StudentManageCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var StudentManageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentManageCtrl = $controller('StudentManageCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
