'use strict';

describe('Controller: JobShowCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var JobShowCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    JobShowCtrl = $controller('JobShowCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
