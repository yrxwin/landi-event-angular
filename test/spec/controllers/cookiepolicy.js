'use strict';

describe('Controller: CookiepolicyCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var CookiepolicyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CookiepolicyCtrl = $controller('CookiepolicyCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
