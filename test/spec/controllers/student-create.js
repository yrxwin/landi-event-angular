'use strict';

describe('Controller: StudentCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var StudentCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCreateCtrl = $controller('StudentCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
