'use strict';

describe('Controller: NoAccessCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var NoAccessCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NoAccessCtrl = $controller('NoAccessCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
