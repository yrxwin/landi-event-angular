'use strict';

describe('Controller: CompanyManageCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var CompanyManageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CompanyManageCtrl = $controller('CompanyManageCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
