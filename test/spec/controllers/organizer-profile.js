'use strict';

describe('Controller: OrganizerProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var OrganizerProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrganizerProfileCtrl = $controller('OrganizerProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
