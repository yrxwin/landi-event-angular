'use strict';

describe('Controller: UserAgreementCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var UserAgreementCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserAgreementCtrl = $controller('UserAgreementCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
