'use strict';

describe('Controller: EventCompanyCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var EventCompanyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventCompanyCtrl = $controller('EventCompanyCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
