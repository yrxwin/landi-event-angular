'use strict';

describe('Controller: CompanyProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var CompanyProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CompanyProfileCtrl = $controller('CompanyProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
