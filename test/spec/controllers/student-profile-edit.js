'use strict';

describe('Controller: StudentProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var StudentProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentProfileEditCtrl = $controller('StudentProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
