'use strict';

describe('Controller: OrganizerManageCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var OrganizerManageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrganizerManageCtrl = $controller('OrganizerManageCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
