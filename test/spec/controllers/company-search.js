'use strict';

describe('Controller: CompanySearchCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var CompanySearchCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CompanySearchCtrl = $controller('CompanySearchCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
