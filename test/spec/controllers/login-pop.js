'use strict';

describe('Controller: LoginPopCtrl', function () {

  // load the controller's module
  beforeEach(module('landiWebApp'));

  var LoginPopCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoginPopCtrl = $controller('LoginPopCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
