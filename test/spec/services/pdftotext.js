'use strict';

describe('Service: pdfToText', function () {

  // load the service's module
  beforeEach(module('landiWebApp'));

  // instantiate service
  var pdfToText;
  beforeEach(inject(function (_pdfToText_) {
    pdfToText = _pdfToText_;
  }));

  it('should do something', function () {
    expect(!!pdfToText).toBe(true);
  });

});
