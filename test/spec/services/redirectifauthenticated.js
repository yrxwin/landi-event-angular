'use strict';

describe('Service: redirectIfAuthenticated', function () {

  // load the service's module
  beforeEach(module('landiWebApp'));

  // instantiate service
  var redirectIfAuthenticated;
  beforeEach(inject(function (_redirectIfAuthenticated_) {
    redirectIfAuthenticated = _redirectIfAuthenticated_;
  }));

  it('should do something', function () {
    expect(!!redirectIfAuthenticated).toBe(true);
  });

});
