'use strict';

describe('Service: OrganizerService', function () {

  // load the service's module
  beforeEach(module('landiWebApp'));

  // instantiate service
  var OrganizerService;
  beforeEach(inject(function (_OrganizerService_) {
    OrganizerService = _OrganizerService_;
  }));

  it('should do something', function () {
    expect(!!OrganizerService).toBe(true);
  });

});
