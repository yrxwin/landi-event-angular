'use strict';

describe('Service: JobService', function () {

  // load the service's module
  beforeEach(module('landiWebApp'));

  // instantiate service
  var JobService;
  beforeEach(inject(function (_JobService_) {
    JobService = _JobService_;
  }));

  it('should do something', function () {
    expect(!!JobService).toBe(true);
  });

});
