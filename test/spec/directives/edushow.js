'use strict';

describe('Directive: eduShow', function () {

  // load the directive's module
  beforeEach(module('landiWebApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<edu-show></edu-show>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the eduShow directive');
  }));
});
