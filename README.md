# README #

Client side code for Landi using AngularJS with API backend.

### Getting Started ###

安装grunt及bower全局命令：
```
npm install -g grunt-cli bower
```
安装相关依赖：
```
npm install
bower install
```
启动服务：
```
grunt serve
```

### Develop ###

[yeoman](http://yeoman.io/) 安装：
```
npm install -g yo
npm install -g generator-angular
```
相关generator命令查看：[https://github.com/yeoman/generator-angular]()

js的相关依赖使用bower管理，例如需要jquery(已经添加):
```
bower install jquery --save
```
`bower.json`描述了所有的js依赖，`bower install`会自动安装当中的改变。


### 更换主题 ###
当所有package 安装完毕一切准备就绪之后

在此处下载bootstrap 蓝色主题 并覆盖 bower_components\bootstrap\dist 下的 bootstrap.css 或 bootstrap.min.css
https://dl.dropboxusercontent.com/u/75858017/landi-theme/Cerulean/bootstrap.css
https://dl.dropboxusercontent.com/u/75858017/landi-theme/Cerulean/bootstrap.min.css
以下为红色主题
https://dl.dropboxusercontent.com/u/75858017/landi-theme/Simplex/bootstrap.css
https://dl.dropboxusercontent.com/u/75858017/landi-theme/Simplex/bootstrap.min.css
