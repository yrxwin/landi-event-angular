'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.JobService
 * @description
 * # JobService
 * Factory in the landiWebApp.
 */
angular.module('landiWebApp')
  .factory('JobService', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
