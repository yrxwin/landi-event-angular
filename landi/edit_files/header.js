'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('HeaderCtrl', function ($scope, AuthService) {
        var _this = this;
        _this.login = function () {
            AuthService.login(_this.credential);
        }
        $scope.$on('showLogin', function () {
            $('#loginPop').show();
        })
    });
