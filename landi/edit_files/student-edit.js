'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentProfileEditCtrl
 * @description
 * # StudentProfileEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('StudentEditCtrl', function ($scope) {
        $('.js-aboutme-btn-edit').on('click',function(){
            $('.js-aboutme-show').addClass('hidden');
            $('.js-aboutme-edit').removeClass('hidden');
        });
        $('.js-aboutme-cancel').on('click',function(){
            $('.js-aboutme-show').removeClass('hidden');
            $('.js-aboutme-edit').addClass('hidden');
        });


        var _this = this;
        _this.student = {
            "firstName":"Super",
            "lastName":"Star",
            "aboutme":"This is the student self-description, This is the student self-description,",
            "location":"30 Main St, Melrse, MA, 02176",
            "primEdu": "Northeastern University",
            "primMajor": "Mechanical Engineering",
            "email":"adis@mit.edu",
            "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "edus": [{"university":"Massachusetts Institute of Technology", "GPA": 3.0, "GPAFull": 4.0,
                "start":"2014-12-18T14:25:00.000Z" , "end":"2014-12-18T14:25:00.000Z",
                "major": "Mathematics", "level":1 },
                {"university":"Northeastern University", "GPA": 3.0, "GPAFull": 5.0,
                    "start":"2014-12-18T14:25:00.000Z" , "end":"2014-12-18T14:25:00.000Z",
                    "major": "Mechanical Engineering", "level":2 }],
            "courses":["Image processing","Algorithms","Linear Algebra"],
            "exps":[{"title": "PHP Developer", "company":"Google Inc.",
                "start":"2014-12-18T14:25:00.000Z", "end":"Present", "description":"This is an experience description"},
                {"title": "PHP Developer", "company":"Google Inc.",
                    "start":"2014-12-18T14:25:00.000Z", "end":"2014-12-18T14:25:00.000Z", "description":"This is an experience description"},
                {"title": "PHP Developer", "company":"Google Inc.",
                    "start":"2014-12-18T14:25:00.000Z", "end":"2014-12-18T14:25:00.000Z", "description":"This is an experience description"}],
            "skills":[{"name": "Java", "rate":4,
                "description":"This is an experience description"},
                {"name": "C#", "rate":3,
                    "description":"This is an experience description"},
                {"name": "C++", "rate":5,
                    "description":"This is an experience description"},
                {"name": "Swift", "rate":2,
                    "description":"This is an experience description"}],
            "projects":[{"name": "Java", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                "description":"This is a project description"},
                {"name": "C#", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is another project description"},
                {"name": "C++", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is a third experience description"},
                {"name": "Swift", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is a fourth project description"}],
            "saved":false
        };
        _this.newEdu = {"level":1};
        _this.newCourse = "";
        _this.newExp = {};
        _this.newSkill = {};
        console.log(_this.student)

        $scope.acadLevels = [
            {name: 'Bachelor', code: 1, abbr:'B.S.'},
            {name: 'Master', code: 2, abbr:'M.S.'},
            {name: 'Doctor', code: 3, abbr:'Ph.D.'},
            {name: 'Post Doc', code: 4, abbr:'Postdoc.'},
            {name: 'Unknown', code: 0, abbr:''}
        ];

        _this.addCourse = function(){
            if(_this.newCourse.length > 2){
                _this.student.courses.push(_this.newCourse);
                _this.newCourse = "";
            }
        };
        _this.removeCourse = function(course){
            _this.student.courses.splice(_this.student.courses.indexOf(course),1);
        };

        _this.removeExp = function(exp){
            _this.student.exps.splice(_this.student.exps.indexOf(exp),1);
        };
        _this.removeSkill = function(skill){
            _this.student.skills.splice(_this.student.skills.indexOf(skill),1);
        };

        _this.addEdu = function() {
            console.log(_this.newEdu);
            _this.student.edus.push(_this.newEdu);
            _this.newEdu = {"level":1};
        };
        _this.addExp = function() {
            console.log(_this.newExp);
            _this.student.exps.push(_this.newExp);
            _this.newExp = {};
        };
        _this.addSkill = function() {
            console.log(_this.newExp);
            _this.student.skills.push(_this.newSkill);
            _this.newSkill = {};
        };

        _this.clearNewEdu = function(){
            _this.newEdu = {"level":1};
        };
        _this.clearNewExp = function(){
            _this.newExp = {};
        };
        _this.clearNewSkill = function(){
            _this.newSkill = {};
        };
        _this.getAcadLevelAbbr = function(edu){
            switch(edu.level){
                case 1:
                    return "B.S.";
                case 2:
                    return "M.S.";
                case 3:
                    return "Ph.D.";
                case 4:
                    return "Post-doc";
                default:
                    return null;
            }
        };

        _this.getStudentName = function(){
            return _this.student.firstName + " " + _this.student.lastName;
        };

        _this.formatGPAStr = function(gpa){
            return gpa.toFixed(2);
        };
        _this.removeEdu = function(edu){
            _this.student.edus.splice(_this.student.edus.indexOf(edu),1);
        }


    });
