'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:NoAccessCtrl
 * @description
 * # NoAccessCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('NoAccessCtrl', function ($scope, $location) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
        var _this = this;
        _this.goBack = function(){
            $location.path('/');
        };
        _this.goHome = function(){
            $location.path('/');
        }
  });
