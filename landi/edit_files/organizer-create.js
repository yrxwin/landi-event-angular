'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:OrganizerCreateCtrl
 * @description
 * # OrganizerCreateCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('OrganizerCreateCtrl', function ($scope, $location) {
        var _this = this;
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];

        _this.organizer= {};
        _this.formSubmit = function() {
            console.log(_this.company);
            $location.path('/organizer/show');
        }
    });
