'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.CompanyService
 * @description
 * # CompanyService
 * Factory in the landiWebApp.
 */
angular.module('landiWebApp')
  .factory('CompanyService', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
