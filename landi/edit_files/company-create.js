'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanyCreateCtrl
 * @description
 * # CompanyCreateCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('CompanyCreateCtrl', function ($scope, $location) {
        var _this = this;
        $scope.awesomeThings = [
          'HTML5 Boilerplate',
          'AngularJS',
          'Karma'
        ];
        $scope.companyScales = [
            {name: ' < 50 Employees', code: 0},
            {name: '50 - 100 Employees', code: 1},
            {name: '100 - 200 Employees', code: 2},
            {name: '200 - 600 Employees', code: 3},
            {name: '600 - 1000 Employees', code: 4},
            {name: '1000+ Employees', code: 5}
        ];

        _this.company = {};
        _this.formSubmit = function() {
            console.log(_this.company);
            $location.path('/company/show');
        }
    });
