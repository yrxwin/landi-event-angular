'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:EventShowCtrl
 * @description
 * # EventShowCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('EventShowCtrl', function ($scope) {

        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        var _this = this;
        _this.event = {
            "title":"This is the event title",
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-16T14:25:00.000Z",
            "location":"30 Main St, Melrse, MA, 02176",
            "organizer":{"title":"MIT Ventureship club",
                        "url":"organizer/show"},
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "partComps": [{"title":"Samsung Electronics", "url":"http://www.samsung.com" },
                              {"title":"Cisco Telecommunication Company", "url":"http://www.samsung.com" },
                              {"title":"This is another company", "url":"http://www.samsung.com"}],
            "annos":[{"title":"Announcement1", "body":"This is announcement1"},
                {"title":"Announcement2", "body":"This is announcement2"},
                {"title":"Announcement3", "body":"This is announcement3"}],
            "saved":false,
            "attend":false
        };
        _this.searchStr = "";

        _this.getAttendStr = function(){
            if (_this.event.attend){
                return "Reserved";
            }else{
                return "Attend";
            }
        };
        _this.getSavedStr = function(){
            if (_this.event.saved){
                return "Unsave";
            }else{
                return "Save This Event";
            }
        };

        _this.attendClick = function() {
            if(!_this.event.attend){
                _this.event.attend = true;
                console.log("Participate clicked");
                //$location.path('/attendEventAction');
            }
        };

        _this.saveClick  = function() {
            _this.event.saved = !_this.event.saved;
            if(_this.job.saved){
                console.log("Save job clicked")
            }else{
                console.log("Unsave job clicked")
            }
        };

        _this.searchCompany = function(){
            if(_this.searchStr.length > 1 ){
                console.log("Search with _this.searchStr")
            }else{
                console.log("Search with no keyword but this event")
            }
        };
        _this.map = { center: { latitude: 45, longitude: -73 }, zoom: 8 };

    });
