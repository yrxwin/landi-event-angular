'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:loginModal
 * @description
 * # loginModal
 */
angular.module('landiWebApp')
  .directive('loginModal', ['AuthService', function (AuthService) {
        return {
            restrict: "A",
            scope: {
                modalVisible: "="
            },
            link: function (scope, element, attrs) {

                //Hide or show the modal
                scope.login = function (credential) {
                    AuthService.login(credential);
                }

                scope.showModal = function (visible) {
                    if (visible)
                    {
                        element.modal("show");
                    }
                    else
                    {
                        element.modal("hide");
                    }
                }

                //Check to see if the modal-visible attribute exists
                if (!attrs.modalVisible)
                {

                    //The attribute isn't defined, show the modal by default
                    scope.showModal(true);

                }
                else
                {

                    //Watch for changes to the modal-visible attribute
                    scope.$watch("modalVisible", function (newValue, oldValue) {
                        scope.showModal(newValue);
                    });

                    //Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
                    element.bind("hide.bs.modal", function () {
                        scope.modalVisible = false;
                        if (!scope.$$phase && !scope.$root.$$phase)
                            scope.$apply();
                    });

                }

            }
        };

  }]);
