'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:OrganizerEditCtrl
 * @description
 * # OrganizerEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('OrganizerEditCtrl', function ($scope, $location) {
        $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
        ];
        $('.js-aboutus-btn-edit').on('click',function(){
            $('.js-aboutus-show').addClass('hidden');
            $('.js-aboutus-edit').removeClass('hidden');
        });
        $('.js-aboutus-cancel').on('click',function(){
            $('.js-aboutus-show').removeClass('hidden');
            $('.js-aboutus-edit').addClass('hidden');
        });

        var _this = this;
        _this.organizer = {
            "name":"MIT Ventureships Club",
            "aboutus":"This is the organizer description, This is the event description, ",
            "location":"Melrose, MA",
            "extUrl": "http://ventureships.mit.edu",
            "type": "Non-profit",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "events": [{"title":"VentureShips Career Fair", "start": "2014-12-18T14:25:00.000Z", "end": "2014-12-18T14:25:00.000Z",
                "location":" World Trade Center, New York, NY, 123456", "editUrl":"/event/edit", "description": "This is an event description" },
                {"title":"Thank Giving Party", "start": "2014-12-18T14:25:00.000Z", "end": "2014-12-18T14:25:00.000Z",
                    "location":" World Trade Center, New York, NY, 123456", "editUrl":"/event/edit", "description": "This is another event description" }]
        };

        _this.getEventCountStr = function(){
            var num = _this.organizer.events.length;
            if (num > 1){
                return "Organized " + num + " events";
            }else{
                return "Organized " + num + " event";
            }
        };
        _this.createEvent = function() {
            console.log("Navigate to create a job");
            $location.path('/event/create');
        };

        _this.editEvent = function(event) {
            console.log("Navigate to edit an event with information from event entity");
            $location.path('/event/edit');
        };
        _this.removeEvent = function(event) {
            _this.organizer.events.splice(_this.organizer.events.indexOf(event),1);
        };
    });
