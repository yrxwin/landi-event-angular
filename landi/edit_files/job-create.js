'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:JobCreateCtrl
 * @description
 * # JobCreateCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('JobCreateCtrl', function ($scope, $location) {
        var _this = this;
        $scope.awesomeThings = [
          'HTML5 Boilerplate',
          'AngularJS',
          'Karma'
        ];

        $scope.jobTypes = [
            {name: 'Full-time', code: 0},
            {name: 'Part-time', code: 1},
            {name: 'Intern', code: 2},
            {name: 'Intern (Part-time)', code: 3},
            {name: 'Coop', code: 4}
        ];

        _this.job = {};
        _this.formSubmit = function() {
            console.log(_this.job);
            $location.path('/job/show');
        }
  });
