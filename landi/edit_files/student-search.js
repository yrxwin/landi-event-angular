'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentSearchCtrl
 * @description
 * # StudentSearchCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp').controller('StudentSearchCtrl', function ($scope) {
        $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];

    $scope.acadLevels = [
        {name: 'Bachelor', code: 1, abbr:'B.S.'},
        {name: 'Master', code: 2, abbr:'M.S.'},
        {name: 'Doctor', code: 3, abbr:'Ph.D.'},
        {name: 'Post Doc', code: 4, abbr:'Postdoc.'},
        {name: 'Unknown', code: 0, abbr:''}
    ];
    $scope.gradDates = [
        {name: 'All', code: 0},
        {name: 'Graduated', code: 1},
        {name: 'Spring 2015', code: 2},
        {name: 'Summer 2015', code: 3},
        {name: 'Fall 2015', code: 4},
        {name: 'Spring 2016', code: 5}
    ];

    var _this = this;
    _this.search = {
        "keyword":"Northeastern",
        // Location
        // Optional refining conditions returned in the JSON object
        "results":[{ "firstName":"Renze",
            "lastName":"Wang",
            "level":1,
            "primMajor":"Mechanical Engineering",
            "primEdu":"Cambridge University",
            "primGradDate":"Spring 2015",
            "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "url":"student/show"},
            { "firstName":"Renze",
                "lastName":"Wang",
                "level":1,
                "primMajor":"Mechanical Engineering",
                "primEdu":"Cambridge University",
                "primGradDate":"Spring 2015",
                "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "url":"student/show"},
            { "firstName":"Renze",
                "lastName":"Wang",
                "level":1,
                "primMajor":"Mechanical Engineering",
                "primEdu":"Cambridge University",
                "primGradDate":"Spring 2015",
                "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "url":"student/show"}
        ]
    };

    _this.refineSearch = function(){
        console.log("Do a refine search query/action");
    };

    _this.getAcadLevelAbbr = function(edu){
        switch(edu.level){
            case 1:
                return "B.S.";
            case 2:
                return "M.S.";
            case 3:
                return "Ph.D.";
            case 4:
                return "Post-doc";
            default:
                return null;
        }
    };

    _this.getStudentName = function(student){

        return student.firstName + " " + student.lastName
    };

    _this.loadMore = function(){
        console.log("load more search results");
    }
});
