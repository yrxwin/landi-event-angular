'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('MainCtrl', ['$scope', 'Group', function ($scope, Group) {
    $scope.groups = Group.query();
  }]);
