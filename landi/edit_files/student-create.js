'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentCreateCtrl
 * @description
 * # StudentCreateCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('StudentCreateCtrl', function ($scope, $location) {
        var _this = this;
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];

        _this.student = {};
        _this.student.edus = [];
        _this.student.courses = [];

        _this.newEdu = {"level":1};
        _this.newCourse = null;

        _this.addCourse = function() {
            console.log(_this.newCourse);
            if(_this.newCourse){
                _this.student.courses.push(_this.newCourse);
                _this.newCourse = null;
            }
        };
        $scope.acadLevels = [
            {name: 'Bachelor', code: 1, abbr:'B.S.'},
            {name: 'Master', code: 2, abbr:'M.S.'},
            {name: 'Doctor', code: 3, abbr:'Ph.D.'},
            {name: 'Post Doc', code: 4, abbr:'Postdoc.'},
            {name: 'Unknown', code: 0, abbr:''}
        ];
        _this.getAcadLevelAbbr = function(edu){
            switch(edu.level){
                case 1:
                    return "B.S.";
                case 2:
                    return "M.S.";
                case 3:
                    return "Ph.D.";
                case 4:
                    return "Post-doc";
                default:
                    return null;
            }
        };
        _this.formatGPAStr = function(gpa){
            return gpa.toFixed(2);
        };

        _this.removeEdu = function(edu) {
            _this.student.edus.splice(_this.student.edus.indexOf(edu),1);
        };
        _this.removeCourse = function(course) {
            _this.student.courses.splice(_this.student.edus.indexOf(course),1);
        };

        _this.addEdu = function() {
            console.log(_this.newEdu);
            _this.student.edus.push(_this.newEdu);
            _this.newEdu = {"level":1};
        };
        _this.clearNewEdu = function() {
            _this.newEdu = {"level":1};
        };

        _this.formSubmit = function() {
            console.log(_this.student);
            $location.path('/student/show');
        }
  });
