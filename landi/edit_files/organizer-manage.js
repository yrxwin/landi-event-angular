'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:OrganizerManageCtrl
 * @description
 * # OrganizerManageCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('OrganizerManageCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
