'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentProfileCtrl
 * @description
 * # StudentProfileCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('StudentShowCtrl', function ($scope) {
        $scope.awesomeThings = [
          'HTML5 Boilerplate',
          'AngularJS',
          'Karma'
        ];
        var _this = this;

        _this.student = {
            "firstName":"Super",
            "lastName":"Star",
            "aboutme":"This is the student self-description, This is the student self-description,",
            "location":"30 Main St, Melrse, MA, 02176",
            "primEdu": "Northeastern University",
            "primMajor": "Mechanical Engineering",
            "email":"adis@mit.edu",
            "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "edus": [{"university":"Massachusetts Institute of Technology", "GPA": 3.0, "GPAFull": 4.0,
                "start":"2014-12-18T14:25:00.000Z" , "end":"2014-12-18T14:25:00.000Z",
                "major": "Mathematics", "level":1 },
                {"university":"Northeastern University", "GPA": 3.0, "GPAFull": 5.0,
                    "start":"2014-12-18T14:25:00.000Z" , "end":"2014-12-18T14:25:00.000Z",
                    "major": "Mechanical Engineering", "level":2 }],
            "courses":["Image processing","Algorithms","Linear Algebra"],
            "exps":[{"title": "PHP Developer", "company":"Google Inc.",
                    "start":"2014-12-18T14:25:00.000Z", "end":"2014-12-18T14:25:00.000Z", "description":"This is an experience description"},
                {"title": "PHP Developer", "company":"Google Inc.",
                    "start":"2014-12-18T14:25:00.000Z", "end":"2014-12-18T14:25:00.000Z", "description":"This is an experience description"},
                {"title": "PHP Developer", "company":"Google Inc.",
                    "start":"2014-12-18T14:25:00.000Z", "end":"2014-12-18T14:25:00.000Z", "description":"This is an experience description"}],
            "skills":[{"name": "Java", "rate":4,
                "description":"This is an experience description"},
                {"name": "C#", "rate":3,
                    "description":"This is an experience description"},
                {"name": "C++", "rate":5,
                    "description":"This is an experience description"},
                {"name": "Swift", "rate":2,
                    "description":"This is an experience description"}],
            /*
            "projects":[{"name": "Java", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                "description":"This is a project description"},
                {"name": "C#", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is another project description"},
                {"name": "C++", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is a third experience description"},
                {"name": "Swift", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is a fourth project description"}],*/
            "saved":false,
            "processed":false
        };

        _this.getStudentName = function(){
            return _this.student.firstName + " " + _this.student.lastName;
        };

        _this.getSavedStr = function(){
            if (_this.student.saved){
                return "Unsave";
            }else{
                return "Save This Student";
            }
        };

        _this.getAcadLevelAbbr = function(edu){
            switch(edu.level){
                case 1:
                    return "B.S.";
                case 2:
                    return "M.S.";
                case 3:
                    return "Ph.D.";
                case 4:
                    return "Post-doc";
                default:
                    return null;
            }
        };

        _this.formatGPAStr = function(gpa){
            return gpa.toFixed(2);
        };

        _this.saveClick  = function() {
            _this.student.saved = !_this.student.saved;
            if(_this.student.saved){
                console.log("Save student clicked")
            }else{
                console.log("Unsave student clicked")
            }
        };
        _this.offerStudent = function(){
            _this.student.processed = true;
        };
        _this.rejectStudent = function(){
            _this.student.processed = true;
        }
    });
