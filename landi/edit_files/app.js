'use strict';

/**
 * @ngdoc overview
 * @name landiWebApp
 * @description
 * # landiWebApp
 *
 * Main module of the application.
 */
var app = angular.module('landiWebApp', [
  'ngCookies',
  'ngResource',
  'ngRoute',
  'restangular',
  'LocalStorageModule',
  'summernote',
  'ngAutocomplete',
  'flow',
  'ui.bootstrap',
  'mgcrea.ngStrap',
  'xeditable',
  'internationalPhoneNumber',
  'uiGmapgoogle-maps',
  'ui.bootstrap.datetimepicker',
  'validation',
  'ui.utils'
]);


app.run(function ($location, Restangular, AuthService, editableOptions) {
  Restangular.addFullRequestInterceptor(function (element, operation, route, url, headers, params, httpConfig) {
    headers['Authorization'] = 'Token ' + AuthService.getToken();
      console.log(headers)
    return {
      headers: headers
    };
  });

  Restangular.setErrorInterceptor(function (response, deferred, responseHandler) {
    if (response.config.bypassErrorInterceptor) {
      return true;
    } else {
      switch (response.status) {
        case 401:
          AuthService.logout();
          $location.path('/login');
          break;
        default:
            deferred.reject('serverError')
      }
      return false;
    }
  });

    editableOptions.theme = 'bs3';
});

app.config(function ($routeProvider, $locationProvider, uiGmapGoogleMapApiProvider, $validationProvider, RestangularProvider) {

  var expression = {
    required: function (value) {
      return !!value;
    },
    url: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/,
    email: /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
    number: /^\d+$/,
    password: function (value) {
      return (value.length >= 6)
    }
  };

  RestangularProvider.setRequestSuffix('/');
  RestangularProvider.setBaseUrl('/api');

  var partialsDir = '../partials';

  /*var redirectIfAuthenticated = function(route) {
    return function($location, $q, AuthService) {

      var deferred = $q.defer();

      if (AuthService.isAuthenticated()) {
        deferred.reject();
        $location.path(route);
      } else {
        deferred.resolve()
      }

      return deferred.promise;
    }
  };*/

  var redirectIfNotAuthenticated = function(route, redirect) {
    return function($location, $q, AuthService) {

      var deferred = $q.defer();

      if (! AuthService.isAuthenticated()) {
           deferred.reject();
           // $rootScope.$broadcast('showLogin');
           $location.path(route).search({redirect: redirect});
      } else {
        deferred.resolve()
      }

      return deferred.promise;

    }
  };

  var defaultMsg = {
    required: {
      error: 'This field is required.',
      success: 'It\'s Required'
    },
    url: {
      error: 'This should be Url',
      success: 'It\'s Url'
    },
    email: {
      error: 'Invalid Email.',
      success: 'It\'s Email'
    },
    number: {
      error: 'This should be Number',
      success: 'It\'s Number'
    },
    password: {
      error: 'Password should be at least 6 characters',
      success: 'Good'
    }
  };
  $validationProvider.setExpression(expression).setDefaultMsg(defaultMsg);
  $validationProvider.showSuccessMessage = false;
  $validationProvider.setErrorHTML(function (msg) {
    return "<label class='control-label has-error'>" + msg + "</label>";
  });

  uiGmapGoogleMapApiProvider.configure({
    v: '3.17',
    libraries: 'weather,geometry,visualization'
  });
  $locationProvider.html5Mode(true);
  $routeProvider
    .when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })
    .when('/login', {
      templateUrl: 'views/login.html',
      controller: 'LoginCtrl',
      controllerAs:'lc'
    })
    .when('/about', {
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl'
    })
    .when('/aboutus', {
      templateUrl: '../views/about-us.html',
      controller: 'AboutusCtrl'
    })
    .when('/event/create', {
      templateUrl: 'views/event-create.html',
      controller: 'EventCreateCtrl',
      controllerAs: 'ec'
      /*resolve: {
        redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login')
      }*/
    })
    .when('/company/manage', {
      templateUrl: 'views/company-manage.html',
      controller: 'CompanyManageCtrl',
      controlllerAs: 'cm'
      /*resolve: {
        redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login')
      }*/
    })
    .when('/company/edit', {
      templateUrl: 'views/company-edit.html',
      controller: 'CompanyEditCtrl',
      controllerAs: 'ce',
      resolve: {
        redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login', '/company/edit')
      }
    })
    .when('/company/show', {
      templateUrl: '../views/company-show.html',
      controller: 'CompanyProfileCtrl',
      controllerAs: 'cp'
    })
    .when('/company/search', {
      templateUrl: '../views/company-search.html',
      controller: 'CompanySearchCtrl',
      controllerAs: 'cs'
    })
    .when('/contactus', {
      templateUrl: 'views/contact-us.html',
      controller: 'ContactUsCtrl',
      controllerAs: 'cu'
    })
    .when('/event/show', {
      templateUrl: 'views/event-show.html',
      controller: 'EventShowCtrl',
      controllerAs: 'ep'
    })
    .when('/event/show-comp', {
      templateUrl: 'views/event-company.html',
      controller: 'EventCompanyCtrl',
      controllerAs: 'epComp'
    })
    .when('/event/edit', {
      templateUrl: 'views/event-edit.html',
      controller: 'EventEditCtrl',
      controllerAs: 'ee'
    })
    .when('/job/create', {
      templateUrl: 'views/job-create.html',
      controller: 'JobCreateCtrl',
      controllerAs: 'jc'
    })
    .when('/job/show', {
      templateUrl: 'views/job-show.html',
      controller: 'JobShowCtrl',
      controllerAs: 'jp'
    })
    .when('/job/edit', {
      templateUrl: 'views/job-edit.html',
      controller: 'JobEditCtrl',
      controllerAs: 'je'
    })
    .when('/organizer/show', {
      templateUrl: 'views/organizer-show.html',
      controller: 'OrganizerShowCtrl',
      controllerAs: 'op'
    })
    .when('/organizer/edit', {
      templateUrl: 'views/organizer-edit.html',
      controller: 'OrganizerEditCtrl',
      controllerAs: 'oe'
    })
    .when('/organizer/manage', {
      templateUrl: 'views/organizer-manage.html',
      controller: 'OrganizerManageCtrl',
      controllerAs: 'om'
    })
    .when('/search', {
      templateUrl: 'views/search.html',
      controller: 'SearchCtrl',
      controllerAs: 'sear'
    })
    .when('/user/create', {
      templateUrl: '../views/user-create.html',
      controller: 'UserCreateCtrl',
      controllerAs: 'uc'
    })
    .when('/student/edit', {
      templateUrl: 'views/student-edit.html',
      controller: 'StudentEditCtrl',
      controllerAs: 'se'

    })

     /* .when('/student/company', {
     templateUrl: 'views/student-company.html',
     controller: 'StudentCompanyCtrl',
     controllerAs: 'spComp'
     }) */
    .when('/student/show', {
      templateUrl: 'views/student-show.html',
      controller: 'StudentShowCtrl',
      controllerAs: 'sp'
    })
    .when('/student/manage', {
      templateUrl: 'views/student-manage.html',
      controller: 'StudentManageCtrl',
          controllerAs: 'sm'
    })
    .when('/student/create', {
      templateUrl: 'views/student-create.html',
      controller: 'StudentCreateCtrl',
      controllerAs: 'sc'
    })
    .when('/company/create', {
      templateUrl: 'views/company-create.html',
      controller: 'CompanyCreateCtrl',
      controllerAs: 'cc'
    })
    .when('/organizer/create', {
      templateUrl: 'views/organizer-create.html',
      controller: 'OrganizerCreateCtrl',
      controllerAs: 'oc'
    })
    .when('/student/search', {
      templateUrl: 'views/student-search.html',
      controller: 'StudentSearchCtrl',
          controllerAs: 'ss'
    })
    .when('/401', {
      templateUrl: 'views/no-access.html',
      controller: 'NoAccessCtrl',
      controllerAs: 'noAcc'
    })
    .otherwise({
      redirectTo: '/'
    });
});
