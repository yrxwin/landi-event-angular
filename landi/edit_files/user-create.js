'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentSignupCtrl
 * @description
 * # StudentSignupCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('UserCreateCtrl', function ($scope, $location, $anchorScroll, $validation, $http, localStorageService) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        var _this = this;
        _this.clicked = false;
        _this.user ={};
        _this.serverError = false;
        // Use integer code for identity
        // 0 for admin, 1 for student, 2 for company, 3 for organizer.
        _this.user.identity = null;
        _this.identityError = false;
        _this.setIdentity = function(identity){
          _this.user.identity = identity;
        };

        _this.isStudent = function(){
            if(_this.user.identity == 1){
                return true;
            }else{
                return false;
            }
        };
        _this.isCompany = function(){
            if(_this.user.identity == 2){
                return true;
            }else{
                return false;
            }
        };
        _this.isOrganizer = function(){
            if(_this.user.identity == 3){
                return true;
            }else{
                return false;
            }
        };

        _this.checkEmail = function() {
            $http.post('/api/verifyEmail', _this.user.email).success(function(data){
                if (data.user_exist) {

                }
            }).error(function(data){
                console.log(data)
            })
        }

        _this.submitForm = function (form) {
            $validation.validate(form)
                .success(function () {
                    if (!_this.user.identity) {
                        _this.identityError = true;
                        $location.hash('alert1');
                        $anchorScroll();
                        return;
                    }
                    switch (_this.user.identity){
                        case 1:
                            _this.user.first_name = 'first_name';
                            _this.user.last_name = 'last_name';
                            _this.user.high_degree = 'Phd';
                            // console.log(_this.user);
                            $http.post('/api/students/', _this.user).
                                success(function(data, status, headers, config) {
                                    $location.path('/student/create');
                                }).
                                error(function(data, status, headers, config) {
                                    _this.clicked = true;
                                    $location.hash('alert1');
                                    _this.serverError = true;
                                });
                            break;
                        case 2:
                            _this.user.company_name = 'company_name';
                            _this.user.contact_name = 'contact_name';
                            // console.log(_this.user);
                            $http.post('/api/companies/', _this.user).
                                success(function(data, status, headers, config) {
                                    $location.path('/company/create');
                                }).
                                error(function(data, status, headers, config) {
                                    _this.clicked = true;
                                    $location.hash('alert1');
                                    _this.serverError = true;
                                });
                            break;
                        case 3:
                            _this.user.organization_name = 'organization_name';
                            _this.user.contact_name = 'contact_name';
                            // console.log(_this.user);
                            $http.post('/api/organizers/', _this.user).
                                success(function(data, status, headers, config) {
                                    $location.path('/organizer/create');
                                }).
                                error(function(data, status, headers, config) {
                                    _this.clicked = true;
                                    $location.hash('alert1');
                                    _this.serverError = true;
                                });
                            break;
                        default:
                            _this.clicked = true;
                            $location.hash('alert1');
                            _this.serverError = true;
                    }

                })
                .error(function () {
                    _this.clicked = true;
                    $location.hash('alert1');
                    $anchorScroll();
                });

        };

        _this.recEvents = [[{"id":12341,"logo":"http://placekitten.com/4/300","title":"Event 1", "selected":false},
            {"id":12342,"logo":"http://placekitten.com/2/300","title":"Event 2","selected":false},
            {"id":12343,"logo":"http://placekitten.com/3/300","title":"Event 3","selected":false}],
            [{"id":12344,"logo":"http://placekitten.com/1/300","title":"Event 4","selected":false},
                {"id":12345,"logo":"http://placekitten.com/2/300","title":"Event 5","selected":false},
                {"id":12346,"logo":"http://placekitten.com/3/300","title":"Super long evnet 6","selected":false}],
            [{"id":12347,"logo":"http://placekitten.com/1/300","title":"Event 7","selected":false},
                {"id":12348,"logo":"http://placekitten.com/2/300","title":"Event 8","selected":false},
                {"id":12349,"logo":"http://placekitten.com/3/300","title":"Super long evnet 9","selected":false}],
            [{"id":12350,"logo":"http://placekitten.com/1/300","title":"Event 10"},
                {"id":12351,"logo":"http://placekitten.com/2/300","title":"Event 11"},
                {"id":12352,"logo":"http://placekitten.com/3/300","title":"Super long evnet 12"}]
        ];
        _this.selectedEvents = [];
        _this.eventsCarouselInterval = 2800;

        _this.selectEvent = function(event){
            if(_this.isStudent() || _this.isCompany()){
                event.selected = !event.selected;
                if(event.selected){
                    _this.selectedEvents.append(event.id);
                }else{
                    _this.selectedEvents.splice(_this.selectedEvents.indexOf(event.id),1);

                }
            }
        }
    });
