'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanySearchCtrl
 * @description
 * # CompanySearchCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp').controller('CompanySearchCtrl', function ($scope) {
        $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];
    var _this = this;
    _this.search = {
        "keyword":"Northeastern",
        "searchEvent":"MIT Career Fair Spring 2015",
        // Optional refining conditions returned in the JSON object
        "results":[{ "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "location":"Cambridge, MA"}
        ]
    };
    _this.refineSearch = function (){
        console.log("Search results refined.")
    }

});
