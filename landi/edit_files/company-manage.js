'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanyManageCtrl
 * @description
 * # CompanyManageCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('CompanyManageCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
