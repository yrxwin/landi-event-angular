'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanyProfileCtrl
 * @description
 * # CompanyProfileCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('CompanyProfileCtrl', function ($scope) {
        $scope.awesomeThings = [
          'HTML5 Boilerplate',
          'AngularJS',
          'Karma'
        ];

        var _this = this;
        _this.company = {
            "name":"Samsung Electronics Inc.",
            "aboutus":"This is the organizer description, This is the event description, ",
            "location":"Cambridge, MA",
            "extUrl": "http://www.samsung.com",
            "industry": "Manufacturing",
            "scale": 5,
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "jobs": [{"title":"PHP Developer", "locations":["Cambridge, MA"],
                "description": "This is a job description", "url":"job/show" },
                {"title":"Project Manager", "locations":["Cambridge, MA", "Boston, MA"],
                    "description": "This is another job description", "url":"job/show" },
                {"title":"Project Manager", "locations":["Cambridge, MA", "Boston, MA", "China"],
                    "description": "This is a third job description", "url":"job/show" }],
            "saved":false
        };

        $scope.companyScales = [
            {name: ' < 50 Employees', code: 0},
            {name: '50 - 100 Employees', code: 1},
            {name: '100 - 200 Employees', code: 2},
            {name: '200 - 600 Employees', code: 3},
            {name: '600 - 1000 Employees', code: 4},
            {name: '1000+ Employees', code: 5}
        ];

        _this.getScaleStr = function(){
            switch(_this.company.scale) {
                case 0:
                    return "< 50 Employees";
                case 1:
                    return "50 - 100 Employees";
                case 2:
                    return "100 - 200 Employees";
                case 3:
                    return "200 - 600 Employees";
                case 4:
                    return "600 - 1000 Employees";
                case 5:
                    return "1000+ Employees";
                default:
                    return "unavailabel";
            }
        };

        _this.getLocationStr = function(locations){
            if (locations.length > 0){
                console.log("locations count > 0");
                var locationStr = "";
                for (var i = 0; i < locations.length; i++) {
                    if(i == locations.length - 1){
                        locationStr += locations[i];
                    }else{
                        locationStr += locations[i] +", ";
                    }
                }
                return locationStr;
            }else{
                return "Unavailable";
            }
        };

        _this.getSavedStr = function(){
            if (_this.company.saved){
                return "Unsave";
            }else{
                return "Save This Company";
            }
        };

        _this.saveClick  = function() {
            _this.company.saved = !_this.company.saved;
            if(_this.company.saved){
                console.log("Save company clicked")
            }else{
                console.log("Unsave company clicked")
            }
        };
    });
