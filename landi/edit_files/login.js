'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('LoginCtrl', function ($scope, AuthService, $location, $routeParams) {
        var _this = this;
        _this.credential = {};
        _this.showError = false;
        // console.log($routeParams.redirect);
        var validationPassed = function() {
            console.log(_this.credential);
            var loginPromise = AuthService.login(_this.credential);
            loginPromise.then(function(data){
                _this.showError = false;
                console.log('success');
                $location.path($routeParams.redirect).search({});
            }, function(status) {
                _this.showError = true;
                switch (status) {
                    case 'noMatch':
                        _this.errorInfo = "Email and password doesn't match";
                        break;
                    case 'serverError':
                        _this.errorInfo = "Server Error";
                        break;
                    default :
                        _this.errorInfo = "Something wrong"
                }
            })
        };

        $('.ui.form')
            .form({
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'This field is required'
                        },
                        {
                            type: 'email',
                            prompt: 'Please enter valid email'
                        }
                    ]
                }
            },
            {
                inline: true,
                on: 'blur',
                onSuccess: validationPassed
            });
  });
