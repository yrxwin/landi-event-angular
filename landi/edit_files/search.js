'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp').controller('SearchCtrl', function ($scope) {
    //$('.ui.accordion').accordion();
    $('.ui.dropdown').dropdown();
    $('.ui.checkbox').checkbox();
    //$('#eventStart').datetimepicker();

    var _this = this;
    _this.search = {
        "searchType":1,
        "type":1,
        "keyword":"Electrical engineering",
        "inEvent":"MIT Career Fair Spring 2015",
        "degree":"",
        "location":"Cambridge, MA",
        "positionTypes":[
            {"code": 1, "name": "Full-time", "selected": true},
            {"code": 2, "name": "Part-time", "selected": true},
            {"code": 3, "name": "Intern", "selected": false},
            {"code": 4, "name": "Other", "selected": false}
        ],
        // Optional refining conditions returned in the JSON object
        "companyResults":[{ "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "description":"this is a company description, this is a company description, this is a company description, ",
            "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "description":"this is a company description, this is a company description, this is a company description, ",
                "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "description":"this is a company description, this is a company description, this is a company description, ",
                "location":"Cambridge, MA"},
            { "name":"Google Inc.",
                "url":"company/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "description":"this is a company description, this is a company description, this is a company description, ",
                "location":"Cambridge, MA"}
        ],
        "organizerResults":[{ "name":"MIT Asian Career Fair",
            "url":"organizer/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "description":"this is a company description, this is a company description, this is a company description, ",
            "location":"Cambridge, MA"}],
        "eventResults":[{ "name":"MIT Spring 2015 Career Fair",
            "url":"event/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "organizer":{"name":"MIT Asian Career Fair"},
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-16T14:25:00.000Z",
            "location":"Cambridge, MA"},
            { "name":"MIT Spring 2015 Career Fair",
                "url":"event/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "organizer":{"name":"MIT Asian Career Fair"},
                "description":"This is the event description, This is the event description, ",
                "start":"2014-12-16T14:25:00.000Z",
                "end":"2014-12-16T14:25:00.000Z",
                "location":"Cambridge, MA"},
            { "name":"MIT Spring 2015 Career Fair",
                "url":"event/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "organizer":{"name":"MIT Asian Career Fair"},
                "description":"This is the event description, This is the event description, ",
                "start":"2014-12-16T14:25:00.000Z",
                "end":"2014-12-16T14:25:00.000Z",
                "location":"Cambridge, MA"},
            { "name":"MIT Spring 2015 Career Fair",
                "url":"event/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "organizer":{"name":"MIT Asian Career Fair"},
                "description":"This is the event description, This is the event description, ",
                "start":"2014-12-16T14:25:00.000Z",
                "end":"2014-12-16T14:25:00.000Z",
                "location":"Cambridge, MA"},
            { "name":"MIT Spring 2015 Career Fair",
                "url":"event/show",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "organizer":{"name":"MIT Asian Career Fair"},
                "description":"This is the event description, This is the event description, ",
                "start":"2014-12-16T14:25:00.000Z",
                "end":"2014-12-16T14:25:00.000Z",
                "location":"Cambridge, MA"}
        ],
        "jobResults":[
            {"title":"Professional Magical Engineer",
                "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                    "logo":"http://semantic-ui.com/images/wireframe/image.png",
                    "industry":"Manufacturing",
                    "contactName":"someone",
                    "contactEmail":"someone@samsung.com"},
                "type":1,
                "url":"job/show",
                "description":"this is a job description, this is a job description, this is a job description, ",
                "dueDate":"2014-12-16T14:25:00.000Z",
                "locations":["location1, LC","location2., LC","location3., LC"],
                "expired":true,
                "applied":false,
                contactName:null,
                contactEmail:null
            },
            {"title":"Professional Magical Engineer",
                "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                    "logo":"http://semantic-ui.com/images/wireframe/image.png",
                    "industry":"Manufacturing",
                    "contactName":"someone",
                    "contactEmail":"someone@samsung.com"},
                "description":"this is a job description, this is a job description, this is a job description, ",
                "type":1,
                "url":"job/show",
                "dueDate":"2014-12-16T14:25:00.000Z",
                "locations":["location1, LC","location2., LC","location3., LC"],
                "expired":true,
                "applied":false,
                contactName:null,
                contactEmail:null
            },
            {"title":"Professional Magical Engineer",
                "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                    "logo":"http://semantic-ui.com/images/wireframe/image.png",
                    "industry":"Manufacturing",
                    "contactName":"someone",
                    "contactEmail":"someone@samsung.com"},
                "type":2,
                "url":"job/show",
                "description":"this is a job description, this is a job description, this is a job description, ",
                "dueDate":"2014-12-16T14:25:00.000Z",
                "locations":["location1, LC","location2., LC","location3., LC"],
                "expired":true,
                "applied":false,
                contactName:null,
                contactEmail:null
            }
        ]
    };
    _this.hasDegree = function(){
        return _this.search.degree!= null && _this.search.degree.length > 0
    };
    _this.removeDegree = function(){
        _this.search.degree = null;
        console.log("Do a search without degree information")
    };
    _this.hasEvent = function(){
        return _this.search.inEvent!= null && _this.search.inEvent.length> 0;
    };
    _this.removeEvent = function(){
        _this.search.inEvent = null;
        console.log("Do a search without event information")
    };
    _this.addSearchInEvent = function(){
        if(_this.tmpInEvent != null && _this.tmpInEvent.length > 1){
            _this.search.inEvent = _this.tmpInEvent;
            _this.tmpInEvent = null;
        }
    };
    _this.hasLocation = function(){
        return _this.search.location!= null && _this.search.location.length> 0;
    };
    _this.removeLocation = function(){
        _this.search.location = null;
        console.log("Do a search without location information")
    };
    _this.addSearchLocation = function(){
        if(_this.tmpLocation != null && _this.tmpLocation.length > 1){
            _this.search.location = _this.tmpLocation;
            _this.tmpLocation = null;
            console.log("searchLocation Added")
        }
    };
    _this.hasPositionTypes = function(){
        if(_this.search.type == 1){
            return _this.search.positionTypes!= null && _this.search.positionTypes.length > 0;
        }else{
            return false;
        }
    };
    _this.togglePositionType = function(positionType){
        console.log(positionType.selected)
    };
    _this.removePositionType = function(positionType){
        positionType.selected = false;
        console.log("Search with new position type requirements");
    };
    _this.getPositionTypeStr = function(typeCode){
        switch(typeCode){
            case 1:
                return "Full-time";
            case 2:
                return "Part-time";
            case 3:
                return "Intern";
            case 4:
                return "Others";
            default:
                return "Unknown";
        }
    };
    /*
    _this.addPositionType = function (typeCode){
        var containLabel = false;
        _this.search.positionTypes.forEach(function (positionType) {
            if (positionType.code = typeCode) {
                containLabel = true;
                return;
            }
        });
        if(!containLabel){
            _this.search.positionTypes.append(typeCode);
            console.log("Do a search based on the new refined condition")
        }
    };*/

    _this.positionTypes = [
        {code: 1, name: 'Full-time'},
        {code: 2, name: 'Part-time'},
        {code: 3, name: 'Intern'},
        {code: 4, name: 'Other'}
    ];
    _this.getSearchTypeStr = function(){
        switch(_this.search.type){
            case 1:
                return "Job";
            case 2:
                return "Company";
            case 3:
                return "Event";
            case 4:
                return "Organizer";
            default:
                return "Job";
        }
    };
    _this.getSearchDegreeStr = function(){
        if(this.search.degree!=null){
            switch(this.search.degree){
                case "BS":
                    return "Bachelor of Science";
                case "BA":
                    return "Bachelor of Arts";
                case "MS":
                    return "Master of Science";
                case "MA":
                    return "Master of Arts";
                case "Phd":
                    return "Doctorate";
                case "Other":
                    return "Others";
                default:
                    return null;
            }
        }else{ return null}
    };

    _this.refineSearchDegree = function(degree){
        _this.search.degree = degree.value;
        console.log("Also do a search based on new degree.value")
    };
    _this.eventDateRanges = [
        {code: 0, text: 'All'},
        {code: 1, text: 'In one weeks'},
        {code: 2, text: 'In two weeks'},
        {code: 3, text: 'Within a month'}
    ];

    _this.setEventDateRange = function (dateRange){
       _this.search.dateRange = dateRange;
    };
    _this.hasDateRange = function(){
        return _this.search.dateRange!= null && _this.search.dateRange.text.length> 0;
    };
    _this.removeLocation = function(){
        _this.search.dateRange = null;
        console.log("Do a search without dateRange information")
    };

    _this.setSearchType = function(type){
        _this.search.searchType = type;
    };
    _this.isSearchingJobs = function(){
        return _this.search.type == 1;
    };

    _this.isSearchingCompanies = function(){
        return _this.search.type == 2;
    };

    _this.isSearchingEvents = function(){
        return _this.search.type == 3;
    };
    _this.isSearchingOrganizers = function(){
        return _this.search.type == 4;
    };
    _this.getResultsCount = function(){
        switch(_this.search.type){
            case 1:
                return _this.search.jobResults.length;
            case 2:
                return _this.search.companyResults.length;
            case 3:
                return _this.search.eventResults.length;
            case 4:
                return _this.search.organizerResults.length;
            default:
                return _this.search.jobResults.length;
        }
    };

    // --------- Methods for search job cards -------------

    _this.getContactName = function(job){
        if(job.contactName){
            return job.contactName;
        }else{
            return job.company.contactName;
        }
    };
    _this.getContactEmail = function(job){
        if(job.contactEmail){
            return job.contactEmail;
        }else{
            return job.company.contactEmail;
        }
    };
    _this.getAppliedStr = function(job){
        if (job.applied){
            return "Applied";
        }else{
            return "Apply";
        }
    };

    _this.applyClick = function(job) {
        if(!job.applied){
            job.applied = true;
            console.log("Apply button clicked");
            $location.path('/DoApplicationAction');
        }
    };

    // --------- Methods for search company cards -------------
    // Not much needed for now.
    // --------- Methods for search event cards -------------
    _this.getAttendStr = function(event){
        if (event.attend){
            return "Reserved";
        }else{
            return "Attend";
        }
    };

    _this.attendClick = function(event) {
        if(!event.attend){
            event.attend = true;
            console.log("Participate clicked");
            //$location.path('/attendEventAction');
        }
    };

    _this.doSearch = function(){
        if(_this.searchKeyword!=null && _this.searchKeyword.length >0){
            switch(_this.search.searchType){
                case 1:
                    _this.search.keyword = _this.searchKeyword;
                    console.log("New Search Jobs");
                case 2:
                    console.log("New Search Companies");
                case 3:
                    console.log("New Search Events");
                case 4:
                    console.log("New Search Organizers");
                default:
                    console.log("New Search Jobs");
            }
        }
    };
    _this.loadMore = function(){
        console.log("load more results here based on _this.search.type")
    };



    _this.all = {value: 'All', text: 'All'};
    _this.degrees = [
        {value: 'BS', text: 'Bachelor of Science'},
        {value: 'BA', text: 'Bachelor of Arts'},
        {value: 'MS', text: 'Master of Science'},
        {value: 'MA', text: 'Master of Arts'},
        {value: 'Phd', text: 'Doctorate'},
        {value: 'Other', text: 'Others'}
    ]
});
