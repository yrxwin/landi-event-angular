'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:JobEditCtrl
 * @description
 * # JobEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('JobEditCtrl', function ($scope) {
        $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
        ];
        $('.js-description-btn-edit').on('click', function () {
            $('.js-description-show').addClass('hidden');
            $('.js-description-edit').removeClass('hidden');
        });
        $('.js-description-btn-cancel').on('click', function () {
            $('.js-description-show').removeClass('hidden');
            $('.js-description-edit').addClass('hidden');
        });
        $('.js-responsibility-btn-edit').on('click', function () {
            $('.js-responsibility-show').addClass('hidden');
            $('.js-responsibility-edit').removeClass('hidden');
        });
        $('.js-responsibility-btn-cancel').on('click', function () {
            $('.js-responsibility-show').removeClass('hidden');
            $('.js-responsibility-edit').addClass('hidden');
        });
        var _this = this;
        var newLocation = "";
        _this.job = {
            "title":"Professional Magical Engineer",
            "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                "url":"company/edit",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "contactName":"someone",
                "contactEmail":"someone@samsung.com"},
            "type":0,
            "applicants": [{"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                "url":"student/show","status":1, "primMajor": "Computer Science"},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                    "url":"student/show","status":2, "primMajor": "Magic Major"},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                    "url":"student/show","status":4, "primMajor": "Magic Major"}],
            "dueDate":"2014-12-16T14:25:00.000Z",
            "description":"This is a job description \n, This is a job description, This is a job description,",
            "responsibility":"This is a job responsibility \n, This is a job responsibility, This is a job responsibility,",
            "locations":["location1, LC","location2., LC","location3., LC"],
            "expired":false,
            "saved":false,
            "applied":false,
            contactName:null,
            contactEmail:null
        };

        $scope.jobTypes = [
            {name: 'Full-time', code: 0},
            {name: 'Part-time', code: 1},
            {name: 'Intern', code: 2},
            {name: 'Intern (Part-time)', code: 3},
            {name: 'Coop', code: 4}
        ];
        _this.getJobTypeStr = function(){
            switch(_this.job.type){
                case 0:
                    return "(Full-time)";
                case 1:
                    return "(Part-time)";
                case 2:
                    return "(Intern)";
                case 3:
                    return "(Intern [part-time])";
                case 4:
                    return "(Coop)";
                default:
                    return null;
            }
        };
        _this.getApplicantMajor = function(applicant){
            if(applicant.primMajor){
                return "(" + applicant.primMajor +")"
            }else{
                return null;
            }
        };
        _this.offerApplicant = function(applicant){
          applicant.status = 4;
            // Do correponding email actions
        };
        _this.rejectApplicant = function(applicant){
            applicant.status = 5;
            // Do correponding email actions
        };
        _this.canOffer = function(applicant){
            // applicant's status 1, applied, 2 reviewed, 3, interview scheduled,  4 offered 5 rejected
            return applicant.status < 4
        };
        _this.removeLocation = function(location){
            _this.job.locations.splice(_this.job.locations.indexOf(location),1);
        };
        _this.addNewLocation = function(){
            if(_this.newLocation.length > 1){
                _this.job.locations.push(_this.newLocation);
                _this.newLocation = "";
            }
        };
        _this.getContactNameStr= function(){
            if(_this.job.contactName){
                return _this.job.contactName;
            }else{
                return _this.job.company.contactName;
            }
        };
        _this.getContactEmailStr= function(){
            if(_this.job.contactEmail){
                return _this.job.contactEmail;
            }else{
                return _this.job.company.contactEmail;
            }
        };
        _this.removeJob = function(){
            console.log("Should remove this job")
        };
        _this.endJob = function(){
            _this.job.expired = true;
        };
        _this.reactivateJob = function(){
            _this.job.expired = false;
        }
    });
