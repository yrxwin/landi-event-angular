'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:EventEditCtrl
 * @description
 * # EventEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('EventEditCtrl', function ($scope,$location) {
        $(document).ready(function () {
            $('.js-event-details-btn-edit').on('click', function () {
                $('.js-event-details-show').addClass('hidden');
                $('.js-event-details-edit').removeClass('hidden');
            });
            $('.js-event-details-btn-cancel').on('click', function () {
                $('.js-event-details-show').removeClass('hidden');
                $('.js-event-details-edit').addClass('hidden');
            });
        });
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        var _this = this;
        _this.shouldEmail = false;
        _this.newAnnoBody = "";
        _this.newAnnoTitle = "";
        _this.event = {
            "title":"Samsung Info Session",
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-18T14:25:00.000Z",
            "location":"30 Main St, Melrse, MA, 02176",
            "organizer":{"title":"MIT Ventureship club",
                "url":"organizer/show"},
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "partComps": [{"name":"Samsung Electronics", "url":"company/show","approved":true },
                {"name":"Cisco Telecommunication Company", "url":"company/show","approved":true },
                {"name":"This is another company", "url":"company/show","approved":false }],
            "partStus": [{"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                            "url":"student/show","approved":true},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                            "url":"student/show","approved":false},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                            "url":"student/show","approved":false}],
            "annos":[{"title":"Announcement1", "body":"This is announcement1"},
                {"title":"Announcement2", "body":"This is announcement2"},
                {"title":"Announcement3", "body":"This is announcement3"}]
        };

        _this.saveEventDetails = function(){
            console.log("event details should be saved here.")
        };

        _this.removeAnno = function(anno){
            _this.event.annos.splice(_this.event.annos.indexOf(anno),1);
        };

        _this.addAnno = function(){
            var newAnno = {};
            newAnno.title = _this.newAnnoTitle;
            newAnno.body = _this.newAnnoBody;
            _this.event.annos.push(newAnno);
            if(_this.shouldEmail){
                console.log("Sending an email using \"newAnno\"")
            }
            _this.newAnnoTitle = "";
            _this.newAnnoBody = "";
        };

        _this.approveComp = function(company){
            company.approved = true;
            // Also send email to notify that company for approval of participating an event
        };
        _this.disapproveComp = function(company){
            company.approved = false;
            // Also send email
        };
        _this.approveStudent = function(student){
            student.approved = true;
            // Also send email
        };
        _this.disapproveStudent = function(student){
            student.approved = false;
            // Also send email
        };
        _this.approveAllStudents = function(){
            _this.event.partStus.forEach(function(student) {
                if(!student.approved){
                    student.approved = true;
                }
            });
        };

        _this.searchStudent = function(){
            console.log("Searching students");
            $location.path('/student/search');
        };
        _this.searchCompany = function(){
            console.log("Searching companies");
            $location.path('/company/search');
        }
    });
