'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:AboutusCtrl
 * @description
 * # AboutusCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('AboutusCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
