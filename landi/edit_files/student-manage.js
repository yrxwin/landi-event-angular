'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentManageCtrl
 * @description
 * # StudentManageCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('StudentManageCtrl', function ($scope, $location) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        var _this = this;
        _this.manage = {
            "notifications": [
                {"title": "This is topic 1",
                    "body": "This is body 1",
                    "sender":"Tingtao Zhou",
                    "senderImg": "http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "isNew": true},
                {"title": "This is topic 1",
                    "body": "This is body 1",
                    "sender":"MIT ACF",
                    "senderImg": "http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "isNew": true},
                {"title": "This is topic 1",
                    "body": "This is body 1",
                    "sender":"Tingtao Zhou",
                    "senderImg": "http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "isNew": true},
                {"title": "This is topic 1",
                    "body": "This is body 1",
                    "sender":"Zeqing Zhang",
                    "senderImg": "http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "isNew": true},
                {"title": "This is topic 1",
                    "body": "This is body 1",
                    "sender":"Renze Wang",
                    "senderImg": "http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "isNew": false},
                {"title": "This is topic 1",
                    "body": "This is body 1",
                    "sender":"MIT VentureShips Club",
                    "senderImg": "http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "isNew": false},
                {"title": "This is topic 1",
                    "body": "This is body 1",
                    "sender":"Unknown",
                    "senderImg": "http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "isNew": false}

            ],
            "newsFeeds": [
                {"title": "This is topic 1",
                    "body": "This is body 1"},
                {"title": "This is topic 2",
                    "body": "This is body dfsfsd 2"},
                {"title": "This is topic 3",
                    "body": "This is body 3"},
                {"title": "This is topic 4",
                    "body": "This is body 4"},
                {"title": "What happend to you 5",
                    "body": "This is body 5"}
            ],
            "appliedJobs": [
                {"title": "Process Engineer",
                    "companyName":"Samsung",
                    "url": "job/show",
                    "status":3,
                    "isNew": true},
                {"title": "iOS Developer",
                    "companyName":"Apple",
                    "url": "job/show",
                    "status":2,
                    "isNew": true},
                {"title": "Material Testing Technician",
                    "companyName":"3M",
                    "url": "job/show",
                    "status":2,
                    "isNew": false},
                {"title": "Customer Representative",
                    "companyName":"AT&T",
                    "url": "job/show",
                    "status":1,
                    "isNew": false},
                {"title": "Service Technician",
                    "companyName":"AT&T",
                    "url": "job/show",
                    "status":3,
                    "isNew": false},
                {"title": "Product Quality Manager",
                    "companyName":"AT&T",
                    "url": "job/show",
                    "status":3,
                    "isNew": false}
            ],
            "savedJobs": [
                {"title": "Product Quality Manager",
                    "companyName":"AT&T",
                    "url": "job/show"},
                {"title": "Product Quality Manager",
                    "companyName":"AT&T",
                    "url": "job/show"},
                {"title": "Product Quality Manager",
                    "companyName":"AT&T",
                    "url": "job/show"},
                {"title": "Product Quality Manager",
                    "companyName":"AT&T",
                    "url": "job/show"},
                {"title": "Product Quality Manager",
                    "companyName":"AT&T",
                    "url": "job/show"}
            ],
            "attendingEvents": [
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA",
                    "isNew":true},
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA",
                    "isNew":true},
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA",
                    "isNew":true},
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA",
                    "isNew":false},
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA",
                    "isNew":false}
            ],
            "savedEvents": [
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA"},
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA"},
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA"},
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA"},
                {"title":"This is the name of Event 1",
                    "start":"2014-12-16T14:25:00.000Z",
                    "end":"2014-12-16T14:25:00.000Z",
                    "url":"event/show",
                    "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                    "location":"Cambridge, MA"}
            ],
            "savedCompanies":[
                {"name":"Samsung Electronics Inc", "url":"company/show"},
                {"name":"Apple Inc", "url":"company/show"},
                {"name":"Nortel the Company", "url":"company/show"},
                {"name":"Ebay Paypal, Boston", "url":"company/show"},
                {"name":"Forever Happnies Group", "url":"company/show"}
            ]

        };

        // Get counts methods

        _this.getNewNotificationCount = function () {
            var count = 0;
            _this.manage.notifications.forEach(function (message) {
                if (message.isNew) {
                    count += 1;
                }
            });
            return count
        };
        _this.getNewJobApplicationCount = function () {
            var count = 0;
            _this.manage.appliedJobs.forEach(function (job) {
                if (job.isNew) {
                    count += 1;
                }
            });
            return count
        };
        _this.getNewEventUpdateCount = function () {
            var count = 0;
            _this.manage.attendingEvents.forEach(function (event) {
                if (event.isNew) {
                    count += 1;
                }
            });
            return count
        };

        // ng-show methods.
        _this.showNewMessageBadge = function(){
            return _this.getNewNotificationCount() > 0;
        };
        _this.showNewJobUpdateBadge = function(){
            return _this.getNewJobApplicationCount() > 0;
        };

        _this.showNewNotificationBadge = function(){
            return _this.getNewNotificationCount() > 0;
        };
        _this.showJobStatusReviewed = function(job){
            if(job.status == 1){return true;}
            else{return false;}
        };
        _this.showJobStatusInterview = function(job){
            if(job.status == 2){return true;}
            else{return false;}
        };
        _this.showJobStatusOffer = function(job){
            if(job.status == 3){return true;}
            else{return false;}
        };
        _this.showCareerFairUpdate = function (){
            return _this.getNewEventUpdateCount() > 0;
        };

        // Remove methods
        _this.removeNotification = function(message){
            console.log("Also need to remove notification from remote");
            _this.manage.notifications.splice(_this.manage.notifications.indexOf(message),1);
        };
        _this.removeNewsFeed = function(newsFeed){
            console.log("Also need to remove news feeds from remote");
            _this.manage.newsFeeds.splice(_this.manage.newsFeeds.indexOf(newsFeed),1);
        };
        _this.removeSavedJob = function(job){
            console.log("Also need to remove saved Jobs from remote");
            _this.manage.savedJobs.splice(_this.manage.savedJobs.indexOf(job),1);
        };
        _this.removeSavedEvent = function(event){
            console.log("Also need to remove saved Events from remote");
            _this.manage.savedEvents.splice(_this.manage.savedEvents.indexOf(event),1);
        };
        _this.removeSavedCompany = function(company){
            console.log("Also need to remove saved company saved status from remote");
            _this.manage.savedCompanies.splice(_this.manage.savedCompanies.indexOf(company),1);
        };

        _this.getJobTitle = function (job){
            if(job.title != null){
                if(job.companyName.length > 1){
                    return job.title +" ( " + job.companyName +" )";
                }else{
                    return job.title;
                }
            }else{ return null }
        };
        _this.editProfile = function() {
            $location.path('/student/edit');
        };
        _this.saveSettings = function() {
            if(_this.manage.newPassword == _this.manage.newPasswordConfirm){
                // Do some more validation
                console.log("requet to change the password")
            }
        };

    });
