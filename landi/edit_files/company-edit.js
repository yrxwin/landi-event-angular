'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanyEditCtrl
 * @description
 * # CompanyEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('CompanyEditCtrl', function ($scope, $location) {
        $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
        ];
        $('.js-aboutus-btn-edit').on('click',function(){
            $('.js-aboutus-show').addClass('hidden');
            $('.js-aboutus-edit').removeClass('hidden');
        });
        $('.js-aboutus-cancel').on('click',function(){
            $('.js-aboutus-show').removeClass('hidden');
            $('.js-aboutus-edit').addClass('hidden');
        });

        var _this = this;
        _this.company = {
            "name":"Samsung Electronics Inc.",
            "aboutus":"This is the organizer description, This is the event description, ",
            "location":"Cambridge, MA",
            "extUrl": "http://www.samsung.com",
            "industry": "Manufacturing",
            "scale": 5,
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "jobs": [{"title":"PHP Developer", "locations":["Cambridge, MA"],
                "description": "This is a job description", "url":"job/show", "editUrl":"job/edit" },
                {"title":"Project Manager", "locations":["Cambridge, MA", "Boston, MA"],
                    "description": "This is another job description", "url":"job/show", "editUrl":"job/edit" },
                {"title":"Project Manager", "locations":["Cambridge, MA", "Boston, MA", "China"],
                    "description": "This is a third job description", "url":"job/show", "editUrl":"job/edit" }],
        };

        $scope.companyScales = [
            {name: ' < 50 Employees', code: 0},
            {name: '50 - 100 Employees', code: 1},
            {name: '100 - 200 Employees', code: 2},
            {name: '200 - 600 Employees', code: 3},
            {name: '600 - 1000 Employees', code: 4},
            {name: '1000+ Employees', code: 5}
        ];

        _this.getScaleStr = function(){
            switch(_this.company.scale) {
                case 0:
                    return "< 50 Employees";
                case 1:
                    return "50 - 100 Employees";
                case 2:
                    return "100 - 200 Employees";
                case 3:
                    return "200 - 600 Employees";
                case 4:
                    return "600 - 1000 Employees";
                case 5:
                    return "1000+ Employees";
                default:
                    return "unavailabel";
            }
        };

        _this.getLocationStr = function(locations){
            if (locations.length > 0){
                console.log("locations count > 0");
                var locationStr = "";
                for (var i = 0; i < locations.length; i++) {
                    if(i == locations.length - 1){
                        locationStr += locations[i];
                    }else{
                        locationStr += locations[i] +", ";
                    }
                }
                return locationStr;
            }else{
                return "Unavailable";
            }
        };

        _this.createJob = function() {
            console.log("Navigate to create a job");
            $location.path('/job/create');
        };

        _this.editJob = function(job) {
            console.log("Navigate to edit a job with information from job entity");
            $location.path('/job/edit');
        };
        _this.removeJob = function(job) {
            _this.company.jobs.splice(_this.company.jobs.indexOf(job),1);
        };
    });
