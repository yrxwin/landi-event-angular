'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:dropdown
 * @description
 * # dropdown
 */
angular.module('landiWebApp')
  .directive('dropdown', function () {
    return {
        templateUrl: 'scripts/directives/dropdown.html',
        restrict: 'AE',
        scope: {
            items: '=',
            defaultText: '@',
            result: '='
        },
        link: function (scope, element, attr) {
            // element.text('this is the dropdown directive');
            element
                .dropdown({
                    onChange: function(value, text) {
                        scope.$apply(function(){
                            scope.result = value
                        })
                    }
                })

        }
    };
  });
