'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.AuthenticationService
 * @description
 * # AuthenticationService
 * Factory in the landiWebApp.
 */
angular.module('landiWebApp')
  .service('AuthService', function ($q, localStorageService, Session, Restangular) {

    this.login = function (credentials) {
      var me = this;
      var deferred = $q.defer();
      Session.create(credentials, true).then(function (response) {
          console.log(response.data)
            me.setToken(response.data);
            return deferred.resolve('success');
            /*Restangular.oneUrl('users/me/').get().then(function(data){
                console.log(data);
                return deferred.resolve('success')
            }, function(){
                localStorageService.clearAll();
                return deferred.reject('serverError')
            })*/
      }, function (response) {
            // console.log(response.data)
            if (response.status == 400) {
                return deferred.reject('noMatch');
            }
            return deferred.reject('serverError');
        // throw new Error('No handler for status code ' + response.status);
      });
      return deferred.promise
    };

    this.logout = function () {
      localStorageService.clearAll();
    };

    this.isAuthenticated = function () {
      var token = this.getToken();
      if (token) {
          console.log('true')
        return true
      }
        console.log('false');
      return false;

    };

    this.setToken = function (data) {
      // localStorageService.set('token', btoa(credentials.email + ':' + credentials.password));
        localStorageService.set('token', data.token);
        localStorageService.set('type', data.user_type);
        localStorageService.set('userId', data.user.id);
    };

    this.getToken = function () {
      return localStorageService.get('token');
    };

    return this;
  })
  .factory('Session', function ($http) {
    var Session;
    Session = {
      create: function(data, bypassErrorInterceptor) {
          var req = {
              method: 'POST',
              url: '/api/auth/',
              bypassErrorInterceptor: bypassErrorInterceptor,
              data: data
          }
          return $http(req);
        /*return Restangular
          .oneUrl('auth')
          .withHttpConfig({bypassErrorInterceptor: bypassErrorInterceptor})
          .customPOST(data);*/
      }
    };
    return Session;
  });
