'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.UserService
 * @description
 * # UserService
 * Factory in the landiWebApp.
 */
angular.module('landiWebApp')
  .factory('UserService', function () {
    // Service logic
    // ...


    // Public API here
    return {
      logIn: function(username, password) {
          return $http.post(options.api.base_url + '/login', {username: username, password: password});
      },

      logOut: function() {

      }
    };
  });
