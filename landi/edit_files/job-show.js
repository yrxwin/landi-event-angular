'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:JobShowCtrl
 * @description
 * # JobShowCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('JobShowCtrl', function ($scope, $location) {
        $scope.awesomeThings = [
          'HTML5 Boilerplate',
          'AngularJS',
          'Karma'
        ];

        var _this = this;
        _this.job = {
            "title":"Professional Magical Engineer",
            "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "contactName":"someone",
                "contactEmail":"someone@samsung.com"},
            "type":"Full-time",
            "dueDate":"2014-12-16T14:25:00.000Z",
            "description":"This is a job description \n, This is a job description, This is a job description,",
            "responsibility":"This is a job responsibility \n, This is a job responsibility, This is a job responsibility,",
            "locations":["location1, LC","location2., LC","location3., LC"],
            "expired":true,
            "saved":false,
            "applied":false,
            contactName:null,
            contactEmail:null
        };
        _this.getTypeStr = function(){
            if (_this.job.type){
                return " (" + _this.job.type + ")";
            }else{
                return null;
            }
        };

        _this.getContactName = function(){
            if(_this.job.contactName){
                return _this.job.contactName;
            }else{
                return _this.job.company.contactName;
            }
        };
        _this.getContactEmail = function(){
            if(_this.job.contactEmail){
                return _this.job.contactEmail;
            }else{
                return _this.job.company.contactEmail;
            }
        };
        _this.getAppliedStr = function(){
            if (_this.job.applied){
                return "Applied";
            }else{
                return "Apply";
            }
        };
        _this.getSavedStr = function(){
            if (_this.job.saved){
                return "Unsave";
            }else{
                return "Save";
            }
        };

        _this.applyClick = function() {
            if(!_this.job.applied){
                _this.job.applied = true;
                console.log("Apply button clicked");
                $location.path('/DoApplicationAction');
            }
        };

        _this.saveClick  = function() {
            _this.job.saved = !_this.job.saved;
            if(_this.job.saved){
                console.log("Save job clicked")
            }else{
                console.log("Unsave job clicked")
            }
        }
    });
