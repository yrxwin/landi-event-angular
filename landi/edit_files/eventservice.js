'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.EventService
 * @description
 * # EventService
 * Factory in the landiWebApp.
 */
angular.module('landiWebApp')
  .factory('EventService', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
