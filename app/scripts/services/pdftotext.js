'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.pdfToText
 * @description
 * # pdfToText
 * Factory in the landiWebApp.
 */
angular.module('landiWebApp')
    .factory('pdfToText', function () {
        // Service logic
        // ...

        var pdfToText = function (data, callbackPageDone, callbackAllDone) {
            var complete = 0;
            // console.assert(data  instanceof ArrayBuffer || typeof data == 'string');
            PDFJS.getDocument(data).then(function (pdf) {
                var div = document.getElementById('viewer');

                var total = pdf.numPages;
                callbackPageDone(0, total);
                var layers = {};
                for (var i = 1; i <= total; i++) {
                    pdf.getPage(i).then(function (page) {
                        var n = page.pageNumber;
                        page.getTextContent().then(function (textContent) {

                            console.log(textContent)
                            if (null != textContent.items) {
                                var page_text = "";
                                var last_block = null;
                                for (var k = 0; k < textContent.items.length; k++) {
                                    var block = textContent.items[k];
                                    /*if (last_block != null && last_block.str[last_block.str.length - 1] != ' ') {
                                     if (block.x < last_block.x)
                                     page_text += "\r\n";
                                     else if (last_block.y != block.y && ( last_block.str.match(/^(\s?[a-zA-Z])$|^(.+\s[a-zA-Z])$/) == null ))
                                     page_text += ' ';
                                     }*/

                                    page_text += block.str.replace(/\s+/, ' ');
                                    //last_block = block;
                                }

                                textContent != null && console.log("page " + n + " finished."); //" content: \n" + page_text);
                                layers[n] = page_text;
                            }
                            ++complete;
                            callbackPageDone(complete, total);
                            if (complete == total) {
                                var full_text = "";
                                var num_pages = Object.keys(layers).length;
                                for (var j = 1; j <= num_pages; j++)
                                    full_text += layers[j];
                                console.log(full_text)
                                callbackAllDone(full_text);
                            }
                        }); // end  of page.getTextContent().then
                    }); // end of page.then
                } // of for
            }).then(null, function(error){
                console.log(error);
            });
        }; // end of pdfToText()
        return pdfToText;
    });
