'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.AuthenticationService
 * @description
 * # AuthenticationService
 * Factory in the landiWebApp.
 */
angular.module('landiWebApp')
  .service('AuthService', function ($q, localStorageService, Session, Restangular, $rootScope) {

    this.login = function (credentials) {
      var me = this;
      var deferred = $q.defer();
      Session.create(credentials, true).then(function (response) {
          console.log(response.data)
            me.setHead(response.data);
            me.getUserType();
            return deferred.resolve('success');
            /*Restangular.oneUrl('users/me/').get().then(function(data){
                console.log(data);
                return deferred.resolve('success')
            }, function(){
                localStorageService.clearAll();
                return deferred.reject('serverError')
            })*/
      }, function (response) {
          // console.log(response.data)
            if (response.status == 400) {
                return deferred.reject('noMatch');
            }
            return deferred.reject('serverError');
        // throw new Error('No handler for status code ' + response.status);
      });
      return deferred.promise
    };

    this.logout = function () {
        localStorageService.clearAll();
        $rootScope.identity = 'visitor';
    };

    this.isAuthenticated = function () {
      var token = this.getToken();
      if (token) {
          console.log('true')
        return true
      }
        console.log('false');
      return false;

    };

    this.setHead = function (data) {
      // localStorageService.set('token', btoa(credentials.email + ':' + credentials.password));
        localStorageService.set('token', data.token);
        localStorageService.set('type', data.user_type);
        localStorageService.set('user_id', data.user.id);
    };

    /*this.getHead = function () {
        var header = {};
        header.token = localStorageService.get('token');
        header.user_type = localStorageService.get('user_type');
        header.user_id = localStorageService.get('user_id');
        return header;
    }*/
    this.getId = function () {
        var id = localStorageService.get('user_id');
        if (!id) {
            // $rootScope.$broadcast('showLogin');
            return id;
        }
        return id;
    }
    this.getUserType = function () {
        var type = localStorageService.get('type');
        if (!type) {
            type = 'visitor'
        }
        $rootScope.identity = type;
    }

    this.getToken = function () {
      return localStorageService.get('token');
    };

    return this;
  })
  .factory('Session', function ($http, $rootScope) {
    var Session;
    Session = {
      create: function(data, bypassErrorInterceptor) {
          var req = {
              method: 'POST',
              // url: '/api/auth/',
              // For Production
              url: $rootScope.baseUrl+'/auth/',
              bypassErrorInterceptor: bypassErrorInterceptor,
              data: data
          }
          return $http(req);
        /*return Restangular
          .oneUrl('auth')
          .withHttpConfig({bypassErrorInterceptor: bypassErrorInterceptor})
          .customPOST(data);*/
      }
    };
    return Session;
  });
