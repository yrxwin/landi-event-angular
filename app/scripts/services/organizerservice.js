'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.OrganizerService
 * @description
 * # OrganizerService
 * Factory in the landiWebApp.
 */
angular.module('landiWebApp')
  .factory('OrganizerService', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
