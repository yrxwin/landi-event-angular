'use strict';

/**
 * @ngdoc service
 * @name landiWebApp.restApi
 * @description
 * # restApi
 * Service in the landiWebApp.
 */
angular.module('landiWebApp')
  .service('restApi', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
