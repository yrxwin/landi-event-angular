'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('HeaderCtrl', function ($scope, AuthService, $modal, $location, $rootScope) {
        var _this = this;

        AuthService.getUserType();
        _this.open = function () {

            var modalInstance = $modal.open({
                templateUrl: 'login-pop.html',
                controller: 'LoginPopCtrl'
                // backdropClass: "color:red"
                /*resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }*/
            });
        };

        $scope.$on('showLogin', function () {
            //$('#loginPop').show();
            _this.open();
        })

        _this.logout = function() {
            AuthService.logout();
            _this.identity = AuthService.getUserType();
            $location.path('/');
        }

        _this.getPath = function() {
            return $location.path();
        }

        _this.getUrl = function() {
            return $location.url();
        }

        _this.clickSearch = function(hash) {
            if (hash) {
                $location.path('search').hash(hash).search({search: _this.searchContent})
            } else {
                $location.path('search').search({search: _this.searchContent})
            }
        }

        _this.showLogin = function(){
            $rootScope.$broadcast('showLogin');
        }
    });

