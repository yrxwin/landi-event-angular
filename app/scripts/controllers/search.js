'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp').controller('SearchCtrl', function ($scope, $rootScope, Restangular, $anchorScroll, $location, $routeParams) {
    /*$('.ui.accordion').accordion();
    $('.ui.dropdown').dropdown();
    $('.ui.checkbox').checkbox();
    $('#eventStart').datetimepicker();*/

    var _this = this;
    switch ($rootScope.getHash()) {
        case 'jobs':
            break;
        case 'organizations':
            break;
        case 'events':
            break;

    }
    _this.searchError = false;
    _this.queryParams = {};
    _this.queryParams.search = $routeParams.search;
    _this.queryParams.type__contains = $routeParams.type__contains||'';
    _this.doSearch = function() {
        _this.queryParams.page = _this.currentPage;
        console.log(_this.queryParams)
        switch ($rootScope.getHash()) {
            case 'jobs':
                Restangular.one('jobs').get(_this.queryParams).then(function(data){
                    console.log(data);
                    _this.searchError = false;
                    _this.results = data.results;
                    _this.count = data.count;
                    $anchorScroll();
                }, function(){
                    _this.searchError = true;
                })
                break;
            case 'organizations':
                Restangular.one('organizations').get(_this.queryParams).then(function(data){
                    console.log(data);
                    _this.searchError = false;
                    _this.results = data.results;
                    _this.count = data.count;
                    $anchorScroll();
                }, function(){
                    _this.searchError = true;
                })
                break;
            default:
                Restangular.one('events').get(_this.queryParams).then(function(data){
                    console.log(data);
                    _this.searchError = false;
                    _this.results = data.results;
                    _this.count = data.count;
                    $anchorScroll();
                }, function(){
                    _this.searchError = true;
                })
        }
    }
    var searchNum = 1;
    _this.doNewSearch = function () {
        _this.searchContent = _this.queryParams.search; // For displaying
        _this.currentPage = 1;
        _this.count = 0;
        if (searchNum > 1){
            $location.search(_this.queryParams);
        }
        searchNum ++;
        _this.doSearch();
    };
    _this.doNewSearch();

    _this.keyPress = function(keyEnter){
        if (keyEnter.which === 13){
            _this.doNewSearch();
        }
    }

    /*_this.shouldShowJobType = function(){
        return $rootScope.getHash()=='jobs' && _this.search.jobTypes.some(function(el) {
            return el.selected === false;
        });
    };

    _this.selectTypeChanged = function(){
        if(_this.search.jobTypes.every(function(el) {
            return el.selected === false;
        })){
            _this.search.jobTypes.forEach(function(type) {
                type.selected = true;
            })
        }
    };
    _this.removeJobType = function(jobType){
        jobType.selected = false;
        console.log("Search with new job type requirements");
    };*/


    /*_this.search = {
        //"searchType":1,
        "type":1,
        "keyword":"Electrical engineering",
        "inEvent":"MIT Career Fair Spring 2015",
        "degree":"",
        "location":"Cambridge, MA",
        "jobTypes":[
            {"code": 'ft', "name": "Full-time", "selected": true},
            {"code": 'pt', "name": "Part-time", "selected": true},
            {"code": 'in', "name": "Intern", "selected": false},
            {"code": 'inpt', "name": "Intern(part-time)", "selected": false},
            {"code": 'other', "name": "Other", "selected": false}
        ]
        // Optional refining conditions returned in the JSON object
    };
    _this.companyResults = [{ "name":"Google Inc.",
        "url":"company/show",
        "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
        "industry":"Manufacturing",
        "description":"this is a company description, this is a company description, this is a company description, ",
        "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "description":"this is a company description, this is a company description, this is a company description, ",
            "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "description":"this is a company description, this is a company description, this is a company description, ",
            "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "description":"this is a company description, this is a company description, this is a company description, ",
            "location":"Cambridge, MA"}
    ];
    _this.organizerResults=[{ "name":"MIT Asian Career Fair",
        "url":"organizer/show",
        "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
        "description":"this is a company description, this is a company description, this is a company description, ",
        "location":"Cambridge, MA"}];
    _this.eventResults=[{ "name":"MIT Spring 2015 Career Fair",
        "url":"event/show",
        "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
        "organizer":{"name":"MIT Asian Career Fair"},
        "description":"This is the event description, This is the event description, ",
        "start":"2014-12-16T14:25:00.000Z",
        "end":"2014-12-16T14:25:00.000Z",
        "location":"Cambridge, MA"},
        { "name":"MIT Spring 2015 Career Fair",
            "url":"event/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "organizer":{"name":"MIT Asian Career Fair"},
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-16T14:25:00.000Z",
            "location":"Cambridge, MA"},
        { "name":"MIT Spring 2015 Career Fair",
            "url":"event/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "organizer":{"name":"MIT Asian Career Fair"},
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-16T14:25:00.000Z",
            "location":"Cambridge, MA"},
        { "name":"MIT Spring 2015 Career Fair",
            "url":"event/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "organizer":{"name":"MIT Asian Career Fair"},
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-16T14:25:00.000Z",
            "location":"Cambridge, MA"},
        { "name":"MIT Spring 2015 Career Fair",
            "url":"event/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "organizer":{"name":"MIT Asian Career Fair"},
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-16T14:25:00.000Z",
            "location":"Cambridge, MA"}
    ];
    _this.jobResults=[
        {"title":"Professional Magical Engineer",
            "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                "logo":"http://semantic-ui.com/images/wireframe/image.png",
                "industry":"Manufacturing",
                "contactName":"someone",
                "contactEmail":"someone@samsung.com"},
            "type":1,
            "url":"job/show",
            "description":"this is a job description, this is a job description, this is a job description, ",
            "dueDate":"2014-12-16T14:25:00.000Z",
            "locations":["location1, LC","location2., LC","location3., LC"],
            "expired":true,
            "applied":false,
            contactName:null,
            contactEmail:null
        },
        {"title":"Professional Magical Engineer",
            "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                "logo":"http://semantic-ui.com/images/wireframe/image.png",
                "industry":"Manufacturing",
                "contactName":"someone",
                "contactEmail":"someone@samsung.com"},
            "description":"this is a job description, this is a job description, this is a job description, ",
            "type":1,
            "url":"job/show",
            "dueDate":"2014-12-16T14:25:00.000Z",
            "locations":["location1, LC","location2., LC","location3., LC"],
            "expired":true,
            "applied":false,
            contactName:null,
            contactEmail:null
        },
        {"title":"Professional Magical Engineer",
            "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                "logo":"http://semantic-ui.com/images/wireframe/image.png",
                "industry":"Manufacturing",
                "contactName":"someone",
                "contactEmail":"someone@samsung.com"},
            "type":2,
            "url":"job/show",
            "description":"this is a job description, this is a job description, this is a job description, ",
            "dueDate":"2014-12-16T14:25:00.000Z",
            "locations":["location1, LC","location2., LC","location3., LC"],
            "expired":true,
            "applied":false,
            contactName:null,
            contactEmail:null
        }
    ];

    _this.removeDegree = function(){
        _this.search.degree = null;
        console.log("Do a search without degree information")
    };

    _this.addEvent = function(){
        if(_this.newEvent && _this.newEvent.length > 0){
            _this.search.event = _this.newEvent;
            _this.newEvent = null;
        }else{
            _this.newEvent = null;
        }
    };
    _this.removeEvent = function(){
        _this.search.event = null;
    };

    _this.addLocation = function(){
        if(_this.newLocation && _this.newLocation.length > 0){
            _this.search.location = _this.newLocation;
            _this.newLocation = null;
        }else{
            _this.newLocation = null;
        }
    };
    _this.removeLocation = function(){
        _this.search.location = null;
    };

    _this.toggleJobType = function(jobType){
        console.log(jobType.selected)
    };

    _this.getPositionTypeStr = function(typeCode){
        switch(typeCode){
            case 'ft':
                return "Full-time";
            case 'pt':
                return "Part-time";
            case 'in':
                return "Intern";
            case 'inpt':
                return "Intern (Part-time)";
            case 'co':
                return "Coop";
            default:
                return "Other";
        }
    };

    _this.jobTypes = [
        {name: 'Full-time', code: 'ft'},
        {name: 'Part-time', code: 'pt'},
        {name: 'Intern', code: 'in'},
        {name: 'Intern (Part-time)', code: 'inpt'},
        {name: 'Coop', code: 'co'}
    ];
    _this.getResultCount = function(){
        switch(_this.search.type){
            case 1:
                return _this.jobResults.length;
            case 2:
                return _this.companyResults.length;
            case 3:
                return _this.eventResults.length;
            case 4:
                return _this.organizerResults.length;
            default:
                return _this.jobResults.length+_this.companyResults.length+_this.eventResults.length+_this.organizerResults.length;
        }
    };
*//*
    _this.getNewSearchTypeStr = function(){
        switch(_this.search.searchType){
            case 1:
                return "Job";
            case 2:
                return "Company";
            case 3:
                return "Event";
            case 4:
                return "Organizer";
            default:
                return "All";
        }
    };*//*
    _this.getSearchTypeStr = function(){
        switch(_this.search.type){
            case 1:
                return "Job";
            case 2:
                return "Company";
            case 3:
                return "Event";
            case 4:
                return "Organizer";
            default:
                return "All";
        }
    };
    _this.getSearchDegreeStr = function(){
        if(this.search.degree!=null){
            switch(this.search.degree){
                case "bs":
                    return "Bachelor of Science";
                case "ba":
                    return "Bachelor of Arts";
                case "ms":
                    return "Master of Science";
                case "ma":
                    return "Master of Arts";
                case "phd":
                    return "Doctorate";
                case "pd":
                    return "Post-doctoral";
                case "other":
                    return "Others";
                default:
                    return null;
            }
        }else{ return null}
    };

    _this.dateRanges = [
        {code: 0, text: 'All'},
        {code: 1, text: 'In one weeks'},
        {code: 2, text: 'In two weeks'},
        {code: 3, text: 'Within a month'}
    ];

    _this.hasDateRange = function(){
        return _this.search.dateRange!= null && _this.search.dateRange.text.length> 0;
    };
    _this.removeDateRange = function(){
        _this.search.dateRange = null;
        console.log("Do a search without dateRange information")
    };

    *//*_this.setSearchType = function(type){
        _this.search.type = type;
    };
    _this.isSearchingJobs = function(){
        return _this.search.type == 1;
    };

    _this.isSearchingCompanies = function(){
        return _this.search.type == 2;
    };

    _this.isSearchingEvents = function(){
        return _this.search.type == 3;
    };
    _this.isSearchingOrganizers = function(){
        return _this.search.type == 4;
    };*//*

    _this.getResultsCount = function(){
        switch(_this.search.type){
            case 1:
                return _this.search.jobResults.length;
            case 2:
                return _this.search.companyResults.length;
            case 3:
                return _this.search.eventResults.length;
            case 4:
                return _this.search.organizerResults.length;
            default:
                return _this.search.jobResults.length;
        }
    }; */

    // --------- Methods for search job cards -------------

    _this.getContactName = function(job){
        if(job.contactName){
            return job.contactName;
        }else{
            return job.company.contactName;
        }
    };
    _this.getContactEmail = function(job){
        if(job.contactEmail){
            return job.contactEmail;
        }else{
            return job.company.contactEmail;
        }
    };
    _this.getAppliedStr = function(job){
        if (job.applied){
            return "Applied";
        }else{
            return "Apply";
        }
    };

    _this.applyClick = function(job) {
        if(!job.applied){
            job.applied = true;
            console.log("Apply button clicked");
            $location.path('/DoApplicationAction');
        }
    };

    // --------- Methods for search company cards -------------
    // Not much needed for now.
    // --------- Methods for search event cards -------------
    _this.getAttendStr = function(event){
        if (event.attend){
            return "Reserved";
        }else{
            return "Attend";
        }
    };

    _this.attendClick = function(event) {
        if(!event.attend){
            event.attend = true;
            console.log("Participate clicked");
            //$location.path('/attendEventAction');
        }
    };






    /*_this.all = {value: 'All', text: 'All'};

    $scope.acadLevels = [
        {name: 'Bachelor', code: 'bs', abbr:'B.S.'},
        {name: 'Master', code: 'ms', abbr:'M.S.'},
        {name: 'Doctor', code: 'phd', abbr:'Ph.D.'},
        {name: 'Post Doc', code: 'postdoc', abbr:'Post-doc.'},
        {name: 'Other', code: 'other', abbr:'Other'},
        {name: 'Unknown', code: 'ua', abbr:''}
    ];
    _this.degrees = [
        {value: 'bs', text: 'Bachelor of Science'},
        {value: 'ba', text: 'Bachelor of Arts'},
        {value: 'ms', text: 'Master of Science'},
        {value: 'ma', text: 'Master of Arts'},
        {value: 'phd', text: 'Doctorate'},
        {value: 'pd', text: 'Post-doc'},
        {value: 'other', text: 'Others'}
    ]*/
});
