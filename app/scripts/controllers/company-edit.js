'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanyEditCtrl
 * @description
 * # CompanyEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('CompanyEditCtrl', function ($scope, $location, Restangular, AuthService, FileUploader, $filter, $rootScope, $anchorScroll, $sce) {

        /*$('.js-aboutus-btn-edit').on('click',function(){
            $('.js-aboutus-show').addClass('hidden');
            $('.js-aboutus-edit').removeClass('hidden');
        });
        $('.js-aboutus-cancel').on('click',function(){
            $('.js-aboutus-show').removeClass('hidden');
            $('.js-aboutus-edit').addClass('hidden');
        });*/
        var _this = this;
        _this.corFields = ['name'];
        var user_id = AuthService.getId();
        Restangular.one('organizations',user_id).get().then(function(data){
            console.log(data)
            _this.organization = data;
            if (data.image) {
                _this.organization.image_url = data.image.file_url;
                _this.organization.image = data.image.id;
            }
            _this.organization.description_show = $sce.trustAsHtml(_this.organization.description);
        })

        _this.searchError = false;
        _this.doSearch = function() {
            var queryParams = {};
            queryParams.search = _this.keywords;
            queryParams.page = _this.currentPage;
            switch ($rootScope.getHash()) {
                case 'jobs':
                    Restangular.one('jobs').get(queryParams).then(function(data){
                        console.log(data);
                        _this.searchError = false;
                        _this.results = data.results;
                        _this.count = data.count;
                        $anchorScroll();
                    }, function(){
                        _this.searchError = true;
                    })
                    break;
                case 'organizations':
                    console.log("New Search organizations");
                    break;
                default:
                    Restangular.one('events').get(queryParams).then(function(data){
                        console.log(data);
                        _this.searchError = false;
                        _this.results = data.results;
                        _this.count = data.count;
                        $anchorScroll();
                    }, function(){
                        _this.searchError = true;
                    })
            }
        }
        _this.doNewSearch = function () {
            _this.currentPage = 1;
            _this.count = 0;
            _this.doSearch();
        };
        _this.doNewSearch();

        /*_this.updateCompany = function(data, field){
            var updateObj = {};
            updateObj[field] = data
            Restangular.one('corporations',_this.organization.company.id).patch(updateObj).then(function(){

            },function(){
                return $rootScope.defaultErrorInfo
            })
        }*/
        _this.updateOrganization = function(data, field){
            var updateObj = {};
            updateObj[field] = data;
            Restangular.one('organizations',user_id).patch(updateObj).then(function(data){
            },function(){
                return $rootScope.defaultErrorInfo
            })
        }

        _this.comEdit = false;
        _this.comError = false;
        _this.editCom = function(name){
            _this.company_temp = _this.organization.company;
            _this.comEdit = !_this.comEdit;
        }
        var companyValidator = $('#company').validate({
            submitHandler: function (form) {
                var company_id = _this.company_temp.id;
                Restangular.one('organizations',_this.organization.id).patch({company:company_id}).then(function(data){
                    console.log(data)
                    _this.organization.company = _this.company_temp;
                    _this.comError = false;
                    _this.comEdit = false;
                },function(){
                    _this.comError = true;
                })
            },
            rules: {
                auto: 'required'
             }
        })

        _this.desEdit = false;
        _this.desError = false;
        _this.editDes = function(name){
            _this.description_temp = _this.organization.description;
            _this.desEdit = !_this.desEdit;
        }
        var descriptionValidator = $('#description').validate({
            submitHandler: function (form) {
                Restangular.one('organizations',_this.organization.id).patch({description:_this.description_temp}).then(function(data){
                    _this.organization.description = _this.description_temp;
                    _this.organization.description_show = $sce.trustAsHtml(_this.organization.description);
                    _this.desError = false;
                    _this.desEdit = false;
                },function(){
                    _this.desError = true;
                })
            }
            /*rules: {
                description: {
                    rangelength: [0, 2000]
                }
            }*/
        })

        var auth = 'Token ' + AuthService.getToken();
        var uploaderImg = _this.uploaderImg = new FileUploader({
            url: $rootScope.baseUrl+'/images/',
            headers: {
                Authorization: auth
            },
            autoUpload: true
        });
        uploaderImg.filters.push({
            name: 'typeFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });
        var imgSize = 1;
        uploaderImg.filters.push({
            name: 'sizeFilter',
            fn: function(item, options) {
                return (item.size/1042/1042 <= imgSize)
            }
        });

        _this.imgError = false;
        uploaderImg.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            _this.imgError = true;
            if (filter.name == 'typeFilter') {
                _this.imgErrorInfo = 'You submit should be image file'
            }
            if (filter.name == 'sizeFilter') {
                _this.imgErrorInfo = 'You submit should be less than '+ imgSize + 'MB'
            }
        };
        uploaderImg.onAfterAddingFile = function(fileItem) {
            // console.log(fileItem)
            _this.imgError = false;
        };
        uploaderImg.onErrorItem = function(item, response, status, headers) {
            _this.imgError = true;
            _this.imgErrorInfo = 'Server Error'
        }
        uploaderImg.onSuccessItem = function(item, response, status, headers) {
            Restangular.one('organizations',_this.organization.id).patch({image:response.id}).then(function(data){
                uploaderImg.clearQueue();
                uploaderImg.queue[0] = item;
                _this.imgError = false;
                _this.organization.image = response.id;
                _this.organization.image_url = response.file_url;
            },function(){
                _this.imgError = true;
            })
            /*Restangular.one('corporations', _this.organization.id).patch({image: response.id}).then(function (data) {
                if (_this.organization.image) {
                    Restangular.one('images', _this.organization.image).remove().then(function () {
                        uploaderImg.clearQueue();
                        uploaderImg.queue[0] = item;
                        _this.imgError = false;
                        _this.organization.image = response.id;
                        _this.organization.image_url = response.file_url;
                    }, function () {
                        _this.imgError = true;
                    })
                } else {
                    uploaderImg.clearQueue();
                    uploaderImg.queue[0] = item;
                    _this.imgError = false;
                    _this.organization.image = response.id;
                    _this.organization.image_url = response.file_url;
                }
            }, function () {
                _this.imgError = true;
            })*/

            // console.log(response)
        }
        _this.removeImg = function() {
            Restangular.one('organizations', _this.organization.id).patch({image:null}).then(function(){
                _this.organization.image = null;
                _this.organization.image_url = null;
                _this.imgError = false;
            },function(){
                _this.imgError = true;
            })
            /*Restangular.one('images', _this.organization.image).remove().then(function () {
                uploaderImg.clearQueue();
                _this.organization.image = null;
                _this.organization.image_url = null;
                _this.imgError = false;
            }, function () {
                _this.imgError = true;
            })*/
        }

        _this.locationEdit = false;
        _this.locationError = false;
        _this.editLocation = function(){
            _this.location_temp = _this.organization.company.location;
            _this.locationEdit = !_this.locationEdit
        }
        var locationValidator = $('#location').validate({
            submitHandler: function (form) {
                Restangular.one('locations',_this.organization.company.location.id).patch(_this.location_temp).then(function(data){
                    console.log(data)
                    _this.organization.company.location = _this.location_temp;
                    _this.locaitonError = false;
                    _this.locationEdit = false;
                },function(){
                    _this.locationError = true;
                })
            },
            rules: {
                city: 'required',
                state: 'required',
                country: 'required'
            }
        })
        _this.changeFormat = function() {
            _this.location_temp.formatted_address = (_this.location_temp.line1 ?  (_this.location_temp.line1+', '):'')
                + (_this.location_temp.city ? (_this.location_temp.city+', '):'')
                + (_this.location_temp.state ? (_this.location_temp.state)+' ':'')
                + (_this.location_temp.zip_code ? (_this.location_temp.zip_code+', '):'')
                + (_this.location_temp.country ? (_this.location_temp.country):'');
        }


        /*_this.company = {
            "name":"Samsung Electronics Inc.",
            "aboutus":"This is the organizer description, This is the event description, ",
            "location":"Cambridge, MA",
            "extUrl": "http://www.samsung.com",
            "industry": "Manufacturing",
            "scale": 5,
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "jobs": [{"title":"PHP Developer", "locations":["Cambridge, MA"],
                "description": "This is a job description", "url":"job/show", "editUrl":"job/edit" },
                {"title":"Project Manager", "locations":["Cambridge, MA", "Boston, MA"],
                    "description": "This is another job description", "url":"job/show", "editUrl":"job/edit" },
                {"title":"Project Manager", "locations":["Cambridge, MA", "Boston, MA", "China"],
                    "description": "This is a third job description", "url":"job/show", "editUrl":"job/edit" }],
        };*/

        /*_this.companyScales = [
            {name: ' < 50 Employees', code: '0'},
            {name: '50 - 100 Employees', code: '1'},
            {name: '100 - 200 Employees', code: '2'},
            {name: '200 - 600 Employees', code: '3'},
            {name: '600 - 1000 Employees', code: '4'},
            {name: '1000+ Employees', code: '5'}
        ];*/

        _this.showScale = function() {
            var selected = $filter('filter')($rootScope.companyScales, {code: _this.organization.company.scale});
            return (_this.organization.company.scale && selected.length) ? selected[0].name : 'Not set';
        };
        /*_this.getScaleStr = function(){
            switch(_this.organization.company.scale) {
                case '0':
                    return "< 50 Employees";
                case '1':
                    return "50 - 100 Employees";
                case '2':
                    return "100 - 200 Employees";
                case '3':
                    return "200 - 600 Employees";
                case '4':
                    return "600 - 1000 Employees";
                case '5':
                    return "1000+ Employees";
                default:
                    return "";
            }
        };*/

        _this.getLocationStr = function(locations){
            if (locations.length > 0){
                // console.log("locations count > 0");
                var locationStr = "";
                for (var i = 0; i < locations.length; i++) {
                    if(i == locations.length - 1){
                        locationStr += locations[i];
                    }else{
                        locationStr += locations[i] +", ";
                    }
                }
                return locationStr;
            }else{
                return "Unavailable";
            }
        };

        _this.createJob = function() {
            console.log("Navigate to create a job");
            $location.path('/job/create');
        };

        _this.editJob = function(job) {
            console.log("Navigate to edit a job with information from job entity");
            $location.path('/job/edit');
        };
        _this.removeJob = function(job) {
            _this.company.jobs.splice(_this.company.jobs.indexOf(job),1);
        };
    });
