'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:OrganizerShowCtrl
 * @description
 * # OrganizerShowCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('OrganizerShowCtrl', function ($scope) {
        $scope.awesomeThings = [
          'HTML5 Boilerplate',
          'AngularJS',
          'Karma'
        ];

        var _this = this;
        _this.organizer = {
            "name":"MIT Ventureships Club",
            "aboutus":"This is the organizer description, This is the event description, ",
            "location":"30 Main St, Melrse, MA, 02176",
            "extUrl": "http://ventureships.mit.edu",
            "type": "Non-profit",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "events": [{"title":"VentureShips Career Fair", "start": "2014/12/23 10:30:PM", "end": "2014/12/23 10:30:PM",
                "location":" World Trade Center, New York, NY, 123456", "url":"/event/show", "description": "This is an event description" },
                {"title":"Thank Giving Party", "start": "2014/12/23 10:30:PM", "end": "2014/12/23 10:30:PM",
                    "location":" World Trade Center, New York, NY, 123456", "url":"/event/show", "description": "This is another event description" }]
        };

        _this.getEventCountStr = function(){
            var num = _this.organizer.events.length;
            if (num > 1){
                return "Organized " + num + " events";
            }else{
                return "Organized " + num + " event";
            }
        };

    });
