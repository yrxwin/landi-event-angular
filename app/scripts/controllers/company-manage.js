'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanyManageCtrl
 * @description
 * # CompanyManageCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('CompanyManageCtrl', function ($scope, Restangular, AuthService, $rootScope, $anchorScroll) {
        var _this = this;

        var id = AuthService.getId();
        _this.passError = false;
        _this.passErrorInfo = '';
        $('#password').validate({
            submitHandler: function (form) {
                console.log(_this.credential)
                Restangular.one('change_password',id).patch(_this.credential).then(function(data){
                    _this.passSuccess = true;
                    _this.passError = false;
                },function(res){
                    if (res.status == 403){
                        _this.passErrorInfo = 'Password is wrong'
                    }
                    _this.passSuccess = false;
                    _this.passError = true;
                })
            },
            rules: {
                old_password: "required",
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirm: {
                    required: true,
                    equalTo: "#inputPassword"
                }
            }
        });

        _this.searchError = false;
        _this.keywords = '';
        _this.doSearch = function() {
            var queryParams = {};
            queryParams.search = _this.keywords;
            queryParams.page = _this.currentPage;
            queryParams.company__id = id;
            switch ($rootScope.getHash()) {
                case 'jobs':
                    Restangular.one('jobs').get(queryParams).then(function(data){
                        console.log(data);
                        _this.searchError = false;
                        _this.results = data.results;
                        _this.count = data.count;
                        $anchorScroll();
                    }, function(){
                        _this.searchError = true;
                    })
                    break;
                case 'organizations':
                    console.log("New Search organizations");
                    break;
                default:
                    Restangular.one('events').get(queryParams).then(function(data){
                        console.log(data);
                        _this.searchError = false;
                        _this.results = data.results;
                        _this.count = data.count;
                        $anchorScroll();
                    }, function(){
                        _this.searchError = true;
                    })
            }
        }
        _this.doNewSearch = function () {
            _this.currentPage = 1;
            _this.count = 0;
            _this.doSearch();
        };
        _this.doNewSearch();
  });
