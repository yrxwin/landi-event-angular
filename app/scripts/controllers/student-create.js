'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentCreateCtrl
 * @description
 * # StudentCreateCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('StudentCreateCtrl', function ($scope, $location, Restangular, AuthService, FileUploader, $rootScope, pdfToText) {
        var _this = this;

        _this.uniFields = ['name'];
        _this.majorFields = ['name'];
        _this.titleFields = ['name'];
        _this.corFields = ['name'];

        _this.student = {};
        _this.student.educations = [];
        _this.student.educations_show = [];
        _this.student.courses = [];
        _this.student.exps = [];
        _this.student.exps_show = [];

        _this.newEdu = {};
        _this.newCourse = null;
        _this.newExp = {};


        _this.getUniversity = function(val){
            return Restangular.oneUrl('universities').get({name__icontains:val}).then(function(data){
                console.log(data.results)
                return(data.results)
            })
        };

        $('#birth_date').datetimepicker({
            pickTime: false
        });
        $("#birth_date").on("dp.change",function (e) {
            var scope = angular.element($('#birth_date')).scope();
            scope.$apply(function(){
                _this.student.birth_date = moment(e.date)
            });
        });

        _this.addEdu = function() {
            // console.log(_this.newEdu);
            Restangular.all('educations').post(_this.newEdu).then(function(data){
                console.log(data)
                _this.student.educations.push(data.id);
                _this.student.educations_show.push(data);
                _this.newEdu = {};
                _this.eduError = false;
                _this.eduErrorInfo = '';
                // console.log(_this.student.educations)
            },function(){
                _this.eduError = true;
            })
        };

        _this.addCourse = function() {
            // console.log(_this.newCourse);
            // console.log(_this.student.courses.indexOf(_this.newCourse))
            if((_this.student.courses.indexOf(_this.newCourse) > -1) && (_this.student.courses.length)) {
                _this.courseError = true;
                _this.courseErrorInfo = 'This course has already existed'
                return;
            }
            if(_this.newCourse){
                _this.courseError = false;
                _this.courseErrorInfo = {};
                _this.student.courses.push(_this.newCourse);
                _this.newCourse = null;
            }
        };

        _this.addExp = function() {
            console.log(_this.newExp);
            Restangular.all('experiences').post(_this.newExp).then(function(data){
                _this.student.exps.push(data.id);
                _this.student.exps_show.push(data);
                _this.newExp = {};
                _this.expError = false;
                _this.expErrorInfo = '';
            }, function(){
                _this.expError = true;
            })
        };

        _this.acadLevels = [
            {name: 'Bachelor', code: '1'},
            {name: 'Master', code: '2'},
            {name: 'Doctorate', code: '3'},
            {name: 'Other', code: '0'}
        ];
        _this.getAcadLevelAbbr = function(edu){
            switch(edu.degree){
                case '1':
                    return "Bachelor";
                case '2':
                    return "Master";
                case '3':
                    return "Doctorate";
                default:
                    return 'Other Degree';
            }
        };
        _this.formatGPAStr = function(gpa){
            return gpa.toFixed(2);
        };

        _this.removeEdu = function(edu) {
            Restangular.one('educations', edu.id).remove().then(function(){
                _this.eduError = false;
                _this.student.educations.splice(_this.student.educations.indexOf(edu.id),1);
                _this.student.educations_show.splice(_this.student.educations_show.indexOf(edu),1);
                // console.log(_this.student.educations)
            }, function(){
                _this.eduError = true;
            })
        };
        _this.removeCourse = function(course) {
            _this.student.courses.splice(_this.student.edus.indexOf(course),1);
        };
        _this.removeExp = function(exp) {
            Restangular.one('experiences', exp.id).remove().then(function(){
                _this.expError = false;
                _this.student.exps.splice(_this.student.exps.indexOf(exp.id),1);
                _this.student.exps_show.splice(_this.student.exps_show.indexOf(exp),1);
            }, function(){
                _this.expError = true;
            })
        }

        _this.clearNewEdu = function() {
            _this.newEdu = {};
            educationValidator.resetForm();
        };

        _this.clearNewExp = function() {
            _this.newExp = {};
            experienceValidator.resetForm();
        }

        _this.error = false;
        _this.errorInfo = '';

        var validationPassed = function(){
            console.log(_this.student);
            var user_id = AuthService.getId();
            Restangular.one('students', user_id).patch(_this.student).then(function(data) {
                console.log(data)
                $location.path('user/finish')
            }, function() {
                // _this.errorInfo = 'Server Error';
                _this.error = true;
            })
        };

        $('#student').validate({
            /*submitHandler: function () {
                console.log(_this.student);
                _this.error = false;
                validationPassed();
            },*/
            rules: {
                firstName: "required",
                lastName: "required",
                high_degree: "required",
                description: {
                    required: true,
                    rangelength: [1, 50]
                }
                /*password: {
                 required: true,
                 minlength: 6
                 },
                 password2: {
                 required: true,
                 equalTo: "#inputPassword"
                 }*/
            }
        });

        _this.submitClick = function() {
            if ($('#student').valid()){
                _this.student.info_completed = 1;
                _this.error = false;
                validationPassed();
            } else {
                _this.error = true;
                _this.errorInfo = 'please correct the highlighted fields'
            }
        }

        var educationValidator = $('#education').validate({
            submitHandler: function (form) {
                if (!_this.newEdu.university && !_this.newEdu.majors){
                    _this.eduError = true;
                    _this.eduErrorInfo = 'The university and major fields are required';
                    return;
                } else if (!_this.newEdu.university){
                    _this.eduError = true;
                    _this.eduErrorInfo = 'The university field is required';
                    return;
                } else if (!_this.newEdu.majors){
                    _this.eduError = true;
                    _this.eduErrorInfo = 'The major field is required';
                }
                _this.newEdu.university = _this.newEdu.university.id;
                var major_id = _this.newEdu.majors.id;
                _this.newEdu.majors = [];
                _this.newEdu.majors.push(major_id); // many to many
               _this.addEdu();
            },
            rules: {
                university: "required",
                degree: "required",
                major: "required",
                gpa: {
                    number: true
                },
                gpaFull: {
                    number: true
                },
                start: "required"
            }
        })

        var experienceValidator = $('#experience').validate({
            submitHandler: function (form) {
                if (!_this.newExp.company && !_this.newExp.title){
                    _this.expError = true;
                    _this.expErrorInfo = 'The company and title fields are required';
                    return;
                } else if (!_this.newExp.company){
                    _this.expError = true;
                    _this.expErrorInfo = 'The company field is required';
                    return;
                } else if (!_this.newExp.title){
                    _this.expError = true;
                    _this.expErrorInfo = 'The title field is required';
                }
                _this.newExp.company = _this.newExp.company.id;
                _this.newExp.title = _this.newExp.title.id;
                _this.addExp();
            },
            rules: {
                title: "required",
                company: "required",
                start: "required",
                type: "required",
                description: {
                    rangelength: [0, 1000]
                }
            }
        });



        $('#start').datetimepicker({
            pickTime: false
        });
        $('#end').datetimepicker({
            pickTime: false
        });
        $("#start").on("dp.change",function (e) {
            var scope = angular.element($('#start')).scope();
            scope.$apply(function(){
                _this.newEdu.start_date = moment(e.date)
            });
            $('#end').data("DateTimePicker").setMinDate(e.date);
        });
        $("#end").on("dp.change",function (e) {
            var scope = angular.element($('#end')).scope();
            scope.$apply(function(){
                _this.newEdu.end_date = moment(e.date)
            });
            $('#start').data("DateTimePicker").setMaxDate(e.date);
        });

        $('#start-exp').datetimepicker({
            pickTime: false
        });
        $('#end-exp').datetimepicker({
            pickTime: false
        });
        $("#start-exp").on("dp.change",function (e) {
            var scope = angular.element($('#start-exp')).scope();
            scope.$apply(function(){
                _this.newExp.start_date = moment(e.date)
            });
            $('#end-exp').data("DateTimePicker").setMinDate(e.date);
        });
        $("#end-exp").on("dp.change",function (e) {
            var scope = angular.element($('#end-exp')).scope();
            scope.$apply(function(){
                _this.newExp.end_date = moment(e.date)
            });
            $('#start-exp').data("DateTimePicker").setMaxDate(e.date);
        });

        var auth = 'Token ' + AuthService.getToken();
        var uploaderPdf = _this.uploaderPdf = new FileUploader({
            url: $rootScope.baseUrl+ '/resumes/',
            headers: {
                Authorization: auth
            },
            autoUpload: true
        });

        // FILTERS

        uploaderPdf.filters.push({
            name: 'typeFilter',
            fn: function(item, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|pdf|PDF|'.indexOf(type) !== -1;
            }
        });
        var pdfSize = 1;
        uploaderPdf.filters.push({
            name: 'sizeFilter',
            fn: function(item, options) {
                return (item.size/1042/1042 <= pdfSize)
            }
        });

        _this.pdfError = false;
        uploaderPdf.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            _this.pdfError = true;
            if (filter.name == 'typeFilter') {
                _this.pdfErrorInfo = 'You submit should be pdf file'
            }
            if (filter.name == 'sizeFilter') {
                _this.pdfErrorInfo = 'You submit should be less than '+ pdfSize + 'MB'
            }
         };
        uploaderPdf.onAfterAddingFile = function(fileItem) {
            _this.pdfError = false;
        };
        uploaderPdf.onErrorItem = function(item, response, status, headers) {
            _this.pdfError = true;
            _this.pdfErrorInfo = 'Server Error'
        }
        uploaderPdf.onSuccessItem = function (item, response, status, headers) {
            var fileUrl = $rootScope.amazonUrl + response.file_url;
            _this.pdfError = false;
            uploaderPdf.clearQueue();
            uploaderPdf.queue[0] = item;
            _this.student.resume = response.id;
            _this.student.resume_url = response.file_url;
            console.log(response)
        }

        _this.removePdf = function() {
            uploaderPdf.queue[0].remove();
            _this.student.resume = {};
            _this.student.resume_url = '';
            _this.pdfError = false;
        }


        var uploaderImg = _this.uploaderImg = new FileUploader({
            url: $rootScope.baseUrl+'/images/',
            headers: {
                Authorization: auth
            },
            autoUpload: true
        });
        uploaderImg.filters.push({
            name: 'typeFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });
        var imgSize = 1;
        uploaderImg.filters.push({
            name: 'sizeFilter',
            fn: function(item, options) {
                return (item.size/1042/1042 <= imgSize)
            }
        });

        _this.imgError = false;
        uploaderImg.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            _this.imgError = true;
            if (filter.name == 'typeFilter') {
                _this.imgErrorInfo = 'You submit should be image file'
            }
            if (filter.name == 'sizeFilter') {
                _this.imgErrorInfo = 'You submit should be less than '+ imgSize + 'MB'
            }
        };
        uploaderImg.onAfterAddingFile = function(fileItem) {
            // console.log(fileItem)
            _this.imgError = false;
        };
        uploaderImg.onErrorItem = function(item, response, status, headers) {
            _this.imgError = true;
            _this.imgErrorInfo = 'Server Error'
        }
        uploaderImg.onSuccessItem = function(item, response, status, headers) {
            _this.imgError = false;
            uploaderImg.clearQueue();
            uploaderImg.queue[0] = item;
            _this.student.image = response.id;
            _this.student.image_url = response.file_url;
            // console.log(response)
        }
        _this.removeImg = function() {
            uploaderImg.queue[0].remove();
            _this.student.image = {};
            _this.student.image_url = '';
            _this.imgError = false;
        }

        _this.show_collapseEdu = false;
        _this.show_collapseExp = false;
        _this.openPanel = function(name) {
            var panelName = 'show_'+name;
            _this[panelName] = !_this[panelName];
        }
  });
