'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:LoginPopCtrl
 * @description
 * # LoginPopCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('LoginPopCtrl', function ($scope, $location, $modalInstance, AuthService, $routeParams, $rootScope) {
        $scope.signUp = function() {
            // console.log('test')
            $modalInstance.close();
            $location.search({redirect: $location.path()}).path('user/create')
        }

        $scope.error = false;
        $scope.success = false;
        $scope.login = function () {
            var loginPromise = AuthService.login($scope.credential);
            loginPromise.then(function(data){
                $scope.error = false;
                // console.log('success');
                $scope.success = true;
                if ($routeParams.redirect) {
                    $location.path($routeParams.redirect).search({});
                } else {
                    $modalInstance.close();
                }
            }, function(status) {
                $scope.error = true;
                $scope.success = false;
                switch (status) {
                    case 'noMatch':
                        $scope.errorInfo = "Email and password doesn't match";
                        break;
                    case 'serverError':
                        $scope.errorInfo = $rootScope.defaultErrorInfo;
                        break;
                    default :
                        $scope.errorInfo = $rootScope.defaultErrorInfo;
                }
            })
        }
  });
