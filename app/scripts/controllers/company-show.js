'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanyProfileCtrl
 * @description
 * # CompanyProfileCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('CompanyProfileCtrl', function ($scope, Restangular, $routeParams, $anchorScroll, AuthService, $sce) {

        var _this = this;
        var id = $routeParams.id;
        _this.user_id = AuthService.getId();
        _this.companyId = id;
        Restangular.one('organizations',id).get().then(function(data){
            console.log(data)
            _this.organization = data;
            if(_this.organization.image){
                _this.organization.image_url = _this.organization.image.file_url;
                _this.organization.image = _this.organization.image.id;
            }
            _this.organization.description = $sce.trustAsHtml(_this.organization.description);
        })
        _this.searchErrorEvent = false;
        _this.keywordsEvent = '';
        _this.doSearchEvent = function() {
            var queryParams = {};
            queryParams.search = _this.keywordsEvent;
            queryParams.page = _this.currentPageEvent;
            Restangular.one('events').get(queryParams).then(function (data) {
                console.log(data);
                _this.searchErrorEvent = false;
                _this.resultsEvent = data.results;
                _this.countEvent = data.count;
                $anchorScroll();
            }, function () {
                _this.searchErrorEvent = true;
            })
        }
        _this.doNewSearchEvent = function(){
            _this.currentPageEvent = 1;
            _this.countEvent = 0;
            _this.doSearchEvent();
        }
        _this.doNewSearchEvent();

        /*_this.company = {
            "name":"Samsung Electronics Inc.",
            "aboutus":"This is the organizer description, This is the event description, ",
            "location":"Cambridge, MA",
            "extUrl": "http://www.samsung.com",
            "industry": "Manufacturing",
            "scale": 5,
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "jobs": [{"title":"PHP Developer", "locations":["Cambridge, MA"],
                "description": "This is a job description", "url":"job/show" },
                {"title":"Project Manager", "locations":["Cambridge, MA", "Boston, MA"],
                    "description": "This is another job description", "url":"job/show" },
                {"title":"Project Manager", "locations":["Cambridge, MA", "Boston, MA", "China"],
                    "description": "This is a third job description", "url":"job/show" }],
            "saved":false
        };*/



        /*_this.getLocationStr = function(locations){
            if (locations.length > 0){
                // console.log("locations count > 0");
                var locationStr = "";
                for (var i = 0; i < locations.length; i++) {
                    if(i == locations.length - 1){
                        locationStr += locations[i];
                    }else{
                        locationStr += locations[i] +", ";
                    }
                }
                return locationStr;
            }else{
                return "Unavailable";
            }
        };

        _this.getSavedStr = function(){
            if (_this.company.saved){
                return "Unsave";
            }else{
                return "Save This Company";
            }
        };

        _this.saveClick  = function() {
            _this.company.saved = !_this.company.saved;
            if(_this.company.saved){
                console.log("Save company clicked")
            }else{
                console.log("Unsave company clicked")
            }
        };*/
    });
