'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:PrivacyPolicyCtrl
 * @description
 * # PrivacyPolicyCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('PrivacyPolicyCtrl', function ($scope) {
        $scope.goToTop = function() {
            $location.hash('top');
            $anchorScroll();
        }
  });
