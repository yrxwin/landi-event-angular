'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentSignupCtrl
 * @description
 * # StudentSignupCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('UserCreateCtrl', function ($scope, $location, $anchorScroll, $http, localStorageService, AuthService, $rootScope) {

        var _this = this;
        _this.user ={};
        _this.error = false;
        _this.identities = [
            {
                value: 1,
                text: 'Individual/Student'
            },
            {
                value: 2,
                text: 'Organization/Company'
            }]
        // Use integer code for identity
        // 0 for admin, 1 for student, 2 for company, 3 for organizer.
        _this.user = {};

        _this.checkEmail = function() {
            $http.post('/api/verifyEmail', _this.user.email).success(function(data){
                if (data.user_exist) {

                }
            }).error(function(data){
                console.log(data)
            })
        };

        var login = function() {
            var credential = {};
            credential.username = _this.user.email;
            credential.password = _this.user.password;
            var loginPromise = AuthService.login(credential);
            loginPromise.then(function(data){
                _this.error = false;
                $('#submit').button('reset')
                switch (_this.user.identity) {
                    case '1':
                        $location.path('/student/create');
                        break;
                    case '2':
                        $location.path('/company/create');
                        break;

                    default:
                        _this.error = true;
                }
                // console.log('success');
            }, function(status) {
                _this.error = true;
                $('#submit').button('reset')
                switch (status) {
                    case 'serverError':
                        _this.errorInfo = "Server Error";
                        break;
                    default :
                        _this.errorInfo = "Something wrong"
                }
            })
        }


        var validationPassed = function() {
            $('#submit').button('loading');
            console.log(_this.user)
           /* if (!_this.user.identity) {
                _this.error = true;
                $('#submit').button('loading');
                return;
            }*/
            switch (_this.user.identity){
                case '1':
                    /*_this.user.first_name = 'first_name';
                    _this.user.last_name = 'last_name';
                    _this.user.high_degree = 'Phd';*/
                    // console.log(_this.user);
                    $http.post($rootScope.baseUrl+'/students/', _this.user).
                        success(function(data, status, headers, config) {
                            // console.log(data)
                            login();
                        }).
                        error(function(data, status, headers, config) {
                            if (status == '400') {
                                _this.errorInfo = JSON.stringify(data)
                            }
                            _this.error = true;
                            $('#submit').button('reset');
                        });
                    break;
                case '2':
                    _this.user.company_name = 'company_name';
                    _this.user.contact_name = 'contact_name';
                    // console.log(_this.user);
                    $http.post($rootScope.baseUrl+'/organizations/', _this.user).
                        success(function(data, status, headers, config) {
                            console.log(data);
                            login();
                        }).
                        error(function(data, status, headers, config) {
                            if (status == '400') {
                                _this.errorInfo = JSON.stringify(data)
                            }
                            _this.error = true;
                            $('#submit').button('reset');
                        });
                    break;
                /*case '3':
                    _this.user.organization_name = 'organization_name';
                    _this.user.contact_name = 'contact_name';
                    // console.log(_this.user);
                    $http.post($rootScope.baseUrl+'/organizers/', _this.user).
                        success(function(data, status, headers, config) {
                            login();
                        }).
                        error(function(data, status, headers, config) {
                            if (status == '400') {
                                _this.errorInfo = JSON.stringify(data)
                            }
                            _this.error = true;
                            $('#submit').button('reset');
                        });
                    // $location.path("organizer/create");
                    break;*/
                default:
                    _this.error = true;
                    $('#submit').button('reset');
            }
        }

        $('#user').validate({
            submitHandler: function (form) {
                _this.error = false;
                validationPassed();
            },
            rules: {
                email: "required",
                identity: "required",
                agreement: "required",
                password: {
                    required: true,
                    minlength: 6
                },
                password2: {
                    required: true,
                    equalTo: "#inputPassword"
                }
            }
        });
    });
