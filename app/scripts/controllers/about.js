'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
