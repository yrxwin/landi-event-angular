'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentProfileCompanyCtrl
 * @description
 * # StudentProfileCompanyCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('StudentCompanyCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
