'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:EventCompanyCtrl
 * @description
 * # EventCompanyCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('EventCompanyCtrl', function ($scope, $location) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        var _this = this;
        _this.event = {
            "title":"This is the event title",
            "description":"This is the event description, This is the event description, ",
            "start":"2014/12/23 10:30:PM",
            "end":"2014/12/23 10:30:PM",
            "location":"30 Main St, Melrse, MA, 02176",
            "organizer":{"title":"MIT Ventureship club",
                "url":"organizer/show"},
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "partStus": [{"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology", "url":"student/show"},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology", "url":"student/show"},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology", "url":"student/show"}],
            "anno":"This is an announcement",
            "saved":false,
            "attend":false
        };
        _this.searchStr = "";

        _this.getAttendStr = function(){
            if (_this.event.attend){
                return "Reserved";
            }else{
                return "Attend";
            }
        };
        _this.getSavedStr = function(){
            if (_this.event.saved){
                return "Unsave";
            }else{
                return "Save This Event";
            }
        };

        _this.attendClick = function() {
            if(!_this.event.attend){
                _this.event.attend = true;
                console.log("Participate clicked");
                $location.path('/attendEventAction');
            }
        };

        _this.saveClick  = function() {
            _this.event.saved = !_this.event.saved;
            if(_this.job.saved){
                console.log("Save job clicked")
            }else{
                console.log("Unsave job clicked")
            }
        }

        _this.searchStudent = function(){
            if(_this.searchStr.length > 1 ){
                console.log("Search with _this.searchStr")
            }else{
                console.log("Search with no keyword but this event")
            }
        }
  });
