'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:UserAgreementCtrl
 * @description
 * # UserAgreementCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('UserAgreementCtrl', function ($scope, $location, $anchorScroll) {
        $scope.goToTop = function() {
            $location.hash('top');
            $anchorScroll();
        }
  });
