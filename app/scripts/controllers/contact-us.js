'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:ContactUsCtrl
 * @description
 * # ContactUsCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('ContactUsCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
