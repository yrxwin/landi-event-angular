'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:EventShowCtrl
 * @description
 * # EventShowCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('EventShowCtrl', function ($scope, $location, Restangular, $routeParams, $rootScope, $anchorScroll, $sce, AuthService) {

        var _this = this;
        var id = $routeParams.id;
        _this.eventId = id;
        _this.userId = AuthService.getId();
        _this.showLogin = function(){
            $rootScope.$broadcast('showLogin');
        }

        switch ($rootScope.identity) {
            case 'organization':
                var joinUrl = 'events/'+id+'/company_applications';
                break;
            case 'student':
                var joinUrl = 'events/'+id+'/student_applications';
                break;
            default:
                _this.joinError = true;
        }
        _this.joinError = false;
        _this.joinAlready = false;
        _this.joinContent = 'Join Event';

        var pickStatus = function(status){
            switch(status) {
                case 0:
                    _this.joinContent = 'Applied';
                    _this.joinAlready = true;
                    break;
                case 1:
                    _this.joinContent = 'Checked by organizer';
                    _this.joinAlready = true;
                    break;
                case 2:
                    _this.joinContent = 'Approved';
                    _this.joinAlready = true;
                    break;
                case 3:
                    _this.joinContent = 'Rejected';
                    _this.joinAlready = true;
                    break;
                default:
                    _this.joinContent = $rootScope.defaultErrorInfo;
            }
        }
        Restangular.one('events',id).get().then(function(data){
            console.log(data)
            _this.event = data;
            if (data.image && data.image.file_url) {
                _this.event.image_url = data.image.file_url;
                _this.event.image = data.image.id;
            }
            _this.event.description = $sce.trustAsHtml(_this.event.description);
            var searchObj = {};
            var joinObj = $rootScope.identity + '__id';
            searchObj[joinObj] = _this.userId;
            Restangular.oneUrl(joinUrl).get(searchObj).then(function(data){
                var pick = null;
                var obj = $rootScope.identity;
                if ($rootScope.identity == 'organization'){
                    obj = 'company';
                }
                data.results.forEach(function(rel){
                    if (rel[obj].id == _this.userId){
                        pick = rel;
                        return;
                    }
                })
                // if (data.count > 0) {
                if (pick) {
                    pickStatus(pick.status);
                    _this.status = pick.status;
                }
            })
        })

        _this.searchErrorComp = false;
        _this.keywordsComp = '';
        _this.doSearchComp = function() {
            var queryParams = {};
            queryParams.search = _this.keywordsComp;
            queryParams.page = _this.currentPageComp;
            Restangular.one('events',id).customGET('companies', queryParams).then(function (data) {
                console.log(data);
                _this.searchErrorComp = false;
                _this.resultsComp = data.results;
                _this.countComp = data.count;
                $anchorScroll();
            }, function () {
                _this.searchErrorComp = true;
            })
        }
        _this.doNewSearchComp = function(){
            _this.currentPageComp = 1;
            _this.countComp = 0;
            _this.doSearchComp();
        }
        _this.doNewSearchComp();

        _this.searchErrorStu = false;
        _this.keywordsStu = '';
        _this.doSearchStu = function() {
            var queryParams = {};
            queryParams.search = _this.keywordsStu;
            queryParams.page = _this.currentPageStu;
            Restangular.one('events',id).customGET('students', queryParams).then(function (data) {
                console.log(data);
                _this.searchErrorStu = false;
                _this.resultsStu = data.results;
                _this.countStu = data.count;
                $anchorScroll();
            }, function (res) {
                if (res.status == 403){
                    _this.searchErrorStuInfo = 'Only approved companies can view this information'
                }
                _this.searchErrorStu = true;
            })
        }
        _this.doNewSearchStu = function(){
            _this.currentPageStu = 1;
            _this.countStu = 0;
            _this.doSearchStu();
        }
        _this.doNewSearchStu();


        _this.joinEvent = function(){
            Restangular.oneUrl(joinUrl).post().then(function(data){
                pickStatus(data.status);
            },function(res){
                console.log(res)
                if (res.status==400){
                    _this.joinContent = 'Already Applied';
                    _this.joinAlready = true;
                    _this.joinError = false;
                    return;
                }
                _this.joinError = true;
                _this.joinAlready = false;
                _this.joinContent = $rootScope.defaultErrorInfo;
            })
        }



        /*_this.companySearch = function(){
            var searchParams = {};
            searchParams.page = _this.currentPageComp;
            Restangular.one('events',id).customGET('company_applications').then(function(data){
                console.log(data)
            })
        }
        _this.companySearch();*/

        /*_this.event = {
            "title":"This is the event title",
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-16T14:25:00.000Z",
            "location":"30 Main St, Melrse, MA, 02176",
            "organizer":{"title":"MIT Ventureship club",
                        "url":"organizer/show"},
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "partComps": [{"title":"Samsung Electronics", "url":"http://www.samsung.com" },
                              {"title":"Cisco Telecommunication Company", "url":"http://www.samsung.com" },
                              {"title":"This is another company", "url":"http://www.samsung.com"}],
            "annos":[{"title":"Announcement1", "body":"This is announcement1"},
                {"title":"Announcement2", "body":"This is announcement2"},
                {"title":"Announcement3", "body":"This is announcement3"}],
            "saved":false,
            "attend":false
        };*/
        /*_this.searchStr = "";

        _this.getAttendStr = function(){
            if (_this.event.attend){
                return "Reserved";
            }else{
                return "Attend";
            }
        };
        _this.getSavedStr = function(){
            if (_this.event.saved){
                return "Unsave";
            }else{
                return "Save This Event";
            }
        };

        _this.attendClick = function() {
            if(!_this.event.attend){
                _this.event.attend = true;
                console.log("Participate clicked");
                //$location.path('/attendEventAction');
            }
        };

        _this.saveClick  = function() {
            _this.event.saved = !_this.event.saved;
            if(_this.job.saved){
                console.log("Save job clicked")
            }else{
                console.log("Unsave job clicked")
            }
        };*/
        /*
        _this.searchCompany = function(){
            if(_this.searchStr.length > 1 ){
                console.log("Search with _this.searchStr")
            }else{
                console.log("Search with no keyword but this event")
            }
        };
        _this.viewAll = function(){
            $location.path('company/search')
        }; */

        _this.map = { center: { latitude: 45, longitude: -73 }, zoom: 8 };

    });
