'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:JobCreateCtrl
 * @description
 * # JobCreateCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp').controller('JobCreateCtrl', function ($scope, $location, AuthService, Restangular, pdfToText, FileUploader, $rootScope) {
    var _this = this;
    _this.titleFields = ['name'];

    var auth = 'Token ' + AuthService.getToken();
    var uploaderPdf = _this.uploaderPdf = new FileUploader({
        url: $rootScope.baseUrl+ '/documents/',
        headers: {
            Authorization: auth
        },
        autoUpload: true
    });

    // FILTERS

    uploaderPdf.filters.push({
        name: 'typeFilter',
        fn: function(item, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|pdf|PDF|'.indexOf(type) !== -1;
        }
    });
    var pdfSize = 1;
    uploaderPdf.filters.push({
        name: 'sizeFilter',
        fn: function(item, options) {
            return (item.size/1042/1042 <= pdfSize)
        }
    });

    _this.pdfError = false;
    uploaderPdf.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        _this.pdfError = true;
        if (filter.name == 'typeFilter') {
            _this.pdfErrorInfo = 'You submit should be pdf file'
        }
        if (filter.name == 'sizeFilter') {
            _this.pdfErrorInfo = 'You submit should be less than '+ pdfSize + 'MB'
        }
    };
    uploaderPdf.onAfterAddingFile = function(fileItem) {
        _this.pdfError = false;
    };
    uploaderPdf.onErrorItem = function(item, response, status, headers) {
        _this.pdfError = true;
        _this.pdfErrorInfo = 'Server Error'
    }
    uploaderPdf.onSuccessItem = function(item, response, status, headers) {
        var fileUrl = $rootScope.amazonUrl+response.file_url;
        /*pdfToText(fileUrl,function(){},function(content){
            Restangular.one('documents',response.id).patch({parsed_text:content}).then(function(){*/
                _this.pdfError = false;
                uploaderPdf.clearQueue();
                uploaderPdf.queue[0] = item;
                _this.job.document = response.id;
                _this.job.document_url = response.file_url;
                console.log(response)
            /*},function(){
                _this.pdfError = true;
            })
        })*/
    }
    _this.removePdf = function() {
        uploaderPdf.queue[0].remove();
        _this.job.document = {};
        _this.job.document_url = '';
        _this.pdfError = false;
    }

    _this.job = {};
    _this.job.locations = [];

    _this.newLoc = {};

    _this.changeFormat = function () {
        _this.newLoc.formatted_address = (_this.newLoc.line1 ? (_this.newLoc.line1 + ', ') : '')
            + (_this.newLoc.city ? (_this.newLoc.city + ', ') : '')
            + (_this.newLoc.state ? (_this.newLoc.state) + ' ' : '')
            + (_this.newLoc.zip_code ? (_this.newLoc.zip_code + ', ') : '')
            + (_this.newLoc.country ? (_this.newLoc.country) : '');
        _this.searchText = '';
    }
        /*_this.newLoc = {};
        _this.job.locations = {};
        _this.addLocation = function() {
            console.log(_this.job.newLoc);
            if ((_this.job.locations.indexOf(_this.job.newLoc) > -1) && (_this.job.newLoc.length)){
                _this.locationError = true;
                _this.locationErrorInfo = 'This location already exists';
                return;
            }
            if(_this.newLocation){
                _this.locationError = false;
                _this.locationErrorInfo = {};
                _this.job.locations.push(_this.job.newLoc);
                _this.job.newLoc = null;
            }
        };*/
    _this.removeLocation = function (location) {
        _this.job.locations.splice(_this.job.locations.indexOf(location), 1);
    };

    _this.error = false;
    _this.errorInfo = '';

    $('#due').datetimepicker({
        pickTime: false
    });
    $("#due").on("dp.change",function (e) {
        var scope = angular.element($('#due')).scope();
        scope.$apply(function(){
            _this.job.due = moment(e.date)
        });
    });

    var validationPassed = function () {
        var user_id = AuthService.getId();
        _this.job.organization = user_id;
        _this.job.title = _this.job.title.id;
        Restangular.all('jobs').post(_this.job).then(function (data) {
            $location.path('job/edit/'+data.id)
        }, function () {
            _this.errorInfo = 'Server Error';
            _this.error = true;
        })
    };

    $('#job').validate({
        rules: {
            title: "required",
            due: "required",
            type: "required",
            abstract: {
                required: 'true',
                rangelength: [1, 150]
            }
        }
    });

    _this.submitClick = function () {
        if ($('#job').valid()) {
            console.log(_this.job);
            _this.error = false;
            validationPassed();
        } else {
            _this.error = true;
            _this.errorInfo = 'please correct the highlighted fields'
        }
    }

    _this.addLoc = function () {
        Restangular.all('locations').post(_this.newLoc).then(function(data){
            _this.job.locations.push(data.id);
            _this.student.exps_show.push(data);
            _this.newExp = {};
            _this.expError = false;
            _this.expErrorInfo = '';
        }, function(){
            _this.expError = true;
        })
        if ((_this.job.locations.indexOf(_this.newLoc) > -1) && (_this.job.locations.length)) {
            _this.locError = true;
            _this.locErrorInfo = 'This location info has existed'
            return;
        }
        _this.locError = false;
        _this.locErrorInfo = {};
        _this.job.locations.push(_this.newLoc);
        _this.newLoc = {};
    };
    _this.removeLoc = function (loc) {
        _this.job.locations.splice(_this.job.locations.indexOf(loc), 1);
    }

    _this.clearNewLoc = function () {
        _this.newLoc = {};
        locationValidator.resetForm();
    };

    var locationValidator = $('#location').validate({
        submitHandler: function (form) {
            _this.locError = false;
            _this.addLoc();
        },
        rules: {
            zip_code: "number",
            city: "required",
            state: "required",
            country: "required"
        }
    })

  });
