'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CookiepolicyCtrl
 * @description
 * # CookiepolicyCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('CookiePolicyCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
