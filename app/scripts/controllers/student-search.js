'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentSearchCtrl
 * @description
 * # StudentSearchCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp').controller('StudentSearchCtrl', function ($scope) {


    $scope.acadLevels = [
        {name: 'Bachelor', code: 'bs', abbr:'B.S.'},
        {name: 'Master', code: 'ms', abbr:'M.S.'},
        {name: 'Doctor', code: 'phd', abbr:'Ph.D.'},
        {name: 'Post Doc', code: 'postdoc', abbr:'Post-doc.'},
        {name: 'Other', code: 'other', abbr:'Other'},
        {name: 'Unknown', code: 'ua', abbr:''}
    ];
    $scope.gradDates = [
        {name: 'All', code: 0},
        {name: 'Graduated', code: 1},
        {name: 'Spring 2015', code: 2},
        {name: 'Summer 2015', code: 3},
        {name: 'Fall 2015', code: 4},
        {name: 'Spring 2016', code: 5}
    ];

    var _this = this;
    _this.search = {
        "keyword":"Northeastern"
        // Location
        // Optional refining conditions returned in the JSON object
    };
    _this.results = [{ "firstName":"Renze",
        "lastName":"Wang",
        "level":1,
        "primMajor":"Mechanical Engineering",
        "primEdu":"Cambridge University",
        "primGradDate":"Spring 2015",
        "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
        "url":"student/show"},
        { "firstName":"Renze",
            "lastName":"Wang",
            "level":1,
            "primMajor":"Mechanical Engineering",
            "primEdu":"Cambridge University",
            "primGradDate":"Spring 2015",
            "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "url":"student/show"},
        { "firstName":"Renze",
            "lastName":"Wang",
            "level":1,
            "primMajor":"Mechanical Engineering",
            "primEdu":"Cambridge University",
            "primGradDate":"Spring 2015",
            "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "url":"student/show"}
    ];



    _this.getAcadLevelAbbr = function(edu){
        switch(edu.level){
            case 'bs':
                return "B.S.";
            case 'ms':
                return "M.S.";
            case 'phd':
                return "Ph.D.";
            case 'postdoc':
                return "Post-doc";
            case 'other':
                return "Other";
            default:
                return null;
        }
    };

    _this.getStudentName = function(student){

        return student.firstName + " " + student.lastName
    };

    _this.loadMore = function(){
        console.log("load more search results");
    };
    _this.addName = function(){
        if(_this.newName && _this.newName.length > 0){
            _this.search.name = _this.newName;
            _this.newName = null;
        }else{
            _this.newName = null;
        }
    };

    _this.removeName = function(){
        _this.search.name = null;
    };

    _this.addEducation = function(){
        if(_this.newEducation && _this.newEducation.length > 0){
            _this.search.education = _this.newEducation;
            _this.newEducation = null;
        }else{
            _this.newEducation = null;
        }
    };

    _this.removeEducation = function(){
        _this.search.education = null;
    };

    _this.addMajor = function(){
        if(_this.newMajor && _this.newMajor.length > 0){
            _this.search.major = _this.newMajor;
            _this.newMajor = null;
        }else{
            _this.newMajor = null;
        }
    };
    _this.removeMajor = function(){
        _this.search.major = null;
    };


    _this.addLocation = function(){
        if(_this.newLocation && _this.newLocation.length > 0){
            _this.search.location = _this.newLocation;
            _this.newLocation = null;
        }else{
            _this.newLocation = null;
        }
    };
    _this.removeLocation = function(){
        _this.search.location = null;
    };
    _this.addMajor = function(){
        if(_this.newMajor && _this.newMajor.length > 0){
            _this.search.major = _this.newMajor;
            _this.newMajor = null;
        }else{
            _this.newMajor = null;
        }
    };
    _this.removeMajor = function(){
        _this.search.major = null;
    };

    _this.removeLevel = function(){
        _this.search.level = null;
    };
    _this.removeGradDate = function(){
        _this.search.gradDate = null;
    }

});
