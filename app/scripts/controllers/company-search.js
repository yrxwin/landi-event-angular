'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanySearchCtrl
 * @description
 * # CompanySearchCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp').controller('CompanySearchCtrl', function ($scope) {
        $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];
    var _this = this;
    _this.search = {
        "keyword":"Northeastern",
        "event":"MIT Career Fair Spring 2015"
        // Optional refining conditions returned in the JSON object
    };
    _this.results =[{ "name":"Google Inc.",
        "url":"company/show",
        "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
        "industry":"Manufacturing",
        "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "location":"Cambridge, MA"},
        { "name":"Google Inc.",
            "url":"company/show",
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "industry":"Manufacturing",
            "location":"Cambridge, MA"}
    ];
    _this.addName = function(){
        if(_this.newName && _this.newName.length > 0){
            _this.search.name = _this.newName;
            _this.newName = null;
        }else{
            _this.newName = null;
        }
    };

    _this.removeName = function(){
        _this.search.name = null;
    };

    _this.addLocation = function(){
        if(_this.newLocation && _this.newLocation.length > 0){
            _this.search.location = _this.newLocation;
            _this.newLocation = null;
        }else{
            _this.newLocation = null;
        }
    };
    _this.removeLocation = function(){
        _this.search.location = null;
    };

    _this.addEvent = function(){
        if(_this.newEvent && _this.newEvent.length > 0){
            _this.search.event = _this.newEvent;
            _this.newEvent = null;
        }else{
            _this.newEvent = null;
        }
    };
    _this.removeEvent = function(){
        _this.search.event = null;
    };

});
