'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:UserFinishCtrl
 * @description
 * # UserFinishCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('UserFinishCtrl', function ($scope, $routeParams, $timeout, $location) {

        var _this = this;
        _this.redirect = $routeParams.redirect;
        if (_this.redirect) {
            _this.counter = 10;
            _this.onTimeout = function () {
                _this.counter--;
                if (_this.counter === 0){
                    $location.path(_this.redirect);
                }
                mytimeout = $timeout(_this.onTimeout, 1000);
            }
            var mytimeout = $timeout(_this.onTimeout, 1000);

            _this.stop = function () {
                $timeout.cancel(mytimeout);
            }
        }
        /*_this.account = {"email":"abc@mit.edu"};
        _this.getEmailExt = function(){
            if(_this.account.email!= null && _this.account.email.length > 3){
                var components = _this.account.email.split("@");
                if(components.length == 2){
                    return components[1];
                }
            }
            return null
        }*/
  });
