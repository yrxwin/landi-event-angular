'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:StudentProfileEditCtrl
 * @description
 * # StudentProfileEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('StudentEditCtrl', function ($scope, Restangular, AuthService, FileUploader, $sce, $rootScope, $route, $filter, pdfToText) {

        var _this = this;
        var user_id = AuthService.getId();
        Restangular.one('students',user_id).get().then(function(data){
            _this.student = data;
            if (data.image && data.image.file_url) {
                _this.student.image_url = data.image.file_url;
                _this.student.image = data.image.id;
            }
            if (data.resume && data.resume.file_url){
                var resumeUrl = 'https://s3.amazonaws.com/landi' + data.resume.file_url;
                _this.resumeUrl = $sce.trustAsResourceUrl(resumeUrl);
                _this.student.resume = data.resume.id;
            }
            _this.student.educations_show = _this.student.educations;
            _this.student.educations = _.pluck(_this.student.educations,'id');
            _this.student.exps_show = _this.student.exps;
            _this.student.exps = _.pluck(_this.student.exps,'id');
        })

        _this.updateFirstName = function(data){
            Restangular.one('students',user_id).patch({first_name:data}).then(function(){

            },function(){
                return $rootScope.defaultErrorInfo
            })
        }
        _this.updateLastName = function(data){
            Restangular.one('students',user_id).patch({last_name:data}).then(function(){

            },function(){
                return $rootScope.defaultErrorInfo
            })
        }
        _this.updateStudent = function(data, field){
            var updateObj = {};
            updateObj[field] = data
            console.log(updateObj)
            Restangular.one('students',user_id).patch(updateObj).then(function(){

            },function(){
                return $rootScope.defaultErrorInfo
            })
        }

        _this.descriptionEdit = false;
        _this.desError = false;

        _this.runEdit = function(name){
            _this[name+'_temp'] = _this.student[name];
            _this[name+'Edit'] = !_this[name+'Edit'];
        }

        var descriptionValidator = $('#description').validate({
            submitHandler: function (form) {
                Restangular.one('students',user_id).patch({description:_this.description_temp}).then(function(data){
                    _this.student.description = _this.description_temp;
                    _this.desError = false;
                    _this.descriptionEdit = false;
                },function(){
                    _this.desError = true;
                })
            },
            rules: {
                description: {
                    rangelength: [0, 50]
                }
            }
        })


        _this.birth_dateEdit = false;
        _this.birthError = false;
        $('#birth-input').datetimepicker({
            pickTime: false
        });
        $("#birth-input").on("dp.change",function (e) {
            var scope = angular.element($('#birth-input')).scope();
            scope.$apply(function(){
                _this.birth_date_temp = moment(e.date)
            });
        });

        var birthValidator = $('#birth').validate({
            submitHandler: function (form) {
                Restangular.one('students',user_id).patch({birth_date:_this.birth_date_temp}).then(function(data){
                    _this.student.birth_date = _this.birth_date_temp;
                    _this.birthError = false;
                    _this.birth_dateEdit = false;
                },function(){
                    _this.birthError = true;
                })
            },
            rules: {
                birth: {
                }
            }
        })

        var auth = 'Token ' + AuthService.getToken();
        var uploaderImg = _this.uploaderImg = new FileUploader({
            url: $rootScope.baseUrl + '/images/',
            headers: {
                Authorization: auth
            },
            autoUpload: true
        });
        uploaderImg.filters.push({
            name: 'typeFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });
        var imgSize = 1;
        uploaderImg.filters.push({
            name: 'sizeFilter',
            fn: function(item, options) {
                return (item.size/1042/1042 <= imgSize)
            }
        });

        _this.imgError = false;
        uploaderImg.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            _this.imgError = true;
            if (filter.name == 'typeFilter') {
                _this.imgErrorInfo = 'You submit should be image file'
            }
            if (filter.name == 'sizeFilter') {
                _this.imgErrorInfo = 'You submit should be less than '+ imgSize + 'MB'
            }
        };
        uploaderImg.onAfterAddingFile = function(fileItem) {
            // console.log(fileItem)
            _this.imgError = false;
        };
        uploaderImg.onErrorItem = function(item, response, status, headers) {
            _this.imgError = true;
            _this.imgErrorInfo = 'Server Error'
        }
        uploaderImg.onSuccessItem = function(item, response, status, headers) {
            Restangular.one('students',user_id).patch({image:response.id}).then(function(data){
                uploaderImg.clearQueue();
                uploaderImg.queue[0] = item;
                _this.imgError = false;
                _this.student.image = response.id;
                _this.student.image_url = response.file_url;
            }, function(){
                _this.imgError = true;
            })
            /*Restangular.one('students',user_id).patch({image:response.id}).then(function(data){
                if (_this.student.image) {
                    Restangular.one('images',_this.student.image).remove().then(function(){
                        uploaderImg.clearQueue();
                        uploaderImg.queue[0] = item;
                        _this.imgError = false;
                        _this.student.image = response.id;
                        _this.student.image_url = response.file_url;
                    },function(){
                        _this.imgError = true;
                    })
                } else {
                    uploaderImg.clearQueue();
                    uploaderImg.queue[0] = item;
                    _this.imgError = false;
                    _this.student.image = response.id;
                    _this.student.image_url = response.file_url;
                }
            }, function(){
                _this.imgError = true;
            })*/

            // console.log(response)
        }
        _this.removeImg = function() {
            Restangular.one('students',_this.student.id).patch({image:null}).then(function(){
                if (uploaderImg.queue[0]){
                    uploaderImg.queue[0].remove();
                }
                _this.student.image = '';
                _this.student.image_url = '';
                _this.imgError = false;
            }, function(){
                _this.imgError = true;
            })
            /* Restangular.one('images',_this.student.image).remove().then(function(){
                if (uploaderImg.queue[0]){
                    uploaderImg.queue[0].remove();
                }
                _this.student.image = '';
                _this.student.image_url = '';
                _this.imgError = false;
            }, function(){
                _this.imgError = true;
            }) */
        }

        var uploaderPdf = _this.uploaderPdf = new FileUploader({
            url: $rootScope.baseUrl + '/resumes/',
            headers: {
                Authorization: auth
            },
            autoUpload: true
        });

        // FILTERS

        uploaderPdf.filters.push({
            name: 'typeFilter',
            fn: function(item, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|pdf|PDF|'.indexOf(type) !== -1;
            }
        });
        var pdfSize = 1;
        uploaderPdf.filters.push({
            name: 'sizeFilter',
            fn: function(item, options) {
                return (item.size/1042/1042 <= pdfSize)
            }
        });

        _this.pdfError = false;
        uploaderPdf.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            _this.pdfError = true;
            if (filter.name == 'typeFilter') {
                _this.pdfErrorInfo = 'You submit should be pdf file'
            }
            if (filter.name == 'sizeFilter') {
                _this.pdfErrorInfo = 'You submit should be less than '+ pdfSize + 'MB'
            }
        };
        uploaderPdf.onAfterAddingFile = function(fileItem) {
            _this.pdfError = false;
        };
        uploaderPdf.onErrorItem = function(item, response, status, headers) {
            _this.pdfError = true;
            _this.pdfErrorInfo = 'Server Error'
        }
        uploaderPdf.onSuccessItem = function (item, response, status, headers) {
            var fileUrl = $rootScope.amazonUrl + response.file_url;

            Restangular.one('students', user_id).patch({resume: response.id}).then(function () {
                uploaderPdf.clearQueue();
                uploaderPdf.queue[0] = item;
                _this.pdfError = false;
                _this.student.resume = response.id;
                var resumeUrl = 'https://s3.amazonaws.com/landi' + response.file_url;
                _this.resumeUrl = $sce.trustAsResourceUrl(resumeUrl);
                $route.reload();
            }, function () {
                _this.pdfError = true;
            })

        }

        _this.removePdf = function() {
            Restangular.one('students',_this.student.id).patch({resume:null}).then(function(){
                if (uploaderPdf.queue[0]){
                    uploaderPdf.queue[0].remove();
                }
                _this.student.resume = '';
                _this.student.resumeUrl = '';
                _this.pdfError = false;
                $route.reload();
            },function(){
                _this.pdfError = true;
            })
        }



        /*_this.getAcadLevelAbbr = function(edu){
            switch(edu.degree){
                case '1':
                    return "Bachelor";
                case '2':
                    return "Master";
                case '3':
                    return "Doctorate";
                default:
                    return 'Other';
            }
        };*/

        _this.addCourse = function(){
            if(_this.newCourse.length > 2){
                _this.student.courses.push(_this.newCourse);
                _this.newCourse = "";
            }
        };
        _this.removeCourse = function(course){
            _this.student.courses.splice(_this.student.courses.indexOf(course),1);
        };

        _this.removeExp = function(exp){
            _this.student.exps.splice(_this.student.exps.indexOf(exp),1);
        };
        _this.removeSkill = function(skill){
            _this.student.skills.splice(_this.student.skills.indexOf(skill),1);
        };

        _this.addExp = function() {
            console.log(_this.newExp);
            _this.student.exps.push(_this.newExp);
            _this.newExp = {};
        };
        _this.addSkill = function() {
            console.log(_this.newExp);
            _this.student.skills.push(_this.newSkill);
            _this.newSkill = {};
        };

        _this.clearNewExp = function(){
            _this.newExp = {};
        };
        _this.clearNewSkill = function(){
            _this.newSkill = {};
        };


        _this.getStudentName = function(){
            return _this.student.firstName + " " + _this.student.lastName;
        };

        _this.acadLevels = [
            {name: 'Bachelor', code: 1},
            {name: 'Master', code: 2},
            {name: 'Doctorate', code: 3},
            {name: 'Other', code: 0}
        ];

        _this.uniFields = ['name'];
        _this.majorFields = ['name'];
        _this.titleFields = ['name'];
        _this.corFields = ['name'];

        _this.newEdu = {};
        _this.newEdu.university = {};

        _this.addEdu = function() {
            // console.log(_this.newEdu);
            Restangular.all('educations').post(_this.newEdu).then(function(data){
                _this.student.educations.push(data.id);
                _this.student.educations_show.push(data);
                Restangular.one('students',user_id).patch({educations:_this.student.educations}).then(function(data){
                    _this.newEdu = {};
                    _this.eduError = false;
                    _this.eduErrorInfo = '';
                }, function(){
                    _this.student.educations.splice(-1,1);
                    _this.student.educations_show.splice(-1,1);
                    _this.eduError = true;
                })
            },function(){
                _this.eduError = true;
            })
        };

        _this.removeEdu = function(edu) {
            Restangular.one('educations', edu.id).remove().then(function(){
                _this.eduError = false;
                _this.student.educations.splice(_this.student.educations.indexOf(edu.id),1);
                _this.student.educations_show.splice(_this.student.educations_show.indexOf(edu),1);
                // console.log(_this.student.educations)
            }, function(){
                _this.eduError = true;
            })
        };

        _this.clearNewEdu = function() {
            _this.newEdu = {};
            educationValidator.resetForm();
        };

        var educationValidator = $('#education').validate({
            submitHandler: function (form) {
                if (!_this.newEdu.university && !_this.newEdu.majors){
                    _this.eduError = true;
                    _this.eduErrorInfo = 'The university and major fields are required';
                    return;
                } else if (!_this.newEdu.university){
                    _this.eduError = true;
                    _this.eduErrorInfo = 'The university field is required';
                    return;
                } else if (!_this.newEdu.majors){
                    _this.eduError = true;
                    _this.eduErrorInfo = 'The major field is required';
                }
                _this.newEdu.university = _this.newEdu.university.id;
                var major_id = _this.newEdu.majors.id;
                _this.newEdu.majors = [];
                _this.newEdu.majors.push(major_id); // many to many
                _this.addEdu();
            },
            rules: {
                university: "required",
                degree: "required",
                major: "required",
                gpa: {
                    number: true
                },
                gpaFull: {
                    number: true
                },
                start: "required"
            }
        })

        _this.newExp = {};

        _this.addExp = function() {

            Restangular.all('experiences').post(_this.newExp).then(function(data){
                _this.student.exps.push(data.id);
                _this.student.exps_show.push(data);
                Restangular.one('students',user_id).patch({exps:_this.student.exps}).then(function(data){
                    _this.newExp = {};
                    _this.expError = false;
                    _this.expErrorInfo = '';
                }, function(){
                    _this.student.exps.splice(-1,1);
                    _this.student.exps_show.splice(-1,1);
                    _this.eduError = true;
                })
            }, function(){
                _this.expError = true;
            })
        };

        _this.removeExp = function(exp) {
            Restangular.one('experiences', exp.id).remove().then(function(){
                _this.expError = false;
                _this.student.exps.splice(_this.student.exps.indexOf(exp.id),1);
                _this.student.exps_show.splice(_this.student.exps_show.indexOf(exp),1);
            }, function(){
                _this.expError = true;
            })
        }

        _this.clearNewExp = function() {
            _this.newExp = {};
            experienceValidator.resetForm();
        }

        var experienceValidator = $('#experience').validate({
            submitHandler: function (form) {
                if (!_this.newExp.company && !_this.newExp.title){
                    _this.expError = true;
                    _this.expErrorInfo = 'The company and title fields are required';
                    return;
                } else if (!_this.newExp.company){
                    _this.expError = true;
                    _this.expErrorInfo = 'The company field is required';
                    return;
                } else if (!_this.newExp.title){
                    _this.expError = true;
                    _this.expErrorInfo = 'The title field is required';
                }
                _this.newExp.company = _this.newExp.company.id;
                _this.newExp.title = _this.newExp.title.id;
                _this.addExp();
            },
            rules: {
                title: "required",
                company: "required",
                start: "required",
                type: "required",
                description: {
                    required: true,
                    rangelength: [1, 1000]
                }
            }
        });


        $('#start').datetimepicker({
            pickTime: false
        });
        $('#end').datetimepicker({
            pickTime: false
        });
        $("#start").on("dp.change",function (e) {
            var scope = angular.element($('#start')).scope();
            scope.$apply(function(){
                _this.newEdu.start_date = moment(e.date)
            });
            $('#end').data("DateTimePicker").setMinDate(e.date);
        });
        $("#end").on("dp.change",function (e) {
            var scope = angular.element($('#end')).scope();
            scope.$apply(function(){
                _this.newEdu.end_date = moment(e.date)
            });
            $('#start').data("DateTimePicker").setMaxDate(e.date);
        });

        $('#start-exp').datetimepicker({
            pickTime: false
        });
        $('#end-exp').datetimepicker({
            pickTime: false
        });
        $("#start-exp").on("dp.change",function (e) {
            var scope = angular.element($('#start-exp')).scope();
            scope.$apply(function(){
                _this.newExp.start_date = moment(e.date)
            });
            $('#end-exp').data("DateTimePicker").setMinDate(e.date);
        });
        $("#end-exp").on("dp.change",function (e) {
            var scope = angular.element($('#end-exp')).scope();
            scope.$apply(function(){
                _this.newExp.end_date = moment(e.date)
            });
            $('#start-exp').data("DateTimePicker").setMaxDate(e.date);
        });

        /*_this.student = {
            "firstName":"Super",
            "lastName":"Star",
            "aboutme":"This is the student self-description, This is the student self-description,",
            "location":"30 Main St, Melrse, MA, 02176",
            "primEdu": "Northeastern University",
            "primMajor": "Mechanical Engineering",
            "email":"adis@mit.edu",
            "profileImg":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "edus": [{"university":"Massachusetts Institute of Technology", "GPA": 3.0, "GPAFull": 4.0,
                "start":"2014-12-18T14:25:00.000Z" , "end":"2014-12-18T14:25:00.000Z",
                "major": "Mathematics", "level":1 },
                {"university":"Northeastern University", "GPA": 3.0, "GPAFull": 5.0,
                    "start":"2014-12-18T14:25:00.000Z" , "end":"2014-12-18T14:25:00.000Z",
                    "major": "Mechanical Engineering", "level":2 }],
            "courses":["Image processing","Algorithms","Linear Algebra"],
            "exps":[{"title": "PHP Developer", "company":"Google Inc.",
                "start":"2014-12-18T14:25:00.000Z", "end":"Present", "description":"This is an experience description"},
                {"title": "PHP Developer", "company":"Google Inc.",
                    "start":"2014-12-18T14:25:00.000Z", "end":"2014-12-18T14:25:00.000Z", "description":"This is an experience description"},
                {"title": "PHP Developer", "company":"Google Inc.",
                    "start":"2014-12-18T14:25:00.000Z", "end":"2014-12-18T14:25:00.000Z", "description":"This is an experience description"}],
            "skills":[{"name": "Java", "rate":4,
                "description":"This is an experience description"},
                {"name": "C#", "rate":3,
                    "description":"This is an experience description"},
                {"name": "C++", "rate":5,
                    "description":"This is an experience description"},
                {"name": "Swift", "rate":2,
                    "description":"This is an experience description"}],
            "projects":[{"name": "Java", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                "description":"This is a project description"},
                {"name": "C#", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is another project description"},
                {"name": "C++", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is a third experience description"},
                {"name": "Swift", "img":"http://giant.gfycat.com/UnconsciousYawningArcticwolf.gif",
                    "description":"This is a fourth project description"}],
            "saved":false
        };*/

    });
