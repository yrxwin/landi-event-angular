'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
  .controller('LoginCtrl', function ($scope, AuthService, $location, $routeParams) {
        var _this = this;
        _this.credential = {};
        _this.error = false;
        _this.success = false;
        _this.login = function () {
            var loginPromise = AuthService.login(_this.credential);
            loginPromise.then(function(data){
                _this.error = false;
                // console.log('success');
                _this.success = true;
                if ($routeParams.redirect) {
                    $location.path($routeParams.redirect).search({});
                }
                $location.path('search');
            }, function(status) {
                _this.error = true;
                _this.success = false;
                switch (status) {
                    case 'noMatch':
                        _this.errorInfo = "Email and password doesn't match";
                        break;
                    case 'serverError':
                        _this.errorInfo = "Server Error";
                        break;
                    default :
                        _this.errorInfo = "Something wrong"
                }
            })
        }
  });
