'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:CompanyCreateCtrl
 * @description
 * # CompanyCreateCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('CompanyCreateCtrl', function ($scope, $location, AuthService, Restangular, $http, FileUploader, $rootScope) {
        var _this = this;

        _this.organization = {};
        _this.showAddress = false;
        _this.corFields = ['name'];

        var user_id = AuthService.getId();
        Restangular.one('organizations',user_id).get().then(function(data){
            console.log(data)
            _this.organization = data;
            if(_this.organization.image){
                _this.organization.image_url = _this.organization.image.file_url;
                _this.organization.image = _this.organization.image.id;
            }
        })

        /*_this.getCompany = function(val) {
            var queryParam = {name__icontains: val};
            return Restangular.oneUrl('corporations').get(queryParam).then(function(response){
                // console.log(response)
                return response.results.map(function(item){
                    return item.name;
                });
            });
        };*/

        _this.error = false;
        _this.errorInfo = '';

        var validationPassed = function(){
            if (_this.organization.web_url) {
                _this.organization.web_url = 'http://' + _this.organization.web_url;
            }
            var user_id = AuthService.getId();
            Restangular.one('organizations',user_id).patch(_this.organization).then(function(){
                $location.path('user/finish');
            },function(res){
                // _this.errorInfo = JSON.stringify(res.data);
                _this.error = true;
            })
            /*Restangular.all('locations').post(_this.company.address).then(function(data){
                console.log(data)
                _this.company.location = data.id;
                Restangular.all('corporations').post(_this.company).then(function(data){
                    _this.organization.company = data.id;
                    Restangular.one('organizations', user_id).patch(_this.organization).then(function(){
                        $location.path('user/finish');
                    },function(res){
                        _this.errorInfo = JSON.stringify(res.data);
                        _this.error = true;
                    })
                }, function(res){
                    _this.errorInfo = JSON.stringify(res.data);
                    _this.error = true;
                })
            }, function(res){
                _this.errorInfo = JSON.stringify(res.data);
                _this.error = true;
            })*/
        };

        $.validator.addMethod("complete_url", function(val, elem) {
            if (val.length == 0) {
                return true;
            }
            return /(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(val);
        }, "Please enter a valid url (without http://)");

        $('#company').validate({
            submitHandler: function () {
                console.log(_this.company);
                _this.organization.info_completed = 1;
                _this.organization.company = _this.organization.company.id;
                _this.error = false;
                validationPassed();
            },
            rules: {
                name: "required",
                scale: "required",
                url: "complete_url",
                contactName: "required",
                contactEmail: {
                    required: true,
                    email: true
                }
            }
        });

        var auth = 'Token ' + AuthService.getToken();
        var uploaderImg = _this.uploaderImg = new FileUploader({
            url: $rootScope.baseUrl+'/images/',
            headers: {
                Authorization: auth
            },
            autoUpload: true
        });
        uploaderImg.filters.push({
            name: 'typeFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });
        var imgSize = 1;
        uploaderImg.filters.push({
            name: 'sizeFilter',
            fn: function(item, options) {
                return (item.size/1042/1042 <= imgSize)
            }
        });

        _this.imgError = false;
        uploaderImg.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            _this.imgError = true;
            if (filter.name == 'typeFilter') {
                _this.imgErrorInfo = 'You submit should be image file'
            }
            if (filter.name == 'sizeFilter') {
                _this.imgErrorInfo = 'You submit should be less than '+ imgSize + 'MB'
            }
        };
        uploaderImg.onAfterAddingFile = function(fileItem) {
            // console.log(fileItem)
            _this.imgError = false;
        };
        uploaderImg.onErrorItem = function(item, response, status, headers) {
            _this.imgError = true;
            _this.imgErrorInfo = 'Server Error'
        }
        uploaderImg.onSuccessItem = function(item, response, status, headers) {
            _this.imgError = false;
            uploaderImg.clearQueue();
            uploaderImg.queue[0] = item;
            _this.organization.image = response.id;
            _this.organization.image_url = response.file_url;
            // console.log(response)
        }
        _this.removeImg = function() {
            uploaderImg.queue[0].remove();
            _this.organization.image = '';
            _this.organization.image_url = '';
            _this.imgError = false;
        }


        /*_this.company.address = {};

        _this.changeFormat = function() {
            _this.company.address.formatted_address = (_this.company.address.line1 ?  (_this.company.address.line1+', '):'')
                // + (_this.company.address.apt  ? ('#'+_this.company.address.apt+', '):'')
                + (_this.company.address.city ? (_this.company.address.city+', '):'')
                + (_this.company.address.state ? (_this.company.address.state)+' ':'')
                + (_this.company.address.zip_code ? (_this.company.address.zip_code+', '):'')
                + (_this.company.address.country ? (_this.company.address.country):'');
            _this.searchText = '';
        }*/

    });
