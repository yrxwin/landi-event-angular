'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:EventEditCtrl
 * @description
 * # EventEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('EventEditCtrl', function ($scope, $location, Restangular, $routeParams, AuthService, FileUploader, $anchorScroll, $rootScope, $sce) {

        var _this = this;

        var id = $routeParams.id;
        Restangular.one('events',id).get().then(function(data){
            console.log(data)
            _this.event = data;
            if (data.image && data.image.file_url) {
                _this.event.image_url = data.image.file_url;
                _this.event.image = data.image.id;
            }
            _this.event.description_show = $sce.trustAsHtml(_this.event.description);
        })

        _this.updateEvent = function(data, field){
            var update = {};
            update[field] = data;
            Restangular.one('events', id).patch(update).then(function(){

            },function(){
                return $rootScope.defaultErrorInfo
            })
        }
        _this.searchErrorComp = false;
        _this.keywordsComp = '';
        _this.doSearchComp = function() {
            var queryParams = {};
            queryParams.search = _this.keywordsComp;
            queryParams.page = _this.currentPageComp;
            Restangular.one('events',id).customGET('company_applications', queryParams).then(function (data) {
                console.log(data);
                _this.searchErrorComp = false;
                _this.resultsComp = data.results;
                _this.countComp = data.count;
                $anchorScroll();
            }, function () {
                _this.searchErrorComp = true;
            })
        }
        _this.doNewSearchComp = function(){
            _this.currentPageComp = 1;
            _this.countComp = 0;
            _this.doSearchComp();
        }
        _this.doNewSearchComp();

        _this.searchErrorStu = false;
        _this.keywordsStu = '';
        _this.doSearchStu = function() {
            var queryParams = {};
            queryParams.search = _this.keywordsStu;
            queryParams.page = _this.currentPageStu;
            Restangular.one('events',id).customGET('student_applications', queryParams).then(function (data) {
                console.log(data);
                _this.searchErrorStu = false;
                _this.resultsStu = data.results;
                _this.countStu = data.count;
                $anchorScroll();
            }, function (res) {
                if (res.status == 403){
                    _this.searchErrorStuInfo = 'Only joined companies can view this information'
                }
                _this.searchErrorStu = true;
            })
        }
        _this.doNewSearchStu = function(){
            _this.currentPageStu = 1;
            _this.countStu = 0;
            _this.doSearchStu();
        }
        _this.doNewSearchStu();

        _this.approveErrorComp = false;
        _this.approveContentComp = 'Approve';
        _this.approveComp = function(index){
            Restangular.one('event_company_applications',_this.resultsComp[index].id).patch({status: 2}).then(function(data){
                _this.resultsComp[index].status = 2;
                _this.approveErrorComp = false;
            }, function(){
                _this.approveErrorComp = true;
                _this.approveContentComp = 'Error';
            })
        }
        _this.rejectErrorComp = false;
        _this.rejectContentComp = 'Reject';
        _this.rejectComp = function(index){
            Restangular.one('event_company_applications',_this.resultsComp[index].id).patch({status: 3}).then(function(data){
                _this.resultsComp[index].status = 3;
                _this.rejectErrorComp = false;
            }, function(){
                _this.rejectErrorComp = true;
                _this.approveContentComp = 'Error';
            })
        }
        _this.approveErrorStu = false;
        _this.approveContentStu = 'Approve';
        _this.approveStu = function(index){
            Restangular.one('event_student_applications',_this.resultsComp[index].id).patch({status: 2}).then(function(data){
                _this.resultsStu[index].status = 2;
                _this.approveErrorStu = false;
            }, function(){
                _this.approveErorStu = true;
                _this.approveContentStu = 'Error';
            })
        }
        _this.rejectErrorStu = false;
        _this.rejectContentStu = 'Reject';
        _this.rejectStu = function(index){
            Restangular.one('event_student_applications',_this.resultsComp[index].id).patch({status: 3}).then(function(data){
                _this.resultsStu[index].status = 3;
                _this.rejectErrorStu = false;
            }, function(){
                _this.rejectErrorStu = true;
                _this.rejectContentStu = 'Error';
            })
        }

        _this.dateEdit = false;
        _this.dateError = false;

        _this.editDate = function(){
            _this.start_date_temp = _this.event.start_date;
            _this.end_date_temp = _this.event.end_date;
            _this.dateEdit = !_this.dateEdit;
        }

        var dateValidator = $('#date').validate({
            submitHandler: function(form){
                Restangular.one('events',_this.event.id).patch({start_date:_this.start_date_temp, end_date:_this.end_date_temp}).then(function(data){
                    _this.event.start_date = _this.start_date_temp;
                    _this.event.end_date = _this.end_date_temp;
                    _this.dateError = false;
                    _this.dateEdit = false;
                },function(){
                    _this.dateError = true;
                })
            },
            rules: {
                start: 'required',
                end: 'required'
            }
        })

        $('#start').datetimepicker();
        $('#end').datetimepicker();
        $("#start").on("dp.change",function (e) {
            var scope = angular.element($('#start')).scope();
            scope.$apply(function(){
                _this.start_date_temp = e.date
            });
            $('#end').data("DateTimePicker").setMinDate(e.date);
        });
        $("#end").on("dp.change",function (e) {
            var scope = angular.element($('#end')).scope();
            scope.$apply(function(){
                _this.end_date_temp = e.date
            });
            $('#start').data("DateTimePicker").setMaxDate(e.date);
        });

        _this.locationEdit = false;
        _this.locationError = false;
        _this.editLocation = function(){
            _this.location_temp = _this.event.location;
            _this.locationEdit = !_this.locationEdit
        }
        var locationValidator = $('#location').validate({
            submitHandler: function (form) {
                Restangular.one('locations',_this.event.location.id).patch(_this.location_temp).then(function(data){
                    console.log(data)
                    _this.event.location = _this.location_temp;
                    _this.locaitonError = false;
                    _this.locationEdit = false;
                },function(){
                    _this.locationError = true;
                })
            },
            rules: {
                city: 'required',
                state: 'required',
                country: 'required'
            }
        })
        _this.changeFormat = function() {
            _this.location_temp.formatted_address = (_this.location_temp.line1 ?  (_this.location_temp.line1+', '):'')
                + (_this.location_temp.city ? (_this.location_temp.city+', '):'')
                + (_this.location_temp.state ? (_this.location_temp.state)+' ':'')
                + (_this.location_temp.zip_code ? (_this.location_temp.zip_code+', '):'')
                + (_this.location_temp.country ? (_this.location_temp.country):'');
        }

        _this.absEdit = false;
        _this.absError = false;
        _this.editAbs = function(name){
            _this.abstract_temp = _this.event.abstract;
            _this.absEdit = !_this.absEdit;
        }
        var abstractValidator = $('#abstract').validate({
            submitHandler: function (form) {
                Restangular.one('events',_this.event.id).patch({abstract:_this.abstract_temp}).then(function(data){
                    _this.event.abstract = _this.abstract_temp;
                    _this.absError = false;
                    _this.absEdit = false;
                },function(){
                    _this.absError = true;
                })
            },
            rules: {
                abstract: {
                    rangelength: [0, 150]
                }
            }
        })

        _this.desEdit = false;
        _this.desError = false;
        _this.editDes = function(name){
            _this.description_temp = _this.event.description;
            _this.desEdit = !_this.desEdit;
        }
        var descriptionValidator = $('#description').validate({
            submitHandler: function (form) {
                Restangular.one('events',_this.event.id).patch({description:_this.description_temp}).then(function(data){
                    _this.event.description = _this.description_temp;
                    _this.event.description_show = $sce.trustAsHtml(_this.event.description);
                    _this.desError = false;
                    _this.desEdit = false;
                },function(){
                    _this.desError = true;
                })
            },
            rules: {
                description: {
                    rangelength: [0, 2000]
                }
            }
        })

        var auth = 'Token ' + AuthService.getToken();
        var uploaderImg = _this.uploaderImg = new FileUploader({
            url: $rootScope.baseUrl+ '/images/',
            headers: {
                Authorization: auth
            },
            autoUpload: true
        });
        uploaderImg.filters.push({
            name: 'typeFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });
        var imgSize = 1;
        uploaderImg.filters.push({
            name: 'sizeFilter',
            fn: function(item, options) {
                return (item.size/1042/1042 <= imgSize)
            }
        });

        _this.imgError = false;
        uploaderImg.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            _this.imgError = true;
            if (filter.name == 'typeFilter') {
                _this.imgErrorInfo = 'You submit should be image file'
            }
            if (filter.name == 'sizeFilter') {
                _this.imgErrorInfo = 'You submit should be less than '+ imgSize + 'MB'
            }
        };
        uploaderImg.onAfterAddingFile = function(fileItem) {
            // console.log(fileItem)
            _this.imgError = false;
        };
        uploaderImg.onErrorItem = function(item, response, status, headers) {
            _this.imgError = true;
            _this.imgErrorInfo = 'Server Error'
        }
        uploaderImg.onSuccessItem = function(item, response, status, headers) {
            Restangular.one('events',_this.event.id).patch({image:response.id}).then(function(data){
                if (_this.event.image){
                    Restangular.one('images',_this.event.image).remove().then(function(){
                        uploaderImg.clearQueue();
                        uploaderImg.queue[0] = item;
                        _this.imgError = false;
                        _this.event.image = response.id;
                        _this.event.image_url = response.file_url;
                    },function(){
                        _this.imgError = true;
                    })
                } else {
                    uploaderImg.clearQueue();
                    uploaderImg.queue[0] = item;
                    _this.imgError = false;
                    _this.event.image = response.id;
                    _this.event.image_url = response.file_url;
                }
            }, function(){
                _this.imgError = true;
            })

            // console.log(response)
        }
        _this.removeImg = function() {
            Restangular.one('images',_this.event.image).remove().then(function(){
                if (uploaderImg.queue[0]){
                    uploaderImg.queue[0].remove();
                }
                _this.event.image = '';
                _this.event.image_url = '';
                _this.imgError = false;
            }, function(){
                _this.imgError = true;
            })
        }

       /* _this.shouldEmail = false;
        _this.newAnnoBody = "";
        _this.newAnnoTitle = "";
        _this.event = {
            "title":"Samsung Info Session",
            "description":"This is the event description, This is the event description, ",
            "start":"2014-12-16T14:25:00.000Z",
            "end":"2014-12-18T14:25:00.000Z",
            "location":"30 Main St, Melrse, MA, 02176",
            "organizer":{"title":"MIT Ventureship club",
                "url":"organizer/show"},
            "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
            "partComps": [{"name":"Samsung Electronics", "url":"company/show","approved":true },
                {"name":"Cisco Telecommunication Company", "url":"company/show","approved":true },
                {"name":"This is another company", "url":"company/show","approved":false }],
            "partStus": [{"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                            "url":"student/show","approved":true},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                            "url":"student/show","approved":false},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                            "url":"student/show","approved":false}],
            "annos":[{"title":"Announcement1", "body":"This is announcement1"},
                {"title":"Announcement2", "body":"This is announcement2"},
                {"title":"Announcement3", "body":"This is announcement3"}]
        };*/

       /* _this.saveEventDetails = function(){
            console.log("event details should be saved here.")
        };

        _this.removeAnno = function(anno){
            _this.event.annos.splice(_this.event.annos.indexOf(anno),1);
        };

        _this.addAnno = function(){
            var newAnno = {};
            newAnno.title = _this.newAnnoTitle;
            newAnno.body = _this.newAnnoBody;
            _this.event.annos.push(newAnno);
            if(_this.shouldEmail){
                console.log("Sending an email using \"newAnno\"")
            }
            _this.newAnnoTitle = "";
            _this.newAnnoBody = "";
        };

        _this.approveComp = function(company){
            company.approved = true;
            // Also send email to notify that company for approval of participating an event
        };
        _this.disapproveComp = function(company){
            company.approved = false;
            // Also send email
        };
        _this.approveStudent = function(student){
            student.approved = true;
            // Also send email
        };
        _this.disapproveStudent = function(student){
            student.approved = false;
            // Also send email
        };
        _this.approveAllStudents = function(){
            _this.event.partStus.forEach(function(student) {
                if(!student.approved){
                    student.approved = true;
                }
            });
        };

        _this.searchStudent = function(){
            console.log("Searching students");
            $location.path('/student/search');
        };
        _this.searchCompany = function(){
            console.log("Searching companies");
            $location.path('/company/search');
        }*/
    });
