'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:JobShowCtrl
 * @description
 * # JobShowCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('JobShowCtrl', function ($scope, $location, Restangular, AuthService, $routeParams, $sce, $rootScope) {
        var _this = this;
        var id = $routeParams.id;
        var user_id = AuthService.getId();
        _this.userId = user_id;
        var applyUrl = 'jobs/'+id+'/applications';
        _this.applyError = false;
        _this.applyAlready = false;
        _this.applyContent = 'Apply';
        var pickStatus = function(status){
            switch(status) {
                case 0:
                    _this.applyContent = 'Applied';
                    _this.applyAlready = true;
                    break;
                case 1:
                    _this.applyContent = 'Checked by Employer';
                    _this.applyAlready = true;
                    break;
                case 2:
                    _this.applyContent = 'Accepted / Second Round';
                    _this.applyAlready = true;
                case 3:
                    _this.applyContent = 'Rejected';
                    _this.applyAlready = true;
                    break;
                default:
                    _this.applyContent = $rootScope.defaultErrorInfo;
            }
        }
        Restangular.one('jobs',id).get().then(function(data){
            console.log(data)
            _this.job = data;
            _this.job.type = _this.job.type.match(/\d/g);
            if (_this.job.description){
                _this.job.description_show = $sce.trustAsHtml(_this.job.description);
            }
            var searchObj = {};
            searchObj['student__id'] = _this.userId;

            Restangular.oneUrl(applyUrl).get(searchObj).then(function (data) {
                console.log(data)
                var ifPick = false;
                data.results.forEach(function(rel){

                    if (rel.student.id == _this.userId){
                        ifPick = true;
                        return;
                    }
                })
                // if (data.count > 0) {
                if (ifPick) {
                    pickStatus(data.results[0].status);
                }
            })
        })

        _this.applyJob = function(){
            Restangular.oneUrl(applyUrl).post().then(function(data){
                pickStatus(data.status);
            },function(res){
                if (res.status==400){
                    _this.applyContent = 'Already applied';
                    _this.applyAlready = true;
                    _this.applyError = false;
                    return;
                }
                _this.applyError = true;
                _this.applyAlready = false;
                _this.applyContent = $rootScope.defaultErrorInfo;
            })
        }
        _this.showLogin = function(){
            $rootScope.$broadcast('showLogin');
        }
        /*_this.job = {
            "title":"Professional Magical Engineer",
            "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "contactName":"someone",
                "contactEmail":"someone@samsung.com"},
            "type":"Full-time",
            "dueDate":"2014-12-16T14:25:00.000Z",
            "description":"This is a job description \n, This is a job description, This is a job description,",
            "responsibility":"This is a job responsibility \n, This is a job responsibility, This is a job responsibility,",
            "locations":["location1, LC","location2., LC","location3., LC"],
            "expired":true,
            "saved":false,
            "status":'',
            contactName:null,
            contactEmail:null
        };*/

        _this.getTypeStr = function(){
            if (_this.job.type){
                return " (" + _this.job.type + ")";
            }else{
                return null;
            }
        };

        _this.getContactName = function(){
            if(_this.job.contactName){
                return _this.job.contactName;
            }else{
                return _this.job.company.contactName;
            }
        };
        _this.getContactEmail = function(){
            if(_this.job.contactEmail){
                return _this.job.contactEmail;
            }else{
                return _this.job.company.contactEmail;
            }
        };
        /*_this.getAppliedStr = function () {
            switch (_this.job.status) {
                case 0:
                    return "Applied"
                case 1:
                    return "Reviewed"
                case 2:
                    return "Approved"
                case 3:
                    return "Rejected"
                default:
                    return "Apply"
            }
        };*/
        _this.getSavedStr = function(){
            if (_this.job.saved){
                return "Unsave";
            }else{
                return "Save";
            }
        };

        _this.applyClick = function() {
            if(!_this.job.applied){
                $('apply-btn').addClass('loading');
                var user_id = AuthService.getId();
                if (user_id) {
                    Restangular.oneUrl('job_applications/'+ user_id).customPOST({'status': 'Reviewed'}).then(function() {
                        $('apply-btn').removeClass('loading');
                        _this.job.applied = 'Reviewed';
                    }, function() {

                    })
                }
                // console.log("Apply button clicked");
                // $location.path('/DoApplicationAction');
            }
        };

        _this.saveClick  = function() {
            _this.job.saved = !_this.job.saved;
            if(_this.job.saved){
                console.log("Save job clicked")
            }else{
                console.log("Unsave job clicked")
            }
        }
    });
