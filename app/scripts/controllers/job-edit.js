'use strict';

/**
 * @ngdoc function
 * @name landiWebApp.controller:JobEditCtrl
 * @description
 * # JobEditCtrl
 * Controller of the landiWebApp
 */
angular.module('landiWebApp')
    .controller('JobEditCtrl', function ($scope, $routeParams, Restangular, $sce, $rootScope, pdfToText, AuthService, FileUploader, $route,  $anchorScroll) {

        var _this = this;
        var id = $routeParams.id;
        var user_id = AuthService.getId();

        Restangular.one('jobs',id).get().then(function(data){
            console.log(data)
            _this.job = data;
            _this.job.type = _this.job.type.match(/\d/g);
            if (data.document && data.document.file_url){
                // var documentUrl = 'https://s3.amazonaws.com/landi' + data.document.file_url;
                // _this.documentUrl = $sce.trustAsResourceUrl(documentUrl);
                _this.documentUrl = data.document.file_url;
                _this.documentName = data.document.file_name;
                _this.job.document = data.document.id;
            }
            if (_this.job.description){
                _this.job.description_show = $sce.trustAsHtml(_this.job.description);
            }
        })

        _this.searchError = false;
        _this.keywords = '';
        _this.doSearch = function() {
            var queryParams = {};
            queryParams.search = _this.keywords;
            queryParams.page = _this.currentPage;
            Restangular.one('jobs',id).customGET('applications', queryParams).then(function (data) {
                console.log(data);
                _this.searchError = false;
                _this.results = data.results;
                _this.count = data.count;
                $anchorScroll();
            }, function (res) {
                if (res.status == 403){
                    _this.searchErrorInfo = 'Only joined companies can view this information'
                }
                _this.searchError = true;
            })
        }
        _this.doNewSearch = function(){
            _this.currentPage = 1;
            _this.count = 0;
            _this.doSearch();
        }
        _this.doNewSearch();

        _this.approveError = false;
        _this.approveContent = 'Accept (Second Round)';
        _this.approveStu = function(index){
            Restangular.one('job_applications',_this.results[index].id).patch({status: 2}).then(function(data){
                _this.results[index].status = 2;
                _this.approveContent = 'Accepted (Second Round)';
                _this.approveError = false;
            }, function(){
                _this.approveError = true;
                _this.approveContent = 'Error'
            })
        }
        _this.rejectError = false;
        _this.rejectContent = 'Reject'
        _this.rejectStu = function(index){
            Restangular.one('job_applications',_this.results[index].id).patch({status: 3}).then(function(data){
                _this.results[index].status = 3;
                _this.rejectContent = 'Rejected';
                _this.rejectError = false;
            }, function(){
                _this.rejectError = true;
                _this.rejectContent = 'Error'
            })
        }

        _this.updateJob = function(data, field){
            var update = {};
            update[field] = data;
            Restangular.one('jobs', id).patch(update).then(function(){

            },function(){
                return $rootScope.defaultErrorInfo
            })
        }

        _this.titleFields = ['name'];
        _this.titleEdit = false;
        _this.titleError = false;
        _this.editTitle = function(name){
            _this.title_temp = _this.job.title;
            _this.titleEdit = !_this.titleEdit;
        }
        var companyValidator = $('#title').validate({
            submitHandler: function (form) {
                var title_id = _this.title_temp.id;
                Restangular.one('jobs',_this.job.id).patch({title:title_id}).then(function(data){
                    // console.log(data)
                    _this.job.title = _this.title_temp;
                    _this.titleError = false;
                    _this.titleEdit = false;
                },function(){
                    _this.titleError = true;
                })
            },
            rules: {
                auto: 'required'
            }
        })

        _this.absEdit = false;
        _this.absError = false;
        _this.editAbs = function(name){
            _this.abstract_temp = _this.job.abstract;
            _this.absEdit = !_this.absEdit;
        }
        var abstractValidator = $('#abstract').validate({
            submitHandler: function (form) {
                Restangular.one('jobs',_this.job.id).patch({abstract:_this.abstract_temp}).then(function(data){
                    _this.job.abstract = _this.abstract_temp;
                    _this.absError = false;
                    _this.absEdit = false;
                },function(){
                    _this.absError = true;
                })
            },
            rules: {
                abstract: {
                    rangelength: [0, 150]
                }
            }
        })

        _this.desEdit = false;
        _this.desError = false;
        _this.editDes = function(name){
            _this.description_temp = _this.job.description;
            _this.desEdit = !_this.desEdit;
        }
        var descriptionValidator = $('#description').validate({
            submitHandler: function (form) {
                Restangular.one('jobs',_this.job.id).patch({description:_this.description_temp}).then(function(data){
                    _this.job.description = _this.description_temp;
                    _this.job.description_show = $sce.trustAsHtml(_this.job.description);
                    _this.desError = false;
                    _this.desEdit = false;
                },function(){
                    _this.desError = true;
                })
            },
            rules: {
                description: {
                    rangelength: [0, 2000]
                }
            }
        })

        var auth = 'Token ' + AuthService.getToken();
        var uploaderPdf = _this.uploaderPdf = new FileUploader({
            url: $rootScope.baseUrl + '/documents/',
            headers: {
                Authorization: auth
            },
            autoUpload: true
        });

        // FILTERS

        uploaderPdf.filters.push({
            name: 'typeFilter',
            fn: function(item, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|pdf|PDF|'.indexOf(type) !== -1;
            }
        });
        var pdfSize = 1;
        uploaderPdf.filters.push({
            name: 'sizeFilter',
            fn: function(item, options) {
                return (item.size/1042/1042 <= pdfSize)
            }
        });

        _this.pdfError = false;
        uploaderPdf.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            _this.pdfError = true;
            if (filter.name == 'typeFilter') {
                _this.pdfErrorInfo = 'You submit should be pdf file'
            }
            if (filter.name == 'sizeFilter') {
                _this.pdfErrorInfo = 'You submit should be less than '+ pdfSize + 'MB'
            }
        };
        uploaderPdf.onAfterAddingFile = function(fileItem) {
            _this.pdfError = false;
        };
        uploaderPdf.onErrorItem = function(item, response, status, headers) {
            _this.pdfError = true;
            _this.pdfErrorInfo = 'Server Error'
        }
        uploaderPdf.onSuccessItem = function (item, response, status, headers) {
            var fileUrl = $rootScope.amazonUrl + response.file_url;
            Restangular.one('jobs', user_id).patch({document: response.id}).then(function () {
                uploaderPdf.clearQueue();
                uploaderPdf.queue[0] = item;
                _this.pdfError = false;
                _this.job.document = response.id;
                _this.documentUrl = response.file_url;
                _this.documentName = response.file_name;
                /*var documentUrl = $rootScope.amazonUrl + response.file_url;
                 _this.documentUrl = $sce.trustAsResourceUrl(documentUrl);*/
                $route.reload();
            }, function () {
                _this.pdfError = true;
            })
        }
        _this.removePdf = function() {
            Restangular.one('jobs',_this.job.id).patch({document:null}).then(function(){
                if (uploaderPdf.queue[0]){
                    uploaderPdf.queue[0].remove();
                }
                _this.job.document = '';
                _this.documentUrl = '';
                _this.documentName = '';
                _this.pdfError = false;
                $route.reload();
            },function(){
                _this.pdfError = true;
            })
        }

        $('.js-description-btn-edit').on('click', function () {
            $('.js-description-show').addClass('hidden');
            $('.js-description-edit').removeClass('hidden');
        });
        $('.js-description-btn-cancel').on('click', function () {
            $('.js-description-show').removeClass('hidden');
            $('.js-description-edit').addClass('hidden');
        });
        $('.js-responsibility-btn-edit').on('click', function () {
            $('.js-responsibility-show').addClass('hidden');
            $('.js-responsibility-edit').removeClass('hidden');
        });
        $('.js-responsibility-btn-cancel').on('click', function () {
            $('.js-responsibility-show').removeClass('hidden');
            $('.js-responsibility-edit').addClass('hidden');
        });
        var _this = this;
        var newLocation = "";
        /*_this.job = {
            "title":"Professional Magical Engineer",
            "company":{"name":"Samsung Electronics", "extUrl":"http://www.samsung.com",
                "url":"company/edit",
                "logo":"http://www.driveproductions.co.uk/wp-content/uploads/2013/01/EVENT.LOGO_..jpg",
                "industry":"Manufacturing",
                "contactName":"someone",
                "contactEmail":"someone@samsung.com"},
            "type":0,
            "applicants": [{"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                "url":"student/show","status":1, "primMajor": "Computer Science"},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                    "url":"student/show","status":2, "primMajor": "Magic Major"},
                {"name":"Zeqing Zhang", "primEdu": "Masssachusetts Institute of Technology",
                    "url":"student/show","status":4, "primMajor": "Magic Major"}],
            "dueDate":"2014-12-16T14:25:00.000Z",
            "description":"This is a job description \n, This is a job description, This is a job description,",
            "responsibility":"This is a job responsibility \n, This is a job responsibility, This is a job responsibility,",
            "locations":["location1, LC","location2., LC","location3., LC"],
            "expired":false,
            "saved":false,
            "applied":false,
            contactName:null,
            contactEmail:null
        };*/

        _this.getJobTypeStr = function(){
            switch(_this.job.type){
                case 0:
                    return "(Full-time)";
                case 1:
                    return "(Part-time)";
                case 2:
                    return "(Intern)";
                case 3:
                    return "(Intern [part-time])";
                case 4:
                    return "(Coop)";
                default:
                    return null;
            }
        };
        _this.getApplicantMajor = function(applicant){
            if(applicant.primMajor){
                return "(" + applicant.primMajor +")"
            }else{
                return null;
            }
        };
        _this.offerApplicant = function(applicant){
          applicant.status = 4;
            // Do correponding email actions
        };
        _this.rejectApplicant = function(applicant){
            applicant.status = 5;
            // Do correponding email actions
        };
        _this.canOffer = function(applicant){
            // applicant's status 1, applied, 2 reviewed, 3, interview scheduled,  4 offered 5 rejected
            return applicant.status < 4
        };
        _this.removeLocation = function(location){
            _this.job.locations.splice(_this.job.locations.indexOf(location),1);
        };
        _this.addNewLocation = function(){
            if(_this.newLocation.length > 1){
                _this.job.locations.push(_this.newLocation);
                _this.newLocation = "";
            }
        };
        _this.getContactNameStr= function(){
            if(_this.job.contactName){
                return _this.job.contactName;
            }else{
                return _this.job.company.contactName;
            }
        };
        _this.getContactEmailStr= function(){
            if(_this.job.contactEmail){
                return _this.job.contactEmail;
            }else{
                return _this.job.company.contactEmail;
            }
        };
        _this.removeJob = function(){
            console.log("Should remove this job")
        };
        _this.endJob = function(){
            _this.job.expired = true;
        };
        _this.reactivateJob = function(){
            _this.job.expired = false;
        }
    });
