'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:autoAddress
 * @description
 * # autoAddress
 */
angular.module('landiWebApp')
    .directive('autoAddress', function ($http, $document, Restangular) {
        return {
            restrict: 'AE',
            scope: {
                selectedTags: '=model',
                selectedOneTag: '=',
                fields: '=',
                showAddress: '=',
                searchText: '=',
                length: '@',
                placeholder: '@'
            },
            templateUrl: 'scripts/directives/autoaddress-template.html',
            link: function (scope, elem, attrs) {

                scope.selectedIndex = -1;

                scope.success = false;
                scope.error = false;
                scope.errorInfo = "";

                scope.removeTag = function (index) {
                    scope.selectedTags.splice(index, 1);
                }

                scope.search = function () {
                    scope.selectedOneTag = {};
                    if (scope.searchText) {
                        $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                            params: {
                                address: scope.searchText,
                                sensor: false
                            },
                            withCredentials: false
                        }).then(function(res){
                            console.log(scope.searchText)
                            scope.suggestions = res.data.results.slice(0,scope.length);
                            console.log(scope.suggestions)
                        });
                    } else {
                        scope.suggestions = [];
                    }
                }

                scope.addToSelectedTags = function (index) {
                    // console.log(scope.suggestions[index]);
                    scope.selectedOneTag = {};
                    scope.searchText = scope.suggestions[index].formatted_address;
                    /*console.log(_.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'country'
                    }).long_name);*/
                    var country = _.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'country'
                    });
                    var state = _.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'administrative_area_level_1'
                    });
                    var city = _.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'locality'
                    });
                    var apt = _.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'subpremise'
                    });
                    var route = _.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'route'
                    });
                    var street_number = _.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'street_number'
                    });
                    var zipCode = _.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'postal_code'
                    });
                    var zipCode_suffix = scope.selectedOneTag.zipCode_suffix = _.find(scope.suggestions[index].address_components, function(item){
                        return item.types[0] == 'postal_code_suffix'
                    });
                    if (country) {
                        scope.selectedOneTag.country = country.long_name;
                    }
                    if (state) {
                        scope.selectedOneTag.state = state.long_name;
                    }
                    if (city) {
                        scope.selectedOneTag.city = city.long_name;
                    }
                    if (apt) {
                        scope.selectedOneTag.apt = apt.long_name;
                    }
                    if (route) {
                        scope.selectedOneTag.route = route.long_name;
                    }
                    if (street_number) {
                        scope.selectedOneTag.street_number = street_number.long_name;
                    }
                    if (zipCode) {
                        scope.selectedOneTag.zip_code = zipCode.long_name;
                    }
                    if (zipCode_suffix) {
                        scope.selectedOneTag.zip_code_suffix = zipCode_suffix.long_name;
                    }

                    scope.selectedOneTag.formatted_address = scope.suggestions[index].formatted_address;
                    scope.selectedOneTag.lng = scope.suggestions[index].geometry.location.lng;
                    scope.selectedOneTag.lat = scope.suggestions[index].geometry.location.lat;
                    scope.selectedOneTag.line1 = (typeof scope.selectedOneTag.street_number  !== "undefined" ?  (scope.selectedOneTag.street_number+' '):'')
                        + (typeof scope.selectedOneTag.route !== "undefined" ? (scope.selectedOneTag.route+' '):'');
                    if (/\s+$/.test(scope.selectedOneTag.line1)){
                        scope.selectedOneTag.line1 = scope.selectedOneTag.line1.substr(0, scope.selectedOneTag.line1.length-1);
                    }
                    if (scope.selectedTags) {
                        scope.selectedTags.push(scope.suggestions[index]);
                    }
                    // scope.searchText='';
                    scope.suggestions = [];
                }

               /* $('.dropdown').on('shown.bs.dropdown', function() {
                    scope.selectedOneTag = {};
                })*/

                $('#autoAddress').on('hidden.bs.dropdown', function () {
                    // console.log(scope.selectedOneTag.lng)
                    if (!scope.selectedOneTag.lng) {
                        console.log('test')
                        scope.$apply(scope.searchText = '');
                    } else {

                    }
                })
            }
        }
    });
