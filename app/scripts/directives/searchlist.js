'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:searchList
 * @description
 * # searchList
 */
angular.module('landiWebApp')
  .directive('searchList', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the searchList directive');
      }
    };
  });
