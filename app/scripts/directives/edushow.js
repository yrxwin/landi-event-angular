'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:eduShow
 * @description
 * # eduShow
 */
angular.module('landiWebApp')
    .directive('eduShow', function () {
        return {
            scope:{
                educations: '=',
                acadLevels: '='
            },
            templateUrl: 'scripts/directives/edushow-template.html',
            restrict: 'AE',
            link: function postLink(scope, element, attrs) {
                scope.showEdu = false;
                scope.showDegree = function (degree) {
                    var selected = $filter('filter')(scope.acadLevels, {code: degree});
                    return (degree && selected.length) ? selected[0].name : 'Not set';
                };
                scope.removeEdu = function(edu){

                }
            }
        };
    });
