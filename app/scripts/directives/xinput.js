'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:xInput
 * @description
 * # xInput
 */
angular.module('landiWebApp')
  .directive('xInput', function () {
        return {
            templateUrl: 'scripts/directives/x-input.html',
            restrict: 'AE',
            scope: {
                eName: '@',
                ePlaceholder: '@'
            },
            link: function (scope, element, attr) {

                // element.text('this is the dropdown directive');
                element
                    .form({
                        emailR: {
                            identifier: 'emailR',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'This field is required'
                                },
                                {
                                    type: 'email',
                                    prompt: 'Please enter valid email'
                                }
                            ]
                        },
                        email: {
                            identifier: 'email',
                            rules: [
                                {
                                    type: 'email',
                                    prompt: 'Please enter valid email'
                                }
                            ]
                        }
                    },
                    {
                        inline: true,
                        on: 'blur',
                        onSuccess: validationPassed
                    });

            }
        };
  });
