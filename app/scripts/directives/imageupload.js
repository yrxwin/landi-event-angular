'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:imageUpload
 * @description
 * # imageUpload
 */
angular.module('landiWebApp')
    .directive('imageUpload', ['AuthService','Restangular', 'FileUploader', function (AuthService, Restangular, FileUploader) {
        return {
            restrict: 'AE',
            scope: {
                isCreate: '@',
                postObj: '@',
                user_id: '@',
                image: '=',
                image_url: '='
            },
            templateUrl: 'views/imageupload-template.html',
            link: function (scope, elem, attrs) {
                var auth = 'Token ' + AuthService.getToken();
                var uploaderImg = scope.uploaderImg = new FileUploader({
                    url: attrs.url,
                    headers: {
                        Authorization: auth
                    },
                    autoUpload: true
                });
                uploaderImg.filters.push({
                    name: 'typeFilter',
                    fn: function (item /*{File|FileLikeObject}*/, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                    }
                });
                var imgSize = 1;
                uploaderImg.filters.push({
                    name: 'sizeFilter',
                    fn: function (item, options) {
                        return (item.size / 1042 / 1042 <= imgSize)
                    }
                });

                scope.imgError = false;
                uploaderImg.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                    scope.imgError = true;
                    if (filter.name == 'typeFilter') {
                        scope.imgErrorInfo = 'You submit should be image file'
                    }
                    if (filter.name == 'sizeFilter') {
                        scope.imgErrorInfo = 'You submit should be less than ' + imgSize + 'MB'
                    }
                };
                uploaderImg.onAfterAddingFile = function (fileItem) {
                    // console.log(fileItem)
                    scope.imgError = false;
                };
                uploaderImg.onErrorItem = function (item, response, status, headers) {
                    scope.imgError = true;
                    scope.imgErrorInfo = 'Server Error'
                }
                uploaderImg.onSuccessItem = function (item, response, status, headers) {
                    if (scope.isCreate) {
                        scope.imgError = false;
                        uploaderImg.clearQueue();
                        uploaderImg.queue[0] = item;
                        scope.image = response.id;
                        scope.image_url = response.file_url;
                    } else {
                        Restangular.one(scope.postObj, user_id).patch({image: response.id}).then(function (data) {
                            uploaderImg.clearQueue();
                            uploaderImg.queue[0] = item;
                            scope.imgError = false;
                            scope.image = response.id;
                            scope.image_url = response.file_url;
                        }, function () {
                            scope.imgError = true;
                        })
                    }
                }
            }
        }
    }]);
