'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:loginPop
 * @description
 * # loginPop
 */
angular.module('landiWebApp')
  .directive('loginPop', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the loginPop directive');
      }
    };
  });
