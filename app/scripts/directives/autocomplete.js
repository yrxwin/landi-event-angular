'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:autoComplete
 * @description
 * # autoComplete
 */
angular.module('landiWebApp')
  .directive('autoComplete', function ($http, $document, Restangular) {
        return {
            restrict:'AE',
            scope:{
                selectedTags:'=model',
                selectedOneTag: '=',
                fields:'=',
                searchObj: '@',
                length: '@',
                urlPost: '@',
                placeholder: '@'
            },
            templateUrl:'views/autocomplete-template.html',
            link:function(scope,elem,attrs){

                scope.suggestions=[];

                scope.selectedIndex=-1;

                scope.success = false;
                scope.error = false;
                scope.errorInfo = "";
                console.log(scope.selectedOneTag)

                scope.removeTag=function(index){
                    scope.selectedTags.splice(index,1);
                }

                scope.search=function(){
                    scope.selectedOneTag = {};
                    if (scope.searchText) {
                        var queryParam={};
                        queryParam[scope.searchObj] = scope.searchText;
                        Restangular.oneUrl(attrs.url).get(queryParam).then(function(data){
                            console.log(data)
                            console.log(scope.searchText)
                            var results = _.map(data.results, function(university) {
                                    var temp = {};
                                    scope.fields.forEach(function(field){
                                        temp[field] = university[field];
                                    })
                                    return temp
                                }
                            );
                            // console.log(results)
                            scope.suggestions = results.slice(0, scope.length);
                            scope.suggestionsFull = data.results.slice(0, scope.length);
                            // scope.selectedIndex=-1;
                        })
                    } else {
                        scope.suggestions=[];
                    }

                    /*$http.get(attrs.url+'?term='+scope.searchText).success(function(data){
                        *//*if(data.indexOf(scope.searchText)===-1){
                            data.unshift(scope.searchText);
                        }*//*
                        scope.suggestions=data;
                        scope.selectedIndex=-1;
                    });*/
                }

                scope.addToSelectedTags = function (index) {
                    scope.searchText = scope.suggestions[index][scope.fields[0]];
                    scope.selectedOneTag = scope.suggestionsFull[index];
                    if (scope.selectedTags) {
                        scope.selectedTags.push(scope.suggestionsFull[index]);
                    }
                    // scope.searchText='';
                    scope.suggestions = [];
                    scope.suggestionsFull = [];
                    console.log(scope.selectedOneTag)
                }

                scope.createNew = function(){
                    var data={};
                    scope.searchText = toTitleCase(scope.searchText);
                    data[scope.fields[0]]=scope.searchText;
                    console.log(data)
                    Restangular.oneUrl(scope.urlPost).customPOST(data).then(function(data){
                        scope.success = true;
                        scope.error = false;
                        scope.selectedOneTag = data;
                        scope.searchText = data[scope.fields[0]];
                    },function(){
                        scope.error = true;
                        scope.success = false;
                    })
                }

               /* $('.dropdown').on('shown.bs.dropdown', function() {
                    scope.selectedOneTag = {};
                })*/

                $('.dropdown').on('hidden.bs.dropdown', function () {
                    // console.log(scope.selectedOneTag.name);
                    if (!scope.selectedOneTag || !scope.selectedOneTag[scope.fields[0]]) {
                        scope.$apply(scope.searchText = '');
                    }
                })

                scope.$watch('selectedOneTag', function(val){
                    if (!val){
                        scope.searchText = '';
                    }
                })

                function toTitleCase(str)
                {
                    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
                }
                /*scope.checkKeyDown=function(event){
                    if(event.keyCode===40){
                        event.preventDefault();
                        if(scope.selectedIndex+1 !== scope.suggestions.length){
                            scope.selectedIndex++;
                        }
                    }
                    else if(event.keyCode===38){
                        event.preventDefault();
                        if(scope.selectedIndex-1 !== -1){
                            scope.selectedIndex--;
                        }
                    }
                    else if(event.keyCode===13){
                        scope.addToSelectedTags(scope.selectedIndex);
                    }
                }

                scope.$watch('selectedIndex',function(val){
                    if(val!==-1) {
                        scope.searchText = scope.suggestions[scope.selectedIndex];
                    }
                });

                scope.isPopupVisible = false;

                scope.toggleSelect = function(){
                    scope.isPopupVisible = true;
                }

                $document.bind('click', function(event){
                    if (scope.selectedOneTag=={}) {
                        var isClicked = elem
                            .find(event.target)
                            .length > 0;
                        console.log(isClicked)
                        if (isClicked)
                            return;
                        scope.searchText = "";
                    }
                }); */
            }
        }
  });
