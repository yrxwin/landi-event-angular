'use strict';

/**
 * @ngdoc directive
 * @name landiWebApp.directive:multiChoice
 * @description
 * # multiChoice
 */
angular.module('landiWebApp')
  .directive('multiChoice', function () {
    return {
        templateUrl: 'scripts/directives/multi-choice.html',
        restrict: 'AE',
        scope: {
            item: '=',
            result: '='
        },
        link: function postLink(scope, element, attrs) {
            // element.text('this is the multiChoice directive');
            scope.$apply(function() {
                $('.ui.checkbox')
                    .checkbox();
            })
        }
    };
  });
