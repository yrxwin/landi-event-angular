'use strict';

/**
 * @ngdoc overview
 * @name landiWebApp
 * @description
 * # landiWebApp
 *
 * Main module of the application.
 */
var app = angular.module('landiWebApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'restangular',
    'LocalStorageModule',
    // 'ngAutocomplete',
    'flow',
    'ui.bootstrap',
    'xeditable',
    'internationalPhoneNumber',
    'uiGmapgoogle-maps',
    'ui.bootstrap.datetimepicker',
    // 'validation',
    'ui.utils',
    'angularMoment',
    'angularFileUpload',
    'pdf',
    'summernote'
]);


app.run(function ($location, Restangular, AuthService, editableOptions, $rootScope, paginationConfig, pagerConfig, $filter) {
    Restangular.addFullRequestInterceptor(function (element, operation, route, url, headers, params, httpConfig) {
        if (AuthService.getToken()){
            headers['Authorization'] = 'Token ' + AuthService.getToken();
            // console.log(headers)
            return {
                headers: headers
            };
        }
    });

    Restangular.setErrorInterceptor(function (response, deferred, responseHandler, $rootScope) {
        if (response.config.bypassErrorInterceptor) {
            return true;
        } else {
            switch (response.status) {
                case 401:
                    AuthService.logout();
                    // $rootScope.$broadcast('showLogin');
                    $location.path('/login');
                    break;
                default:
                    deferred.reject(response)
            }
            return false;
        }
    });

    editableOptions.theme = 'bs3';
    paginationConfig.itemsPerPage = 5;
    pagerConfig.itemsPerPage = 5;
    $rootScope.getHash = function () {
        return $location.hash();
    }
    $rootScope.companyScales = [
        {name: ' < 50 Employees', code: '1'},
        {name: '50 - 100 Employees', code: '2'},
        {name: '100 - 200 Employees', code: '3'},
        {name: '200 - 600 Employees', code: '4'},
        {name: '600 - 1000 Employees', code: '5'},
        {name: '1000+ Employees', code: '6'}
    ];
    $rootScope.showScale = function (scale) {
        var selected = $filter('filter')($rootScope.companyScales, {code: scale});
        return (scale && selected.length) ? selected[0].name : 'Company Scale';
    };
    $rootScope.degrees = [
        {name: 'Bachelor', code: '1'},
        {name: 'Master', code: '2'},
        {name: 'Doctorate', code: '3'},
        {name: 'Other Degree', code: '0'}
    ];
    $rootScope.showDegree = function (degree) {
        var selected = $filter('filter')($rootScope.degrees, {code: degree});
        return (degree && selected.length) ? selected[0].name : 'Degree';
    };
    $rootScope.jobTypes = [
        {name: 'Full-time', code: '1'},
        {name: 'Part-time', code: '2'},
        {name: 'Intern (Full-time)', code: '3'},
        {name: 'Intern (Part-time)', code: '4'},
        {name: 'Coop', code: '5'}
    ]

    $rootScope.showTypes = function(types) {
        var selected = [];
        angular.forEach($rootScope.jobTypes, function(s) {
            if (types.indexOf(s.code) >= 0) {
                selected.push(s.name);
            }
        });
        return selected.length ? selected.join(' / ') : 'Job Types';
    };

    $rootScope.showType = function(type) {
        var selected = $filter('filter')($rootScope.jobTypes, {code: type});
        return (type && selected.length) ? selected[0].name : 'Job Type';
    };


    $rootScope.defaultSuccessInfo = 'Save successfully';
    $rootScope.defaultErrorInfo = 'Something wrong. Please try again later';
    $rootScope.amazonUrl = 'https://s3.amazonaws.com/landi';

    // For Development
    // $rootScope.baseUrl = '/api';
    // For Production
    $rootScope.baseUrl = 'http://api.landi.careers';

    $rootScope.countries = [
        {name: 'United States', code: 'US'},
        {name: 'Afghanistan', code: 'AF'},
        {name: 'Åland Islands', code: 'AX'},
        {name: 'Albania', code: 'AL'},
        {name: 'Algeria', code: 'DZ'},
        {name: 'American Samoa', code: 'AS'},
        {name: 'AndorrA', code: 'AD'},
        {name: 'Angola', code: 'AO'},
        {name: 'Anguilla', code: 'AI'},
        {name: 'Antarctica', code: 'AQ'},
        {name: 'Antigua and Barbuda', code: 'AG'},
        {name: 'Argentina', code: 'AR'},
        {name: 'Armenia', code: 'AM'},
        {name: 'Aruba', code: 'AW'},
        {name: 'Australia', code: 'AU'},
        {name: 'Austria', code: 'AT'},
        {name: 'Azerbaijan', code: 'AZ'},
        {name: 'Bahamas', code: 'BS'},
        {name: 'Bahrain', code: 'BH'},
        {name: 'Bangladesh', code: 'BD'},
        {name: 'Barbados', code: 'BB'},
        {name: 'Belarus', code: 'BY'},
        {name: 'Belgium', code: 'BE'},
        {name: 'Belize', code: 'BZ'},
        {name: 'Benin', code: 'BJ'},
        {name: 'Bermuda', code: 'BM'},
        {name: 'Bhutan', code: 'BT'},
        {name: 'Bolivia', code: 'BO'},
        {name: 'Bosnia and Herzegovina', code: 'BA'},
        {name: 'Botswana', code: 'BW'},
        {name: 'Bouvet Island', code: 'BV'},
        {name: 'Brazil', code: 'BR'},
        {name: 'British Indian Ocean Territory', code: 'IO'},
        {name: 'Brunei Darussalam', code: 'BN'},
        {name: 'Bulgaria', code: 'BG'},
        {name: 'Burkina Faso', code: 'BF'},
        {name: 'Burundi', code: 'BI'},
        {name: 'Cambodia', code: 'KH'},
        {name: 'Cameroon', code: 'CM'},
        {name: 'Canada', code: 'CA'},
        {name: 'Cape Verde', code: 'CV'},
        {name: 'Cayman Islands', code: 'KY'},
        {name: 'Central African Republic', code: 'CF'},
        {name: 'Chad', code: 'TD'},
        {name: 'Chile', code: 'CL'},
        {name: 'China', code: 'CN'},
        {name: 'Christmas Island', code: 'CX'},
        {name: 'Cocos (Keeling) Islands', code: 'CC'},
        {name: 'Colombia', code: 'CO'},
        {name: 'Comoros', code: 'KM'},
        {name: 'Congo', code: 'CG'},
        {name: 'Congo, The Democratic Republic of the', code: 'CD'},
        {name: 'Cook Islands', code: 'CK'},
        {name: 'Costa Rica', code: 'CR'},
        {name: 'Cote D\'Ivoire', code: 'CI'},
        {name: 'Croatia', code: 'HR'},
        {name: 'Cuba', code: 'CU'},
        {name: 'Cyprus', code: 'CY'},
        {name: 'Czech Republic', code: 'CZ'},
        {name: 'Denmark', code: 'DK'},
        {name: 'Djibouti', code: 'DJ'},
        {name: 'Dominica', code: 'DM'},
        {name: 'Dominican Republic', code: 'DO'},
        {name: 'Ecuador', code: 'EC'},
        {name: 'Egypt', code: 'EG'},
        {name: 'El Salvador', code: 'SV'},
        {name: 'Equatorial Guinea', code: 'GQ'},
        {name: 'Eritrea', code: 'ER'},
        {name: 'Estonia', code: 'EE'},
        {name: 'Ethiopia', code: 'ET'},
        {name: 'Falkland Islands (Malvinas)', code: 'FK'},
        {name: 'Faroe Islands', code: 'FO'},
        {name: 'Fiji', code: 'FJ'},
        {name: 'Finland', code: 'FI'},
        {name: 'France', code: 'FR'},
        {name: 'French Guiana', code: 'GF'},
        {name: 'French Polynesia', code: 'PF'},
        {name: 'French Southern Territories', code: 'TF'},
        {name: 'Gabon', code: 'GA'},
        {name: 'Gambia', code: 'GM'},
        {name: 'Georgia', code: 'GE'},
        {name: 'Germany', code: 'DE'},
        {name: 'Ghana', code: 'GH'},
        {name: 'Gibraltar', code: 'GI'},
        {name: 'Greece', code: 'GR'},
        {name: 'Greenland', code: 'GL'},
        {name: 'Grenada', code: 'GD'},
        {name: 'Guadeloupe', code: 'GP'},
        {name: 'Guam', code: 'GU'},
        {name: 'Guatemala', code: 'GT'},
        {name: 'Guernsey', code: 'GG'},
        {name: 'Guinea', code: 'GN'},
        {name: 'Guinea-Bissau', code: 'GW'},
        {name: 'Guyana', code: 'GY'},
        {name: 'Haiti', code: 'HT'},
        {name: 'Heard Island and Mcdonald Islands', code: 'HM'},
        {name: 'Holy See (Vatican City State)', code: 'VA'},
        {name: 'Honduras', code: 'HN'},
        {name: 'Hong Kong', code: 'HK'},
        {name: 'Hungary', code: 'HU'},
        {name: 'Iceland', code: 'IS'},
        {name: 'India', code: 'IN'},
        {name: 'Indonesia', code: 'ID'},
        {name: 'Iran, Islamic Republic Of', code: 'IR'},
        {name: 'Iraq', code: 'IQ'},
        {name: 'Ireland', code: 'IE'},
        {name: 'Isle of Man', code: 'IM'},
        {name: 'Israel', code: 'IL'},
        {name: 'Italy', code: 'IT'},
        {name: 'Jamaica', code: 'JM'},
        {name: 'Japan', code: 'JP'},
        {name: 'Jersey', code: 'JE'},
        {name: 'Jordan', code: 'JO'},
        {name: 'Kazakhstan', code: 'KZ'},
        {name: 'Kenya', code: 'KE'},
        {name: 'Kiribati', code: 'KI'},
        {name: 'Korea, Democratic People\'S Republic of', code: 'KP'},
        {name: 'Korea, Republic of', code: 'KR'},
        {name: 'Kuwait', code: 'KW'},
        {name: 'Kyrgyzstan', code: 'KG'},
        {name: 'Lao People\'S Democratic Republic', code: 'LA'},
        {name: 'Latvia', code: 'LV'},
        {name: 'Lebanon', code: 'LB'},
        {name: 'Lesotho', code: 'LS'},
        {name: 'Liberia', code: 'LR'},
        {name: 'Libyan Arab Jamahiriya', code: 'LY'},
        {name: 'Liechtenstein', code: 'LI'},
        {name: 'Lithuania', code: 'LT'},
        {name: 'Luxembourg', code: 'LU'},
        {name: 'Macao', code: 'MO'},
        {name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK'},
        {name: 'Madagascar', code: 'MG'},
        {name: 'Malawi', code: 'MW'},
        {name: 'Malaysia', code: 'MY'},
        {name: 'Maldives', code: 'MV'},
        {name: 'Mali', code: 'ML'},
        {name: 'Malta', code: 'MT'},
        {name: 'Marshall Islands', code: 'MH'},
        {name: 'Martinique', code: 'MQ'},
        {name: 'Mauritania', code: 'MR'},
        {name: 'Mauritius', code: 'MU'},
        {name: 'Mayotte', code: 'YT'},
        {name: 'Mexico', code: 'MX'},
        {name: 'Micronesia, Federated States of', code: 'FM'},
        {name: 'Moldova, Republic of', code: 'MD'},
        {name: 'Monaco', code: 'MC'},
        {name: 'Mongolia', code: 'MN'},
        {name: 'Montserrat', code: 'MS'},
        {name: 'Morocco', code: 'MA'},
        {name: 'Mozambique', code: 'MZ'},
        {name: 'Myanmar', code: 'MM'},
        {name: 'Namibia', code: 'NA'},
        {name: 'Nauru', code: 'NR'},
        {name: 'Nepal', code: 'NP'},
        {name: 'Netherlands', code: 'NL'},
        {name: 'Netherlands Antilles', code: 'AN'},
        {name: 'New Caledonia', code: 'NC'},
        {name: 'New Zealand', code: 'NZ'},
        {name: 'Nicaragua', code: 'NI'},
        {name: 'Niger', code: 'NE'},
        {name: 'Nigeria', code: 'NG'},
        {name: 'Niue', code: 'NU'},
        {name: 'Norfolk Island', code: 'NF'},
        {name: 'Northern Mariana Islands', code: 'MP'},
        {name: 'Norway', code: 'NO'},
        {name: 'Oman', code: 'OM'},
        {name: 'Pakistan', code: 'PK'},
        {name: 'Palau', code: 'PW'},
        {name: 'Palestinian Territory, Occupied', code: 'PS'},
        {name: 'Panama', code: 'PA'},
        {name: 'Papua New Guinea', code: 'PG'},
        {name: 'Paraguay', code: 'PY'},
        {name: 'Peru', code: 'PE'},
        {name: 'Philippines', code: 'PH'},
        {name: 'Pitcairn', code: 'PN'},
        {name: 'Poland', code: 'PL'},
        {name: 'Portugal', code: 'PT'},
        {name: 'Puerto Rico', code: 'PR'},
        {name: 'Qatar', code: 'QA'},
        {name: 'Reunion', code: 'RE'},
        {name: 'Romania', code: 'RO'},
        {name: 'Russian Federation', code: 'RU'},
        {name: 'RWANDA', code: 'RW'},
        {name: 'Saint Helena', code: 'SH'},
        {name: 'Saint Kitts and Nevis', code: 'KN'},
        {name: 'Saint Lucia', code: 'LC'},
        {name: 'Saint Pierre and Miquelon', code: 'PM'},
        {name: 'Saint Vincent and the Grenadines', code: 'VC'},
        {name: 'Samoa', code: 'WS'},
        {name: 'San Marino', code: 'SM'},
        {name: 'Sao Tome and Principe', code: 'ST'},
        {name: 'Saudi Arabia', code: 'SA'},
        {name: 'Senegal', code: 'SN'},
        {name: 'Serbia and Montenegro', code: 'CS'},
        {name: 'Seychelles', code: 'SC'},
        {name: 'Sierra Leone', code: 'SL'},
        {name: 'Singapore', code: 'SG'},
        {name: 'Slovakia', code: 'SK'},
        {name: 'Slovenia', code: 'SI'},
        {name: 'Solomon Islands', code: 'SB'},
        {name: 'Somalia', code: 'SO'},
        {name: 'South Africa', code: 'ZA'},
        {name: 'South Georgia and the South Sandwich Islands', code: 'GS'},
        {name: 'Spain', code: 'ES'},
        {name: 'Sri Lanka', code: 'LK'},
        {name: 'Sudan', code: 'SD'},
        {name: 'Suriname', code: 'SR'},
        {name: 'Svalbard and Jan Mayen', code: 'SJ'},
        {name: 'Swaziland', code: 'SZ'},
        {name: 'Sweden', code: 'SE'},
        {name: 'Switzerland', code: 'CH'},
        {name: 'Syrian Arab Republic', code: 'SY'},
        {name: 'Taiwan, Province of China', code: 'TW'},
        {name: 'Tajikistan', code: 'TJ'},
        {name: 'Tanzania, United Republic of', code: 'TZ'},
        {name: 'Thailand', code: 'TH'},
        {name: 'Timor-Leste', code: 'TL'},
        {name: 'Togo', code: 'TG'},
        {name: 'Tokelau', code: 'TK'},
        {name: 'Tonga', code: 'TO'},
        {name: 'Trinidad and Tobago', code: 'TT'},
        {name: 'Tunisia', code: 'TN'},
        {name: 'Turkey', code: 'TR'},
        {name: 'Turkmenistan', code: 'TM'},
        {name: 'Turks and Caicos Islands', code: 'TC'},
        {name: 'Tuvalu', code: 'TV'},
        {name: 'Uganda', code: 'UG'},
        {name: 'Ukraine', code: 'UA'},
        {name: 'United Arab Emirates', code: 'AE'},
        {name: 'United Kingdom', code: 'GB'},
        {name: 'United States Minor Outlying Islands', code: 'UM'},
        {name: 'Uruguay', code: 'UY'},
        {name: 'Uzbekistan', code: 'UZ'},
        {name: 'Vanuatu', code: 'VU'},
        {name: 'Venezuela', code: 'VE'},
        {name: 'Viet Nam', code: 'VN'},
        {name: 'Virgin Islands, British', code: 'VG'},
        {name: 'Virgin Islands, U.S.', code: 'VI'},
        {name: 'Wallis and Futuna', code: 'WF'},
        {name: 'Western Sahara', code: 'EH'},
        {name: 'Yemen', code: 'YE'},
        {name: 'Zambia', code: 'ZM'},
        {name: 'Zimbabwe', code: 'ZW'}
    ];
});

app.config(function ($routeProvider, $locationProvider, RestangularProvider, $httpProvider) {

    $httpProvider.defaults.withCredentials = true;
    RestangularProvider.setDefaultHttpFields({'withCredentials': true});

    RestangularProvider.setRequestSuffix('/');

    // For Development
    // RestangularProvider.setBaseUrl('/api');

    // For Production
    RestangularProvider.setBaseUrl('http://api.landi.careers');

    var partialsDir = '../partials';

    /*var redirectIfAuthenticated = function(route) {
     return function($location, $q, AuthService) {

     var deferred = $q.defer();

     if (AuthService.isAuthenticated()) {
     deferred.reject();
     $location.path(route);
     } else {
     deferred.resolve()
     }

     return deferred.promise;
     }
     };*/

    var redirectIfNotAuthenticated = function (route, redirect) {
        return function ($location, $q, AuthService) {

            var deferred = $q.defer();

            if (!AuthService.isAuthenticated()) {
                deferred.reject();
                $location.path(route).search({redirect: redirect});
            } else {
                deferred.resolve()
            }

            return deferred.promise;

        }
    };


    /*uiGmapGoogleMapApiProvider.configure({
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });*/
    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'HomeCtrl',
            controllerAs: 'home'
        })
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'login'
        })
        .when('/about', {
            templateUrl: 'views/about-us.html',
            controller: 'AboutCtrl'
        })
        .when('/event/create', {
            templateUrl: 'views/event-create.html',
            controller: 'EventCreateCtrl',
            controllerAs: 'ec'
            /*resolve: {redirect: redirectIfNotAuthenticated('/login')}*/
        })
        .when('/company/manage', {
            templateUrl: 'views/company-manage.html',
            controller: 'CompanyManageCtrl',
            controllerAs: 'cm'
            /*resolve: {
                redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login')
            }*/
        })
        .when('/company/edit', {
            templateUrl: 'views/company-edit.html',
            controller: 'CompanyEditCtrl',
            controllerAs: 'ce'
            /*resolve: {
                redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login', '/company/edit')
            }*/
        })
        .when('/company/show/:id', {
            templateUrl: 'views/company-show.html',
            controller: 'CompanyProfileCtrl',
            controllerAs: 'cp'
        })
        .when('/company/search', {
            templateUrl: 'views/company-search.html',
            controller: 'CompanySearchCtrl',
            controllerAs: 'cs'
        })
        .when('/contactus', {
            templateUrl: 'views/contact-us.html',
            controller: 'ContactUsCtrl',
            controllerAs: 'cu'
        })
        .when('/event/show/:id', {
            templateUrl: 'views/event-show.html',
            controller: 'EventShowCtrl',
            controllerAs: 'ep'
        })
        .when('/event/show-comp', {
            templateUrl: 'views/event-company.html',
            controller: 'EventCompanyCtrl',
            controllerAs: 'epComp'
        })
        .when('/event/edit/:id', {
            templateUrl: 'views/event-edit.html',
            controller: 'EventEditCtrl',
            controllerAs: 'ee'
        })
        .when('/job/create', {
            templateUrl: 'views/job-create.html',
            controller: 'JobCreateCtrl',
            controllerAs: 'jc'
            /*resolve: {
                redirect: redirectIfNotAuthenticated('/login')
            }*/
        })
        .when('/job/show/:id', {
            templateUrl: 'views/job-show.html',
            controller: 'JobShowCtrl',
            controllerAs: 'jp'
        })
        .when('/job/edit/:id', {
            templateUrl: 'views/job-edit.html',
            controller: 'JobEditCtrl',
            controllerAs: 'je'
        })
        .when('/organizer/show', {
            templateUrl: 'views/organizer-show.html',
            controller: 'OrganizerShowCtrl',
            controllerAs: 'op'
        })
        .when('/organizer/edit', {
            templateUrl: 'views/organizer-edit.html',
            controller: 'OrganizerEditCtrl',
            controllerAs: 'oe'
        })
        .when('/organizer/manage', {
            templateUrl: 'views/organizer-manage.html',
            controller: 'OrganizerManageCtrl',
            controllerAs: 'om'
        })
        .when('/search', {
            templateUrl: 'views/search.html',
            controller: 'SearchCtrl',
            controllerAs: 'sear'
        })
        .when('/user/create', {
            templateUrl: '../views/user-create.html',
            controller: 'UserCreateCtrl',
            controllerAs: 'uc'
        })
        .when('/user/finish', {
            templateUrl: '../views/user-finish.html',
            controller: 'UserFinishCtrl',
            controllerAs: 'uf'
        })
        .when('/student/edit', {
            templateUrl: 'views/student-edit.html',
            controller: 'StudentEditCtrl',
            controllerAs: 'se'
            /*resolve: {
                redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login')
            }*/
        })

        /* .when('/student/company', {
         templateUrl: 'views/student-company.html',
         controller: 'StudentCompanyCtrl',
         controllerAs: 'spComp'
         }) */
        .when('/student/show/:id', {
            templateUrl: 'views/student-show.html',
            controller: 'StudentShowCtrl',
            controllerAs: 'sp'
        })
        .when('/student/manage', {
            templateUrl: 'views/student-manage.html',
            controller: 'StudentManageCtrl',
            controllerAs: 'sm'
            /*resolve: {
                redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login')
            }*/
        })
        .when('/student/create', {
            templateUrl: 'views/student-create.html',
            controller: 'StudentCreateCtrl',
            controllerAs: 'sc'
            /*resolve: {
                redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login')
            }*/
        })
        .when('/company/create', {
            templateUrl: 'views/company-create.html',
            controller: 'CompanyCreateCtrl',
            controllerAs: 'cc'
            /*resolve: {
                redirectIfNotAuthenticated: redirectIfNotAuthenticated('/login')
            }*/
        })
        .when('/organizer/create', {
            templateUrl: 'views/organizer-create.html',
            controller: 'OrganizerCreateCtrl',
            controllerAs: 'oc'
        })
        .when('/student/search', {
            templateUrl: 'views/student-search.html',
            controller: 'StudentSearchCtrl',
            controllerAs: 'ss'
        })
        .when('/401', {
            templateUrl: 'views/no-access.html',
            controller: 'NoAccessCtrl',
            controllerAs: 'noAcc'
        })
        .when('/cookie-policy', {
            templateUrl: '../views/cookie-policy.html',
            controller: 'CookiePolicyCtrl'
        })
        .when('/privacy-policy', {
            templateUrl: 'views/privacy-policy.html',
            controller: 'PrivacyPolicyCtrl'
        })
        .when('/user-agreement', {
            templateUrl: 'views/user-agreement.html',
            controller: 'UserAgreementCtrl'
        })
        .when('/home', {
            templateUrl: 'views/home.html',
            controller: 'HomeCtrl'
        })
        .when('/event-home', {
            templateUrl: 'views/event-home.html',
            controller: 'EventHomeCtrl'
        })
        .when('/organizer-home', {
            templateUrl: 'views/organizer-home.html',
            controller: 'OrganizerHomeCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
})
