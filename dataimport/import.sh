#!/bin/sh

for file in *.csv; do
    mongoimport -d landi -c "${file%.*}" --type csv --headerline --file "$file"
done
